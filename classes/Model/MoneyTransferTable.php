<?php
/**
 * Handles queries and action related to the money transfer table
 *
 * @author maayan
 */
error_reporting(0);

class Model_MoneyTransferTable {

  /**
   * Calculates the monthly usage of transaction limit per sender
   * @param string $senderId a sender Id or passport no
   * @return int the limit usage, in dollars
   */
  public static function getMonthlyLimitUsage($senderId)
  {
    $conn = Model_ConnectionManager::getConnection();

    // Calc transactions sum from the beginning of current month
    $getMonthSumQuery = sprintf(self::getTransactionUsageQuery(),
                                mysql_real_escape_string($senderId),
                                date('Y-m').'-01 00:00:00');
    $monthlySummaryResultRow = mysql_fetch_array(mysql_query($getMonthSumQuery ,$conn));

    return $monthlySummaryResultRow['total'];
  }

 public static function getReceiverMonthlyLimitUsage($recevierId,$coin)
  {
    $conn = Model_ConnectionManager::getConnection();

    // Calc transactions sum from the beginning of current month
    $getMonthSumQuery = sprintf(self::getReceiverTransactionUsageQuery($coin),
                                mysql_real_escape_string($recevierId),
                                date('Y-m').'-01 00:00:00');
    $monthlySummaryResultRow = mysql_fetch_array(mysql_query($getMonthSumQuery ,$conn));

    return $monthlySummaryResultRow['total'];
  }


  /**
   * Calculates the monthly usage of transaction limit per sender for payout to israel
   * @param string $senderId a sender Id or passport no
   * @return int the limit usage, in dollars
   */
  public static function getMonthlyLimitUsagePayout($senderId,$monthsBack = null)
  {
    $conn = Model_ConnectionManager::getConnection();

    $date = date('Y-m').'-01';

    if(!is_null($monthsBack)){
     $date = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-" . $monthsBack . " month" ) );
    }
    // Calc transactions sum from the beginning of current month
    $getMonthSumQuery = sprintf(self::getTransactionUsageQueryPayout(),
      mysql_real_escape_string($senderId),
      $date .  ' 00:00:00');
    $monthlySummaryResultRow = mysql_fetch_array(mysql_query($getMonthSumQuery ,$conn));

    return $monthlySummaryResultRow['total'];
  }

 public static function getSenderReceiverMonthlyLimitUsage($senderId,$receiverId , $coin = null)
  {
    $conn = Model_ConnectionManager::getConnection();

    /* 8.12.15 new regulation rules. month ago and not from the start of the month;*/
    $monthAgo = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

    // Calc transactions sum from the beginning of current month
    $getMonthSumQuery = sprintf(self::getTransactionUsageQuery($coin),
                                        mysql_real_escape_string($senderId),
                                        //date('Y-m').'-01 00:00:00');
                                        $monthAgo.'. 00:00:00');

    // Calc transactions sum from the beginning of current day
    $getMonthSumQueryReceiver = sprintf(self::getReceiverTransactionUsageQuery($coin),
                                        mysql_real_escape_string($receiverId),
                                        //date('Y-m').'-01 00:00:00');
                                        $monthAgo.'. 00:00:00');

    $monthlySummaryResultRow = mysql_fetch_array(mysql_query($getMonthSumQuery ,$conn));
    $monthlySummaryResultRowReceiver = mysql_fetch_array(mysql_query($getMonthSumQueryReceiver ,$conn));

    $arr = array($monthlySummaryResultRow['total'],$monthlySummaryResultRowReceiver['total']);
    return $arr;
  }

public static function getIndiaSenderReceiverMonthlyLimitUsage($senderId,$receiverId)
  {
    $conn = Model_ConnectionManager::getConnection();

    /* 8.12.15 new regulation rules. month ago and not from the start of the month;*/
    /* 6.1.16 remove the regulation so it for the day not for month*/
    // $monthAgo = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
    $monthAgo = date("Y-m-d H:i:s", strtotime( date( "Y-m-d H:i:s", strtotime( date("Y-m-d H:i:s") ) ) . "-1 days" ) );

    // Calc transactions sum from the beginning of current month
    $getMonthSumQuery = sprintf(self::getIndiaTransactionUsageQuery(),
      mysql_real_escape_string($senderId),
      //date('Y-m').'-01 00:00:00');
      //$monthAgo.'. 00:00:00');
      $monthAgo);

    // Calc transactions sum from the beginning of current day
    $getMonthSumQueryReceiver = sprintf(self::getIndiaReceiverTransactionUsageQuery(),
      mysql_real_escape_string($receiverId),
      //date('Y-m').'-01 00:00:00');
      //$monthAgo.'. 00:00:00');
      $monthAgo);


    $monthlySummaryResultRow = mysql_fetch_array(mysql_query($getMonthSumQuery ,$conn));
    $monthlySummaryResultRowReceiver = mysql_fetch_array(mysql_query($getMonthSumQueryReceiver ,$conn));

    $arr = array($monthlySummaryResultRow['total'],$monthlySummaryResultRowReceiver['total']);
    return $arr;
  }

public static function getIndiaMonthlyLimitCount($senderId,$receiverId)
  {
    $conn = Model_ConnectionManager::getConnection();

     $monthAgo = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 years" ) );

    // Calc transactions sum from the beginning of current month
    $getMonthSumQuery = sprintf(self::getIndiaTransactionCountQuery(),
      mysql_real_escape_string($senderId),
      $monthAgo);
    $getMonthSumReceiverQuery = sprintf(self::getIndiaTransactionCountReceiverQuery(),
      mysql_real_escape_string($receiverId),
      $monthAgo);

    $monthlySummaryResultRow = mysql_fetch_array(mysql_query($getMonthSumQuery ,$conn));
    $monthlySummaryResultReceiverRow = mysql_fetch_array(mysql_query($getMonthSumReceiverQuery ,$conn));

    $arr = array($monthlySummaryResultRow['total'],$monthlySummaryResultReceiverRow['total']);
    return $arr;
  }

public static function getBankAccountTransactionMonthlyLimitUsage($bankName,$accountNumber,$coin=null)
  {
    $conn = Model_ConnectionManager::getConnection();

    /* 8.12.15 new regulation rules. month ago and not from the start of the month;*/
    /* 6.1.16 remove the regulation so it for the day not for month*/
    // $monthAgo = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
    $monthAgo = date("Y-m-d H:i:s", strtotime( date( "Y-m-d H:i:s", strtotime( date("Y-m-d H:i:s") ) ) . "-1 month" ) );

    // Calc transactions sum from the beginning of current month
    $getMonthSumQuery = sprintf(self::getBankAccountTransactionLimitUsageQuery($coin),
      mysql_real_escape_string($bankName),mysql_real_escape_string($accountNumber),
      //date('Y-m').'-01 00:00:00');
      //$monthAgo.'. 00:00:00');
      $monthAgo);




    $monthlySummaryResultRow = mysql_fetch_assoc(mysql_query($getMonthSumQuery ,$conn));

    $arr = ($monthlySummaryResultRow['total']);
    return $arr;
  }


  /**
   * Calculates the monthly usage of transaction limit per sender
   * @param string $senderId a sender Id or passport no 
   * @return int the limit usage, in dollars
   */
  public static function getYealyLimitUsage($senderId)
  {
    $conn = Model_ConnectionManager::getConnection();
    
    // Calc transactions sum from the beginning of current month
    $getYearSumQuery = sprintf(self::getTransactionUsageQuery(), 
                               mysql_real_escape_string($senderId),
                               date('Y').'-01-01 00:00:00');
    
    $yearlySummaryResultRow = mysql_fetch_array(mysql_query($getYearSumQuery ,$conn));  
    
    return $yearlySummaryResultRow['total'];
  }
  public static function getSenderReceiverYealyLimitUsage($senderId,$receiverId,$coin = null)
  {
    $conn = Model_ConnectionManager::getConnection();

    // Calc transactions sum from the beginning of current month
    $getYearSumQuery = sprintf(self::getTransactionUsageQuery(),
                               mysql_real_escape_string($senderId),
                               date('Y').'-01-01 00:00:00');

    // Calc transactions sum from the beginning of current day
    $getYearSumQueryReceiver = sprintf(self::getReceiverTransactionUsageQuery(),
                               mysql_real_escape_string($receiverId),
                               date('Y').'-01-01 00:00:00');

    $yearlySummaryResultRow = mysql_fetch_array(mysql_query($getYearSumQuery ,$conn));
    $yearlySummaryResultRowReceiver = mysql_fetch_array(mysql_query($getYearSumQueryReceiver ,$conn));

    $arr = array($yearlySummaryResultRow['total'],$yearlySummaryResultRowReceiver['total']);
    return $arr;
  }

  public static function getYealyLimitUsagePayout($senderId)
  {
    $conn = Model_ConnectionManager::getConnection();

    // Calc transactions sum from the beginning of current month
    $getYearSumQuery = sprintf(self::getTransactionUsageQueryPayout(),
      mysql_real_escape_string($senderId),
      date('Y').'-01-01 00:00:00');

    $yearlySummaryResultRow = mysql_fetch_array(mysql_query($getYearSumQuery ,$conn));

    return $yearlySummaryResultRow['total'];
  }

  public static function getDailyLimitUsage($senderId,$coin = null)
  {
    $conn = Model_ConnectionManager::getConnection();

    // Calc transactions sum from the beginning of current day
    $getDaySumQuery = sprintf(self::getTransactionUsageQuery($coin),
      mysql_real_escape_string($senderId),
      date('Y-m-d').' 00:00:00');

    $daySummaryResultRow = mysql_fetch_array(mysql_query($getDaySumQuery ,$conn));

    return $daySummaryResultRow['total'];
  }

  public static function getSenderReceiverDailyLimitUsageArr($senderId,$receiverId,$coin = null)
  {
    $conn = Model_ConnectionManager::getConnection();


    // Calc transactions sum from the beginning of current day
    $getDaySumQuery = sprintf(self::getTransactionUsageQuery($coin),
                              mysql_real_escape_string($senderId),
                              date('Y-m-d').' 00:00:00');

    // Calc transactions sum from the beginning of current day
    $getDaySumQueryReceiver = sprintf(self::getReceiverTransactionUsageQuery($coin),
                                      mysql_real_escape_string($receiverId),
                                      date('Y-m-d').' 00:00:00');

    $daySummaryResultRow = mysql_fetch_array(mysql_query($getDaySumQuery ,$conn));
    $daySummaryResultRowReceiver = mysql_fetch_array(mysql_query($getDaySumQueryReceiver ,$conn));

    $senderReceiverArr = array( $daySummaryResultRow['total'] ,  $daySummaryResultRowReceiver['total']);

    return $senderReceiverArr;
  }



  private static function getTransactionUsageQuery($coin = null)
  {
    if ($coin == null){
    return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total 
            FROM wic.administration_moneytransfer 
            WHERE money_sender_id='%s'  AND
                  created > '%s' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }else{
      return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE money_sender_id='%s' AND
                  created > '%s' AND usd_eur='$coin'  AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }
  }
 private static function getIndiaTransactionUsageQuery($coin = null)
  {
    if ($coin == null){
    return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE money_sender_id='%s' AND  country_id='INDIA'  AND
                  created > '%s'  AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }else{
      return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE money_sender_id='%s' AND  country_id='INDIA' AND
                  created > '%s' AND usd_eur='$coin' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }
  }
 private static function getIndiaTransactionCountQuery($coin = null)
  {
    if ($coin == null){
    return "SELECT COUNT(*) AS total
            FROM wic.administration_moneytransfer
            WHERE money_sender_id='%s' AND  country_id='INDIA'  AND
                  created > '%s'  AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }else{
      return "SELECT COUNT(*) AS total
            FROM wic.administration_moneytransfer
            WHERE money_sender_id='%s' AND  country_id='INDIA' AND
                  created > '%s' AND usd_eur='$coin' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }
  }
 private static function getIndiaTransactionCountReceiverQuery($coin = null)
  {
    if ($coin == null){
    return "SELECT COUNT(*) AS total
            FROM wic.administration_moneytransfer
            WHERE money_receiver_id ='%s' AND  country_id='INDIA'  AND
                  created > '%s'  AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }else{
      return "SELECT COUNT(*) AS total
            FROM wic.administration_moneytransfer
            WHERE money_receiver_id ='%s' AND  country_id='INDIA' AND
                  created > '%s' AND usd_eur='$coin' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }
  }
 private static function getBankAccountTransactionLimitUsageQuery($coin = null)
  {
    if ($coin == null){
    return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE bank_name='%s' AND  bank_account='%s' AND
                  created > '%s' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }else{
      return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE bank_name='%s' AND   bank_account='%s' AND
                  created > '%s' AND usd_eur='$coin' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }
  }

  private static function getTransactionUsageQueryPayout($coin = null)
  {
    if ($coin == null){
      return "SELECT COALESCE(SUM(administration_transferrequest.amount_to_pay),0) AS total
            FROM wic.administration_transferrequest
            WHERE sender_first_name='%s' AND
                  created > '%s' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }else{
      return "SELECT COALESCE(SUM(administration_transferrequest.amount_to_pay),0) AS total
            FROM wic.administration_transferrequest
            WHERE sender_first_name='%s' AND
                  created > '%s' AND usd_eur='$coin' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }
  }
  private static function getReceiverTransactionUsageQuery($coin = null)
  {
    if ($coin == null){
    return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE money_receiver_id ='%s'  AND
                  created > '%s' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }else{
      return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE money_receiver_id ='%s'  AND
                  created > '%s' AND usd_eur='$coin' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }
  }
  private static function getIndiaReceiverTransactionUsageQuery($coin = null)
  {
    if ($coin == null){
    return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE money_receiver_id 	='%s' AND    country_id='INDIA' AND
                  created > '%s' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }else{
      return "SELECT COALESCE(SUM(administration_moneytransfer.amount),0) AS total
            FROM wic.administration_moneytransfer
            WHERE money_receiver_id ='%s' AND       country_id='INDIA' AND
                  created > '%s' AND usd_eur='$coin' AND
                  status_id NOT IN ('FAILED', 'REQUEST CANCEL','ABORTED')";
    }
  }

}

?>
