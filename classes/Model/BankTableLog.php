<?php

/**
 * Class Model_BankTableLog
 * Handles transactions and banks info
 */
class Model_BankTableLog {
  /**
   * insert new bank to the table bank_info or update Existing bank
   */

  public static function insertLogBankRow($bank_name, $bank_country, $bank_no,$account_dollar, $amount_dollar, $account_euro, $amount_euro, $account_local, $amount_local)  {
    $conn = Model_ConnectionManager::getConnection();
    $use_dollar = 0;
    $currency_local = ' ';

    if (!empty($account_local)) {
      $query = mysql_query("SELECT * FROM administration_country WHERE description = '$bank_country'");
      $row = mysql_fetch_array($query);
      $currency_local = $row['coin_id'];
    }
    if (!empty($account_dollar)) {
      $use_dollar = 1;
    }
    $query = mysql_query("SELECT * FROM providers_per_bank WHERE bank = '$bank_name'");
    $result1 = mysql_fetch_array($query);
    if ($result1 == false){
      $query = sprintf("INSERT INTO providers_per_bank(id, bank) VALUES ('', '%s')", $bank_name);

       $result1 = mysql_query($query, $conn);
        if (!$result1) {
        die('Error: ' . mysql_errno($conn));
        }
        $bank_id = mysql_insert_id();
    }else{
      $bank_id = $result1['id'];
    }

    $query = mysql_query("SELECT * FROM bank_info WHERE bank_id = '$bank_id'");
    $result2 = mysql_fetch_array($query);
    if($result2 == false){
        $date = date('Y-m-d H:i:s');
        $query1 = sprintf("INSERT INTO bank_info (bank_id,bank_no,account_dollar,amount_dollar,account_euro,amount_euro,account_local,
        amount_local,currency_local,created_time) " .
            "VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
            $bank_id,$bank_no,$account_dollar, $amount_dollar, $account_euro, $amount_euro, $account_local, $amount_local, $currency_local,
            $date);

        $result2 = mysql_query($query1, $conn);
        if (!$result2) {
            return false;
        }
    }

    if($result2 === true){
        return true;
    } else {
        return false;
    }
  }

  /**
   * @param $bank_id
   * @param $update_type 1 - Deduct amount, 2 - increase amount
   * @param $type_enter - MANUAL or AUTO
   * @param $amount_dollar
   * @param $amount_euro
   * @param $amount_local
   * @param $transaction_id
   * @param $Commissions
   * @param $comment
   * update specific bank info and enter new transction
   */
  public static function updateBankLogTran($bank_id,$update_type,$status,$type_enter,$amount_dollar,$amount_euro,$amount_local,$transaction_id,$Commissions,$comment){
    $conn = Model_ConnectionManager::getConnection();
      $table1 =   'administration_moneytransfertype';
      if ($type_enter == 'MANUAL'){
          $table1 = 'providers_per_bank';
      }
    if   ($update_type == "1"){
      $amount_dollar = -$amount_dollar;
      $amount_euro = - $amount_euro;
      $amount_local = - $amount_local;
    }
    $query = sprintf("UPDATE bank_info SET
                      amount_dollar = amount_dollar +'$amount_dollar'  ,
                      amount_euro = amount_euro + '$amount_euro' ,
                      amount_local = amount_local + '$amount_local'
                      WHERE bank_id = '$bank_id' ");
    $result1 = mysql_query($query, $conn);
    if (!$result1) {
      echo('Error1: ' . mysql_errno($conn));
    }
    $query2 = mysql_query("SELECT * FROM $table1 WHERE id = '$bank_id'");
    $row = mysql_fetch_array($query2);
      if ($type_enter == 'MANUAL') {
          $query3 = mysql_query("SELECT * FROM bank_info WHERE bank_id = '$bank_id'");
          $row1 = mysql_fetch_array($query3);
          $bank_name = $row['bank'];
          $account_number = $row1['bank_no'];
      }else{
          $bank_name = $row['description'];
          $account_number = $row['bank_no'];
      }

    $query1 = sprintf("INSERT INTO  bank_report (bank_name,account_number,type_enter,status,transaction_id,Commissions,USD_amount,EURO_amount,local_amount,comment)" .
      "VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
      $bank_name,$account_number,$type_enter,$status,$transaction_id,$Commissions, $amount_dollar, $amount_euro,$amount_local,
            $comment);
    $result2 = mysql_query($query1, $conn);
    if (!$result2) {
      die('Error2: ' . mysql_errno($conn));
    }
  }
   /**
    * check what is the currency and return the right amount
    **/
  public static function checkCurrency($bank_type,$currency){
    $conn = Model_ConnectionManager::getConnection();
    $query = mysql_query("SELECT * FROM administration_moneytransfertype as o JOIN bank_info as n ON (O.bank_id = n.id)
    WHERE description = '$bank_type'");
    if ($query == false){
      return null;
    }
    $row = mysql_fetch_array($query);
    if ($currency == 'USA'){
      return $row['amount_dollar'];
    }else{
        if ($currency == 'EURO'){
            return $row['amount_euro'];
        }else {
           return $row['amount_local'];
             }
    }
  }


}