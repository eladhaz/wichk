<?php
/**
 * Handles queries and action related to the sender table
 *
 * @author maayan
 */
class Model_MoneySenderTable {
  
  public static function getIdentificationList()
  {
    $conn = Model_ConnectionManager::getConnection();
    
    $query = "SELECT passport_or_identity_number 
              FROM wic.administration_moneysender 
              ORDER BY passport_or_identity_number";
    $resultSet = mysql_query($query ,$conn);
    
    // Get the column as array
    $result = array();
    while ($row = mysql_fetch_array($resultSet, MYSQL_NUM)) {
        $result[] = $row[0];
    }
    
    return $result;
  }
}
?>
