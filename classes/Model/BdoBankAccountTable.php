<?php
/**
 * Handles queries and action related to the BdoBankAccount table
 *
 * @author maayan
 */
class Model_BdoBankAccountTable {
  /**
   * Returns the bank account row according to receiver id and transfer type
   * @param int $receiverId the receiver id
   * @param string $transferType 'BDO Deposit in BDO' or 'BDO Deposit in other bank' are expected
   * @return mixed the row data as array (id, bank_coed, account_no) or false if none found
   */
  public static function getBankAccountRow($receiverId, $transferType)
  { 
    // Create query template
    $query = "SELECT id, bank_code, account_no ".
             "FROM   administration_bdobankaccount ".
             "WHERE  receiver_id = '%s' AND ".
                    "bank_code IS %s";
    
    // Check which one of the two relevant transfer type is at stake
    if ($transferType == 'BDO Deposit in BDO')
    {
      $query = sprintf($query, $receiverId, 'NULL');
    }
    // It is 'BDO Deposit in other bank'
    else
    {
      $query = sprintf($query, $receiverId, 'NOT NULL');
    }
    
    $conn = Model_ConnectionManager::getConnection();
    $row=mysql_fetch_array(mysql_query($query,$conn)); // Select first row (only one row supposed to be fetched)
    
    // Translate null to the 'BDO' default code for "BDO to BDO" transactions
    if ($row && $transferType == 'BDO Deposit in BDO')
    {
      $row['bank_code'] = 'BDO';
    }
    
    return $row;  
  }
  
  /**
   * Updates a bank account row specified by row id. BDO internal accounts are marked as null bank_code
   * @param type $rowId
   * @param type $bankCode
   * @param type $accountNo
   * @return mixed return value of mysql_query()
   */
  public static function updateBankAccountRow($rowId, $bankCode, $accountNo)
  {
    // Handle a transfer type of 'BDO Deposit in BDO'
    if ($bankCode == 'BDO')
    {
      $bankCode = 'NULL';
    }
    else
    {
      $bankCode = "'".$bankCode."'";
    }
    
    // Create query template
    $query = "UPDATE administration_bdobankaccount ".
             "SET bank_code = %s, account_no = '%s' ".
             "WHERE id = '%s'";
    $query = sprintf($query,$bankCode ,$accountNo ,$rowId);
    
    $conn = Model_ConnectionManager::getConnection();
    return mysql_query($query,$conn);
  }
  
  /**
   * Inserts a bank account row. BDO internal accounts are marked as null bank_code
   * @param type $bankCode
   * @param type $accountNo
   * @return mixed return value of mysql_query()
   */
  public static function insertBankAccountRow($receiverId, $bankCode, $accountNo)
  {
    // Handle a transfer type of 'BDO Deposit in BDO'
    if ($bankCode == 'BDO')
    {
      $bankCode = 'NULL';
    }
    else
    {
      $bankCode = "'".$bankCode."'";
    }
    
    // Create query template
    $query = "INSERT INTO administration_bdobankaccount ".
             "SET receiver_id = %s,bank_code = %s, account_no = '%s' ";
    $query = sprintf($query, $receiverId, $bankCode ,$accountNo);
    
    $conn = Model_ConnectionManager::getConnection();
    return mysql_query($query,$conn);
  }
}

?>
