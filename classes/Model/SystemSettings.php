<?php
error_reporting(0);

/**
 * Encapsulates the system settings table
 */
class Model_SystemSettings
{
  /**
   * Returns a system settings value by key and namespace, or default if not found
   * @param string $key the key to search for
   * @param string $default a default to be returned if key not found
   * @param string $namespace a namespace to look in
   * @return mixed the system setting value 
   */
  public static function get($key, $default = null, $namespace = 'general')
  {
    $conn = Model_ConnectionManager::getConnection();
    
    $getSettingQuery = sprintf("SELECT value 
                                FROM wic.system_settings 
                                WHERE namespace='%s' AND 
                                      name='%s' AND value='$default'",
                                mysql_real_escape_string($namespace),
                                mysql_real_escape_string($key));
    
    $value = $default;
    if ($settingRow = mysql_fetch_array(mysql_query($getSettingQuery ,$conn)))
    {
      $value = $settingRow['value'];
    }
    
    return $value;
  }
  
  /**
   * Sets a system settings value by key and namespace
   * @param string $key the key to search for
   * @param string $value the new value
   * @param string $namespace a namespace to look in
   * @return mixed return value of mysql_query()
   */
  public static function set($key, $value, $namespace = 'general')
  {
    $conn = Model_ConnectionManager::getConnection();

    $query = sprintf("UPDATE wic.system_settings 
                      SET value = '%s' 
                      WHERE namespace='%s' AND 
                            name='%s'",
                     $value, $namespace, $key);
    
    mysql_query($query, $conn);
  }
}

?>
