<?php

/**
 * A singleton class to allow a single connection for the system
 *
 * @author maayan
 */

class Model_ConnectionManager {
  
  private static $conn = null;
  
  public static function getConnection()
  {
    if (self::$conn == null)
    {
      self::$conn = wic_db_connect();
    }

  @mysql_query("SET NAMES 'utf8' ");
    return self::$conn;
  }
}

?>
