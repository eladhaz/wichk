<?php

/**
 * Handles queries and action related to the credit log table
 *
 * @author maayan
 */
class Model_CreditLogTable
{

  /**
   * This function is used to report on a status change, if an action shuuld be taken
   * such as updating the credit and credit log, it will be done
   * @param string $id a transaction id or a refer_no
   * @param boolean $isReferNo is the id given a refer_no or a transaction id
   * @param boolean $isExchange are we updating an exchange transaction or a normal transaction
   */
  public static function reportStatusChange($id, $isReferNo = false, $isExchange = false, $updated = null,$vipNum = null)
  {
    $conn = Model_ConnectionManager::getConnection();
    $amount_dollar = 0;
    $amount_euro = 0;
    $amount_local = 0;
    // We are making an automatic update
    $type = 'AUTO';
    $transCurrency = 'USD'; // 28.5.13 - Added EURO Handling
    // Act according to the entity at stake (transaction/exchange)
    if (!$isExchange) {
      // Get the transaction reported
      if (!$isReferNo) {
        $getTransactionQuery = sprintf("SELECT * FROM administration_moneytransfer WHERE id='%s'", $id);
      } else {
        $getTransactionQuery = sprintf("SELECT * FROM administration_moneytransfer WHERE refer_no=%s", $id);
      }

      $transactionRow = mysql_fetch_array(mysql_query($getTransactionQuery, $conn));

      // 28.5.13 - Added EURO Handling


      // Checks if it's USD or EURO transaction


      $transCurrency = $transactionRow['usd_eur'];


      // Init necessary data

      $bank_name = $transactionRow['money_transfer_type_id'];
      $commissions = $transactionRow['wic_cost'];
      $commissions1 = $transactionRow['commission'];
      $usedCreditDelta = $transactionRow['amount'] + $transactionRow['commission'];
      $agentId = $transactionRow['agent_id'];
      $userId = $transactionRow['user_id'];
      $newStatus = $transactionRow['status_id'];
      $transactionId = $transactionRow['id'];
      $transactionType = 'TRANSFER';
      $transferType = $transactionRow['money_transfer_type_id'];
      $country = $transactionRow['country_id'];
      if ($transactionRow['coin_id'] == 'USD') {
        $amount_dollar = $usedCreditDelta + $commissions;
      } else {
        if ($transactionRow['coin_id'] == 'EURO') {
          $amount_euro = $usedCreditDelta + $commissions;
        } else {
          $amount_local = $usedCreditDelta + $commissions;
        }
      }
    } else {
      // Get the (exchnage) transaction reported
      if (!$isReferNo) {
        $getTransactionQuery = sprintf("SELECT * FROM administration_moneyexchange WHERE id='%s' AND (administration_moneytransfer.sender_gbg != 'FAIL'  AND administration_moneytransfer.receiver_gbg != 'FAIL') ", $id);
      } else {
        $getTransactionQuery = sprintf("SELECT * FROM administration_moneyexchange WHERE refer_no=%s AND (administration_moneytransfer.sender_gbg != 'FAIL'  AND administration_moneytransfer.receiver_gbg != 'FAIL')", $id);
      }

      $transactionRow = mysql_fetch_array(mysql_query($getTransactionQuery, $conn));

      // Init necessary data
      $bank_name = $transactionRow['money_transfer_type_id'];
      $commissions = $transactionRow['wic_cost'];
      $commissions1 = $transactionRow['commission'];
      $usedCreditDelta = $transactionRow['collected_usd'];
      $agentId = $transactionRow['agent_id'];
      $userId = $transactionRow['user_id'];
      $newStatus = $transactionRow['status_id'];
      $transactionId = $transactionRow['id'];
      $transactionType = 'EXCHANGE';
      $transferType = $transactionRow['money_transfer_type_id'];
      $country = $transactionRow['country_id'];
      if ($transactionRow['coin_id'] == 'USD') {
        $amount_dollar = $usedCreditDelta + $commissions;
      } else {
        if ($transactionRow['coin_id'] == 'EURO') {
          $amount_euro = $usedCreditDelta + $commissions;
        } else {
          $amount_local = $usedCreditDelta + $commissions;
        }
      }
    }
    $agentIdUpdate = $agentId;
    if ($updated == null) {
      $result = self::isExist($transactionId, $newStatus, $transCurrency,$agentId);
    /*  $isVip = self::isExistVip($transactionId, $transCurrency);
      if ($isVip){
        $vipNum = null;
      }*/
    } else {
      $result = false;
    }
    // For transactions moved to 'WAIT' we should sum their value to te used credit,
    // and the opposite for 'FAILED'
    $shouldUpdateUsedCredit = ($newStatus == 'FAILED' || $newStatus == 'WAIT'  || $newStatus == 'COMPLIANCE PROCESS' ||
      $newStatus == 'WAIT FOR PROCESS' || $newStatus == 'ABORTED' || $newStatus == 'CHECK DOCUMENTS' || $newStatus == 'WAIT FOR PROCESS(Obligo)');
    // If it is a failed transaction, then we should deduct its value from the used credit
    // And send an email to notify the agent
    if ($newStatus == 'FAILED' || $newStatus == 'ABORTED') {
      $usedCreditDelta = -$usedCreditDelta;
      $amount_dollar = -$amount_dollar;
      $amount_euro = -$amount_euro;
      $amount_local = -$amount_local;
      $commissions1 = -$commissions1;
    }

//    if ($newStatus == 'POSTED'){
//      self::sendSms($id);
//    }

    if ($newStatus == 'FAILED' && ($transferType == "CEBUANA LHUILLIER pick up Php only" || $transferType == "M. LHUILLIER pick up" || $transferType == "DFFC pick up")) {
      switch ($transferType) {
        case "CEBUANA LHUILLIER pick up Php only" :
          $data = file_get_contents('../../transfers/API/CB.txt');
          break;
        case "M. LHUILLIER pick up" :
          $data = file_get_contents('../../transfers/API/Ml.txt');
          break;
        case "DFFC pick up" :
          $data = file_get_contents('../../transfers/API/DFFC.txt');
          break;
        default :
          return null;
      }
        $json_array = json_decode($data, true);
        $assoc_array_file=array();
        for($i = 0; $i < sizeof($json_array); $i++)
        {
            $key = $json_array[$i];
            $assoc_array_file[$key] = "'".$json_array[$i]."'";
        }
        unset($assoc_array_file[$transactionId]);
        $assoc_array_file = str_replace("'","",array_values($assoc_array_file));
      switch ($transferType) {
        case "CEBUANA LHUILLIER pick up Php only" :
          file_put_contents('../../transfers/API/CB.txt', json_encode($assoc_array_file));
          break;
        case "M. LHUILLIER pick up" :
          file_put_contents('../../transfers/API/Ml.txt', json_encode($assoc_array_file));
          break;
        case "DFFC pick up" :
          file_put_contents('../../transfers/API/DFFC.txt', json_encode($assoc_array_file));
          break;
        default :
      }
      self::sendFailedTransactionNotificationMail($userId, $transactionId);
    }
    $bank_id = mysql_query("SELECT * FROM administration_moneytransfertype WHERE description = '$bank_name'");
    $row = mysql_fetch_array($bank_id);
    $bank_id = $row['id'];


    // Init description
    $description = 'Unknown';
    switch ($newStatus) {
      case 'WAIT':
        $description = 'Transaction added';
        break;
      case 'CHECK DOCUMENTS':
        $description = 'Transaction added';
        break;

      case 'COMPLIANCE PROCESS':
        $description = 'Transaction added';
        break;
      case 'WAIT FOR PROCESS':
        $description = 'Transaction added on expense of future credit';
        self::sendWaitForProcessTransactionNotificationMail($transactionId, $agentId);
        break;
      case 'WAIT FOR PROCESS(Obligo)':
        $description = 'Transaction added on expense of future credit';
        self::sendWaitForProcessTransactionNotificationMail($transactionId, $agentId);
        break;
      case 'FAILED':
        $description = 'Transaction failed.';
        break;
      case 'ABORTED':
        $description = 'Transaction aborted';
        break;
      default:
        break;
    }
    $networkType =  self::checkNetworkType($agentId);
    if ($networkType['network_type'] == 2){
      $agentIdUpdate = self::getSupervisorNetwork($networkType);
    }

    // Set new used credit
    if (($shouldUpdateUsedCredit) && ($result == false)) {
      $newUsedCredit = Model_AgentTable::updateUsedCredit($agentIdUpdate, $usedCreditDelta, $transCurrency);
      Model_BankTableLog::updateBankLogTran($bank_id, '1', $newStatus, $type, $amount_dollar, $amount_euro, $amount_local, $transactionId,
        $commissions, '');
      if (($vipNum !=null) && ($vipNum != 'N/A')){
        deIncrementVip($vipNum,$newStatus,$country,$transferType);
        self::insertLogRow($agentId,$userId, $type, $usedCreditDelta, $transactionId, $newStatus, $newUsedCredit, $transCurrency, $transactionType,
          $description, null, null, $updated, $commissions1);
        self::updateCredit($vipNum,$commissions1);
      }else {
        self::insertLogRow($agentId, $userId, $type, $usedCreditDelta, $transactionId, $newStatus, $newUsedCredit, $transCurrency, $transactionType,
            $description, null, null, $updated, 0);
      }
    }


  }

  /**
   * This function is used to report on a status change of payout, if an action shuuld be taken
   * such as updating the credit and credit log, it will be done
   * @param string $id a transaction id or a refer_no
   * @param boolean $isReferNo is the id given a refer_no or a transaction id
   * @param boolean $isExchange are we updating an exchange transaction or a normal transaction
   */
  public static function reportStatusChangePayout($id,$via)
  {
    $conn = Model_ConnectionManager::getConnection();
    $amount_dollar = 0;
    $amount_euro = 0;
    $amount_local = 0;

    // We are making an automatic update
    $type = 'AUTO';
    $transCurrency = 'USD'; // 28.5.13 - Added EURO Handling
    // Act according to the entity at stake (transaction/exchange)
    if ($via == 'Contact') {
      // Get the transaction reported
     $getTransactionQuery = sprintf("SELECT * FROM administration_transferrequest WHERE transaction_id='%s' ", $id);
     $transactionRow = mysql_fetch_array(mysql_query($getTransactionQuery, $conn));
     $res = mysql_query("SELECT * FROM usd_rates_log ORDER BY date DESC LIMIT 1");
     $row = mysql_fetch_assoc($res);
      // 28.5.13 - Added EURO Handling

      // Checks if it's USD or EURO transaction

      if (($transactionRow['coin_id'] == 'EUR')) {

        $transCurrency = 'EUR';
      }
      if (($transactionRow['coin_id'] == 'ILS')) {
        $transactionRow['amount'] = $transactionRow['amount']/$row['rate'];
        $transCurrency = 'USD';
      }
      // Init necessary data

      $bank_name = $transactionRow['receiver_bankname'];
      $commissions = $transactionRow['agent_commission'];
      $usedCreditDelta = -($transactionRow['amount_to_pay'] + $transactionRow['agent_commission']);
      $agentId = $transactionRow['agent_id'];
      $userId = $transactionRow['user_id'];
      $newStatus = $transactionRow['status_id'];
      $transactionId = $transactionRow['transaction_id'];
      $transactionType = 'TRANSFER';
      if ($transactionRow['coin_id'] == 'USD') {
        $amount_dollar = -($usedCreditDelta + $commissions);
      } else {
        if ($transactionRow['coin_id'] == 'EURO') {
          $amount_euro = -($usedCreditDelta + $commissions);
        } else {
          $amount_local = -($usedCreditDelta + $commissions);
        }
      }
    } else {
      // Get the (exchnage) transaction reported

      $getTransactionQuery = sprintf("SELECT * FROM transfer_request_payout WHERE tfpin='%s'", $id);
      $transactionRow = mysql_fetch_array(mysql_query($getTransactionQuery, $conn));

      // Init necessary data
      $bank_name = $transactionRow['bank_name'];
      $commissions = $transactionRow['agent_commission'];
      $usedCreditDelta = -($transactionRow['receiver_amount'] + $commissions);
      $agentId = $transactionRow['agent_id'];
        $userId = $transactionRow['user_id'];

        $newStatus = $transactionRow['status'];
      $transactionId = $transactionRow['tfpin'];
      $transactionType = 'transfer';
      if ($transactionRow['coin_id'] == 'USD') {
        $amount_dollar = -($usedCreditDelta + $commissions);
      } else {
        if ($transactionRow['coin_id'] == 'EURO') {
          $amount_euro = -($usedCreditDelta + $commissions);
        } else {
          $amount_local = -($usedCreditDelta + $commissions);
        }
      }
    }
    $bank_id = mysql_query("SELECT * FROM administration_moneytransfertype WHERE description = '$bank_name'");
    $row = mysql_fetch_array($bank_id);
    $bank_id = $row['id'];


    // Init description
    $description = 'Transaction added';

    // Set new used credit
      $newUsedCredit = Model_AgentTable::updateUsedCredit($agentId, $usedCreditDelta, $transCurrency);

      Model_BankTableLog::updateBankLogTran($bank_id, '1', $newStatus, $type, $amount_dollar, $amount_euro, $amount_local, $transactionId,
        $commissions, '');
        self::insertLogRow($agentId,$userId, $type, $usedCreditDelta, $transactionId, $newStatus, $newUsedCredit, $transCurrency, $transactionType,
          $description, null, null, null, 0);



  }

  /**
   * @param $agent
   * @return array of network the user is
   */
  public static function checkNetworkType($agent){
    $conn = Model_ConnectionManager::getConnection();
    $getTransactionQuery = sprintf("SELECT * FROM agents JOIN network ON network.id_num=agents.network
                                                         JOIN branches ON branches.id_num=agents.branch_num
                                                         AND branches.branch_name = '%s'",$agent);
    $transactionRow = mysql_fetch_array(mysql_query($getTransactionQuery, $conn));
    return $transactionRow;
  }

  /**
   * @param $network
   * @return superviser branch name
   */
  public static function getSupervisorNetwork($network){
    $conn = Model_ConnectionManager::getConnection();
    $getTransactionQuery = sprintf("SELECT * FROM agents JOIN branches ON branches.id_num=agents.branch_num
                                                  WHERE network='%s' AND type='supervisor'", $network['network']);
    $transactionRow = mysql_fetch_array(mysql_query($getTransactionQuery, $conn));
    return $transactionRow['branch_name'];
  }

  // added by barak for sms notification 02/11/14
  function sendSms($transctionId)
  {
    $pathToSms = getcwd() . "/../../wic/webservices/APIs/SMS/SMS_Sender.php";
    require_once($pathToSms);
    $smsSender = new SMS_Sender();
    $smsSender->wicSmsNotifiy($transctionId, 1);
  }


  public static function updateCredit($vip_num,$commission)
  {
    mysql_query("UPDATE vip_users SET total_credit=total_credit + '$commission' WHERE vip_number='$vip_num' and active = 'YES'");
  }

  public static function sendFailedTransactionNotificationMail($userId, $transactionId)
  {
    $userQuery = sprintf("SELECT username, email from administration_webuser WHERE username = '%s'", $userId);
    $userRow = mysql_fetch_array(mysql_query($userQuery));
    $username = $userRow['username'];
    $userEmail = $userRow['email'];

    $subject = 'Failed transaction - ' . $transactionId;

    $message = "Dear %s,\n" .
      "Transaction %s has failed.";

    $message = sprintf($message, $username, $transactionId);

    $headers = "From: Webmaster <no-reply@wic.com>";

    @mail($userEmail, $subject, $message, $headers);
  }


  public static function sendWaitForProcessTransactionNotificationMail($transactionId, $agentId)
  {
    $recipientsQuery = sprintf("SELECT * FROM administration_email WHERE type_id = 'bookkeeping' AND active = 1 ");
    $recipientsResult = mysql_query($recipientsQuery);

    $recipientsList = '';
    $row = mysql_fetch_array($recipientsResult);
    if ($row) {
      $recipientsList = $row['email'];
      while ($row = mysql_fetch_array($recipientsResult)) {
        $recipientsList .= ", " . $row['email'];
      }
    }

    $subject = 'Transaction waiting for clearance - ' . $transactionId . '. Agent: ' . $agentId;

    $message = "Dear bookkeeping attendant,\n" .
      "Transaction %s was added to the system without enough funds for the agent to use.\n" .
      "This can be solved either by updating the agent's debt with a deposit, or by increasing the obligo.\n" .
      "When the funds will be cleared, the transaction will change it's status automatically from WAIT FOR PROCESS to PENDING.\n" .
      "\nTake care,\nSystem Administrator";

    $message = sprintf($message, $transactionId);

    $headers = "From: Webmaster <no-reply@wic.com>";

    @mail($recipientsList, $subject, $message, $headers);
  }

  //this function check if the $transactionId exist in credit_log at $newStatus
  private function isExist($transactionId, $newStatus, $transCurrency,$agentId)
  {
    $result = false;
    if ($transCurrency == 'EUR') {
      $Query = sprintf("SELECT new_status,agent_id FROM obligo_report_eur WHERE transaction_id = '$transactionId'");
    } else {
      $Query = sprintf("SELECT new_status,agent_id FROM obligo_report WHERE transaction_id = '$transactionId'");
    }
    $Result = mysql_query($Query);
    while ($row = mysql_fetch_array($Result)) {
      if (($row['new_status'] == $newStatus) && ($row['agent_id'] == $agentId) ) { //|| ($newStatus == "CONFIRMED")) {
        $result = true;
        break;
      }
    }

    return $result;
  }

  //this function check if the $transactionId exist in credit_log
  private function isExistVip($transactionId, $transCurrency)
  {
    $result = false;
    if ($transCurrency == 'EUR') {
      $Query = sprintf("SELECT credit FROM obligo_report_eur WHERE transaction_id = '$transactionId'");
    } else {
      $Query = sprintf("SELECT credit FROM obligo_report WHERE transaction_id = '$transactionId'");
    }
    $Result = mysql_query($Query);
    while ($row = mysql_fetch_array($Result)) {
      if ($row['credit'] == 0) { //|| ($newStatus == "CONFIRMED")) {
        $result = true;
        break;
      }
    }

    return $result;
  }






  /**
   * The function inserts a row to the credit log
   * @param string $agentId
   * @param string $type
   * @param int $usedCreditDelta
   * @param string $transactionId
   * @param string $newStatus
   * @param int $newUsedCredit
   * @param string $transactionType whether the id identifies a moneytransfer or a money exchange
   * @param string $comment
   * @param string $description
   * @return boolean did the insert succeeded
   */
  public static function insertLogRow($agentId,$user_id, $type, $usedCreditDelta, $transactionId, $newStatus, $newUsedCredit, $transCurrency, $transactionType = 'TRANSFER', $description, $comment = null, $filename = null, $update = null, $commissions = 0)
  {
    $currency = null;
    $network = 0;
    $networkType =  self::checkNetworkType($agentId);
    if ($networkType['network_type'] == 2){
      $network = $networkType['network'];
    }
    // 28.5.13 - Added EURO Handling
    if ($transCurrency == 'USD') {
      $table = 'obligo_report';
    } else {
      if ($transCurrency == 'EUR') {
        $table = 'obligo_report_eur';
      }else{
        $table = 'obligo_currency';

        $currency = $transCurrency;
      }
    }
    if ($_SESSION['autonomy'] == 2){
      $d = new DateTime("now", new DateTimeZone("Asia/Hong_Kong"));
    }else{
      $d = new DateTime("now", new DateTimeZone("Asia/Jerusalem"));

    }
    $time =  $d->format('Y-m-d H:i:s'); //2010-08-14T15:22:22+00:00

    $conn = Model_ConnectionManager::getConnection();
    $query = sprintf("INSERT INTO $table (agent_id, type,network_num,currency, used_credit_delta, transaction_id, transaction_type, new_status, new_used_credit, executer_agent_id,executer_clerk_id, description, comment, filename, created,credit) " .
      "VALUES ('%s' ,'%s' ,%s,'%s',%s, %s, %s,%s, %s, %s, %s, '%s', %s, %s, '$time','%s')",
      $agentId,
      $type,
      $network,
      $currency,
      $usedCreditDelta,
      (isset($transactionId) ? "'" . $transactionId . "'" : 'NULL'),
      (isset($transactionType) ? "'" . $transactionType . "'" : 'NULL'),
      (isset($newStatus) ? "'" . $newStatus . "'" : 'NULL'),
      $newUsedCredit,
      (isset($_SESSION['branch']) ? "'" . $_SESSION['branch'] . "'" : 'NULL'),
        (isset($_SESSION['myusername']) ? "'" . $_SESSION['myusername'] . "'" : 'NULL'),
      $description,
      ((isset($comment) && $comment != '') ? "'" . $comment . "'" : 'NULL'),
      ((isset($filename) && $filename != '') ? "'" . $filename . "'" : 'NULL'),
      $commissions);
    return mysql_query($query, $conn);
  }

}


/** make decrement -- to the vipdiscount counter
 *
 * @param $vipNum
 */
function deIncrementVip($vipNum,$newStatus,$country = "",$type = ""){
  $isContact = ( strtolower($type) === strtolower("Contact") && ( strtolower($country) != strtolower("BULGARIA") ||  strtolower($country) != strtolower("ROMANIA") ) ? true : false );

  if(($newStatus == "ABORTED" || $newStatus == "FAILED") && !$isContact){
    mysql_query("UPDATE  vip_users SET vip_discount =  vip_discount - 1  WHERE `vip_number` = '$vipNum'");
  }

}

?>
