<?php
/**
 * Handles queries and action related to the agent table
 *
 * @author maayan
 */
class Model_AgentTable {
  
  /**
   * Updates the used credit for an agent
   * @param string $agent_id the agent id
   * @param int $creditUsedDelta the amount to be added to the used_credit (can be a negative integer)
   * @return int the new used_credit 
   */

  public static function updateUsedCredit($agentId, $creditUsedDelta, $transCurrency)
  {
      
      // 28.5.13 - Added EURO Handling
      if ($transCurrency == 'USD')
      {
          $field = 'used_credit';
      }
      else {
          if ($transCurrency == 'EUR') {
              $field = 'used_credit_eur';
          } else {
              $field = 'used_credit_cur';
          }
      }


    $conn = Model_ConnectionManager::getConnection();
    mysql_query(sprintf("UPDATE administration_agent SET $field = $field + %s WHERE id='%s'",
                        $creditUsedDelta, $agentId),
                $conn);
    
    // Fetch and return new used_credit
    $getUsedCreditQuery = sprintf("SELECT $field FROM administration_agent WHERE id='%s'", 
                                  $agentId);
    $usedCreditRow = mysql_fetch_array(mysql_query($getUsedCreditQuery ,$conn));
    
    return $usedCreditRow[$field];
  }
  
  /**
   * Updates the credit for an agent
   * @param string $agent_id the agent id
   * @param int $newCredit the new credit
   */
  public static function updateCredit($agentId, $newCredit, $currency)
  {
      // 28.5.13 - Added EURO Handling
      if ($currency == 'USD')
      {
          $field = 'obligo';
      }
      else
      {
          if ($currency == 'EUR') {
              $field = 'obligo_eur';
          } else {
              $field = 'obligo_cur';
          }
      }
    $conn = Model_ConnectionManager::getConnection();
    mysql_query(sprintf("UPDATE administration_agent SET $field = %s WHERE id='%s'",
                        $newCredit, $agentId),
                $conn);
  }

    /**
     * Updates the second credit for an agent
     * @param string $agent_id the agent id
     * @param int $newCredit the new credit
     */
    public static function updateSecondCredit($agentId, $newCredit, $currency,$time)
    {

        // 28.5.13 - Added EURO Handling
        if ($currency == 'USD')
        {
            $field = 'second_obligo';
            $fieldTime = 'second_obligo_time';
        }
        else
        {
            if ($currency == 'EUR') {
                $field = 'second_obligo_eur';
                $fieldTime = 'second_obligo_eur_time';

            } else {
                $field = 'second_obligo_cur';
                $fieldTime = 'second_obligo_cur_time';

            }
        }
        if ($newCredit == 0){
            $time = null;
        }
        $conn = Model_ConnectionManager::getConnection();
        mysql_query(sprintf("UPDATE administration_agent SET $field = %s , $fieldTime = '%s' WHERE id='%s'",
            $newCredit,$time, $agentId),
            $conn);
    }


  public static function updateLoadSecurity($agentId, $newCredit, $currency)
  {
    // 28.5.13 - Added EURO Handling
    if ($currency == 'USD')
    {
      $field = 'deposit	';
    }
    else
    {
      $field = 'deposit_eur';
    }
    $conn = Model_ConnectionManager::getConnection();
    mysql_query(sprintf("UPDATE administration_agent SET $field = %s WHERE id='%s'",
        $newCredit, $agentId),
      $conn);
  }

  /**
   * Gets a subordinate agents ids list for current logged on user.
   * For office, operation and bookkeeping - all agents will be fetched
   * For supervisors - their subordinates will be fetched
   * For other users - the current agent will be returned
   * The result can be used in a IN condition of a query, as an empty array will never be returned
   * @return array an array of subordinate agents ids
   */
  public static function getCurrentUserSubordinates()
  {
    $bIsAdmin = ($_SESSION['branch'] == "operation") || ($_SESSION['branch'] == 'administrator') || ($_SESSION['branch'] == "bookkeeping") ;
    $bIsSupervisor = ($_SESSION["user_status"] == "Supervisor") || ($_SESSION["user_status"] == "manager");
      
    $conn = Model_ConnectionManager::getConnection();
    $shouldQueryDb = false;
    
    // If admin, has permissions to everyone
    if ($bIsAdmin)
    {
      $query = "SELECT   LOWER(id) AS id ".
               "FROM     administration_agent WHERE (id<>'F5') and (id<>'mlhuillier') and (id<>'TRANSFAST') and (id<>'cebuana') and (id<>'dffc') ".
               "ORDER BY id ASC";

      $shouldQueryDb = true;
    }
    // If supervisor, select subordinates
    else if ($bIsSupervisor)
    {
      $query = sprintf("SELECT   LOWER(id)  AS id ".
                       "FROM     administration_agent ".
                       "WHERE    id = '%s' ".
                       "ORDER BY id ASC",
                       mysql_real_escape_string($_SESSION["branch"]));
              
      $shouldQueryDb = true;
    }
    
    $subordinateAgentsIds = array();
    if (!$shouldQueryDb)
    {
      // Every agent has permissions to itself
      $subordinateAgentsIds = array($_SESSION['branch']);
    }
    else
    {
      $res=mysql_query($query,$conn);
      while ($row=mysql_fetch_array($res)){
        $subordinateAgentsIds[] = $row['id'];
      }
    }
    
    return  $subordinateAgentsIds;  
  }
  public static function getCurrentUserSubordinatesRegularName()
  {
    $bIsAdmin = ($_SESSION['branch'] == "operation") || ($_SESSION['branch'] == 'administrator') || ($_SESSION['branch'] == "bookkeeping") ;
    $bIsSupervisor = strtolower($_SESSION["user_status"]) == strtolower("supervisor");

    $conn = Model_ConnectionManager::getConnection();
    $shouldQueryDb = false;

    // If admin, has permissions to everyone
    if ($bIsAdmin)
    {
     /* $query = "SELECT   LOWER(id) AS id ".
               "FROM     administration_agent ".
               "ORDER BY id ASC";*/
      $query = "SELECT   id AS id ".
               "FROM     administration_agent  WHERE (id<>'F5') and (id<>'mlhuillier') and (id<>'TRANSFAST') and (id<>'cebuana') and (id<>'dffc') ".
               "ORDER BY id ASC";

      $shouldQueryDb = true;
    }
    // If supervisor, select subordinates
    elseif ($bIsSupervisor)
    {


        $branch_num = "SELECT network FROM agents WHERE name = '%s' AND type='%s'";
        $first_query = sprintf($branch_num, mysql_real_escape_string($_SESSION["myusername"]), mysql_real_escape_string($_SESSION["user_status"]));

        $res = mysql_query($first_query, $conn);
        if ($row=mysql_fetch_array($res)){
            $networkNum = $row['network'];
        }

        $str = "SELECT * FROM branches INNER JOIN network_branches ON network_branches.branch_num = branches.id_num WHERE network_branches.id_num='%s'";
        $query = sprintf($str, $networkNum);

      $shouldQueryDb = true;
    }

    $subordinateAgentsIds = array();
    if (!$shouldQueryDb && $_SESSION['user_status'] != 'clerk')
    {
       // Every agent has permissions to itself
      $subordinateAgentsIds = array($_SESSION['branch']);
    }
    else
    {
      $res=mysql_query($query, $conn);
      while ($row=mysql_fetch_array($res)){
        if($bIsAdmin){
            $subordinateAgentsIds[] = strtolower($row['id']);
        } else {
            $subordinateAgentsIds[] = strtolower($row['branch_name']);
        }
      }
    }

    return  $subordinateAgentsIds;
  }
}

?>
