<?php
/**
 * Handles queries and action related to the usd_rates_log table
 *
 * @author maayan
 */
class Model_USDRatesLogTable {
 
  /**
   * Refreshes USD rates using Bank of Israel service
   * @return boolean Whether the refresh process completed successfully
   */
  public static function refreshRates()
  {
    $success = true;
    
    $conn = Model_ConnectionManager::getConnection();
    
    // Get the first missing date and latest rate from log
    $query = "SELECT date, rate
              FROM wic.usd_rates_log
              ORDER BY date DESC";
    $resultSet = mysql_fetch_array(mysql_query($query ,$conn));  
    
    $firstMissingDate = date('Y-m-d',strtotime('+1 day', strtotime($resultSet['date'])));
    $previousRate = $resultSet['rate'];
    
    // Build a list of dates to get rates for
    $dates = self::dateRange($firstMissingDate, date('Y-m-d'));
    
    // The array will contain the rates. with the dates as keys
    $rates = array();
    
    // For each date, fetch its rate, if rate not found, use the previous rate
    foreach ($dates as $currDate)
    {
      $rates[$currDate] = self::fetchRate($currDate, $previousRate);
      
      if ($rates[$currDate] === false)
      {
        $success = false;
        break;
      }
      
      $previousRate = $rates[$currDate];
    }  
    
    // Make an insert for each date->rate couple
    // (Could be in same loop but was done for readability)
    if ($success)
    {
      $insertRateLogQuery = "INSERT INTO wic.usd_rates_log (date ,rate) VALUES ( '%s','%s')";
      foreach ($rates as $date => $rate)
      {
        $success = mysql_query(sprintf($insertRateLogQuery, $date, $rate),$conn);
        
        if (!$success)
        {
          break;
        }
      }
    }
    
    return $success;
  }
  
  private static function fetchRate($date, $previousRate)
  {
    // We will return false for an error
    $rate = false;
    
    try 
    {
      // Init the request url
      $requestURL = sprintf('http://www.bankisrael.gov.il/heb.shearim/currency.php?rdate=%s&curr=01',
                             date('Ymd', strtotime($date)));

      // make the call
      $ch = curl_init($requestURL);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $curlResult = curl_exec($ch);
      curl_close($ch);
      
      // If curl went well
      if ($curlResult !== false)
      {
        // Load result as xml
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($curlResult);
        libxml_use_internal_errors(false);

        // if xml valid
        if ($xml)
        {
          // Check if there were errors, if so, return the previous rate
          if (isset($xml->ERROR1))
          {
            if (isset($xml->ERROR2) && 
                ((string)$xml->ERROR2) == 'No exchange rate published for this date')
            {
              $rate = $previousRate;
            }
            // Otherwise return false as initialized
          }
          else
          {
            // In some cases the xml will be without an error even when there is no rate
            if (isset($xml->CURRENCY))
            {
                $rate = (string)$xml->CURRENCY->RATE; 
            }
            else 
            {
                $rate = $previousRate;
            }
          }
        }
      }
    } 
    catch (Exception $exc) 
    {
      $rate = false;
    }
    
    return $rate;
  }
  
  private static function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d' ) 
  {
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while($current <= $last) 
    {
      $dates[] = date($format,$current);
      $current = strtotime($step, $current);
    }

    return $dates;
  }
}
?>
