<?php

/**
 * Handles actions that related to transaction validation
 */
error_reporting(0);

class TransactionValidation
{  
  /**
   * This function calculates a transaction limit per client
   * @param string $senderId the send Id or passport number 
   * @return int the current transaction limit in dollars
   */

  public static function getTransactionLimit($senderId,$coin,$country)
  {
    // Get transaction limitations from system settings
    if ($coin == 'EUR'){
    $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 7500, 'regulation');
    }else{
    $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 10000, 'regulation');
    }
    if($country == 'JORDAN')
    {
      if ($coin == 'EUR'){
        $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 1070, 'JORDAN');
      }else{
        $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 1200, 'JORDAN');
      }
    }
    $monthTransactionLimit = Model_SystemSettings::get('month_transaction_limit', 10000, 'regulation');
    $yearTransactionLimit = Model_SystemSettings::get('year_transaction_limit', 200000, 'regulation');

    switch ($_SESSION['sender_country']){
      case "HONG KONG"  :
        if ($coin == 'EUR'){
          $monthTransactionLimit = Model_SystemSettings::get('month_transaction_limit', 5000, 'regulation');
        }
        break;

    }

    // Get usages
    $dailyUsage = Model_MoneyTransferTable::getDailyLimitUsage($senderId,$coin);
    $monthlyUsage = Model_MoneyTransferTable::getMonthlyLimitUsage($senderId);
    $yearlyUsage = Model_MoneyTransferTable::getYealyLimitUsage($senderId);

    // Calculate the current limits
    $monthlyLimit = $monthTransactionLimit - $monthlyUsage;
    $yearlyLimit = $yearTransactionLimit - $yearlyUsage;
    $dailyLimit = $singleTransactionLimit - $dailyUsage;


    // Calculate current global limit
    $currentGlobalLimit = min($dailyLimit, $monthlyLimit, $yearlyLimit);

    return $currentGlobalLimit;
  }
  /**
   * This function calculates a transaction limit per client
   * @param string $senderId the send Id or passport number
   * @param string $receiverId the send Id or passport number
   * @return int the current transaction limit in dollars
   */

  public static function getSenderReceiverTransactionLimit($senderId,$receiverID = 0, $coin,$country,$bank_account)
  {
    if ($bank_account == false){
      $bank_account = '';
    }
    // Get transaction limitations from system settings
    if ($coin == 'EUR'){
    $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', getUsdToEuro(10000), 'regulation');
    $monthTransactionLimit = Model_SystemSettings::get('month_transaction_limit', getUsdToEuro(200000), 'regulation');
    $yearTransactionLimit = Model_SystemSettings::get('year_transaction_limit', getUsdToEuro(200000), 'regulation');
    }else if ($coin == 'HKD'){
    $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 8000, 'regulation');
    $monthTransactionLimit = Model_SystemSettings::get('month_transaction_limit', 8000, 'regulation');
    $yearTransactionLimit = Model_SystemSettings::get('year_transaction_limit', 200000, 'regulation');
    }else{
    $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 10000, 'regulation');
    $monthTransactionLimit = Model_SystemSettings::get('month_transaction_limit', 10000, 'regulation');
    $yearTransactionLimit = Model_SystemSettings::get('year_transaction_limit', 200000, 'regulation');
    }
    if($country == 'JORDAN')
    {
      if ($coin == 'EUR'){
        $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', getUsdToEuro(1200), 'JORDAN');
      }else{
        $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 1200, 'JORDAN');
      }
    }
    //$monthTransactionLimit = Model_SystemSettings::get('month_transaction_limit', getUsdToEuro(200000), 'regulation');
    //$yearTransactionLimit = Model_SystemSettings::get('year_transaction_limit', 200000, 'regulation');

   /* // Get usages  sender
    $dailyUsage = Model_MoneyTransferTable::getDailyLimitUsage($senderId,$coin);
    $monthlyUsage = Model_MoneyTransferTable::getMonthlyLimitUsage($senderId);
    $yearlyUsage = Model_MoneyTransferTable::getYealyLimitUsage($senderId);

    */
    // Get usages  receiver
    $dailyUsage = Model_MoneyTransferTable::getSenderReceiverDailyLimitUsageArr($senderId,$receiverID,$coin);
    $monthlyUsage = Model_MoneyTransferTable::getSenderReceiverMonthlyLimitUsage($senderId,$receiverID,null);
    //$monthlyUsage = Model_MoneyTransferTable::getSenderReceiverMonthlyLimitUsage($senderId,$receiverID);
    $yearlyUsage = Model_MoneyTransferTable::getSenderReceiverYealyLimitUsage($senderId,$receiverID,null);
   // $yearlyUsage = Model_MoneyTransferTable::getSenderReceiverYealyLimitUsage($senderId,$receiverID);



    // Calculate the current limits
    $monthlyLimit = $monthTransactionLimit - $monthlyUsage[0];
    $yearlyLimit  = $yearTransactionLimit   - $yearlyUsage [0];
    $dailyLimit   = $singleTransactionLimit  - $dailyUsage  [0];

// Calculate receiver the current limits
    $monthlyLimitReceiver = $monthTransactionLimit - $monthlyUsage[1];
    $yearlyLimitReceiver  = $yearTransactionLimit   - $yearlyUsage [1];
    $dailyLimitReceiver   = $singleTransactionLimit  - $dailyUsage  [1];


    // Calculate current global limit
    $currentGlobalLimit = min($dailyLimit, $monthlyLimit, $yearlyLimit);
    $currentGlobalLimitReceiver = min($dailyLimitReceiver, $monthlyLimitReceiver, $yearlyLimitReceiver);

   switch ($coin){
     case "HKD":
       // becuase gbg is normalize to usd we need to return the normalized to usd;
       $arr = array(getUSDAmountFromHKD1($currentGlobalLimit),getUSDAmountFromHKD1($currentGlobalLimitReceiver));
       break;
     default:
       $arr = array($currentGlobalLimit,$currentGlobalLimitReceiver);
       break;
   }
    return $arr;
  }

  /**
   * This function calculates a transaction limit per client for payout to israel
   * @param string $senderId the send Id or passport number
   * @return int the current transaction limit in dollars
   */
  public static function getTransactionLimitPayout($senderId,$coin,$country)
  {
    // Get transaction limitations from system settings
    if ($coin == 'EUR'){
      $monthTransactionLimit = Model_SystemSettings::get('single_transaction_limit', getShekelToEuro(50000) /*7500*/, 'regulation');
    }else{
      $monthTransactionLimit = Model_SystemSettings::get('month_transaction_limit', getShekelToUsd(50000) /*10000*/, 'regulation');
    }
    $yearTransactionLimit = Model_SystemSettings::get('year_transaction_limit', 45000, 'regulation');

    // Get usages
    $monthlyUsage = Model_MoneyTransferTable::getMonthlyLimitUsagePayout($senderId,1);
    $twoMonthsUsage = Model_MoneyTransferTable::getMonthlyLimitUsagePayout($senderId,2);
    $sixMonthsUsage = Model_MoneyTransferTable::getMonthlyLimitUsagePayout($senderId,6);
    $yearlyUsage = Model_MoneyTransferTable::getMonthlyLimitUsagePayout($senderId,12);

    // Calculate the current limits
    $monthlyLimit = $monthTransactionLimit - $monthlyUsage;
    $monthlyLimit2 = $monthTransactionLimit - $twoMonthsUsage;
    $monthlyLimit6 = $monthTransactionLimit - $sixMonthsUsage;
   // $yearlyLimit = $yearTransactionLimit - $yearlyUsage;

    $howsRegulation = (abs($monthlyLimit6) > getShekelToUsd(200000) ? "monthlyLimit6" : "monthlyLimit" );
    // Calculate current global limit
    $currentGlobalLimit = min($monthlyLimit,$monthlyLimit6);   //, $yearlyLimit

    return array($currentGlobalLimit,$howsRegulation);
  }


  /**
   * This function calculates a transaction limit per client per day for India transactions
   * @param string $senderId the send Id or passport number
   * @return int the current transaction limit in dollars
   */

  public static function getTransactionLimitIndia($senderId)
  {

    $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 2500, 'INDIA');
    $dailyUsage = Model_MoneyTransferTable::getDailyLimitUsage($senderId);
    $dailyLimit = $singleTransactionLimit - $dailyUsage;

    return $dailyLimit;
  }
  public static function getSenderReceiverTransactionLimitIndia($senderId,$receiverId,$coin= null,$bank_account)
  {
    if ($bank_account == false){
      $bank_account = '';
    }
    if($_SESSION['id_transfer'] === "Royal Pickup"){
      $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 2495, 'INDIA');
    } else{
      $singleTransactionLimit = Model_SystemSettings::get('single_transaction_limit', 10000, 'INDIA');
    }
   // $dailyUsage = Model_MoneyTransferTable::getDailyLimitUsage($senderId);
    //$dailyUsage         = Model_MoneyTransferTable::getSenderReceiverDailyLimitUsageArr($senderId,$receiverId);
    $monthlyUsage         = Model_MoneyTransferTable::getIndiaSenderReceiverMonthlyLimitUsage($senderId,$receiverId,null,$bank_account);
    $monthlyLimit         = $singleTransactionLimit - $monthlyUsage[0];
    $monthlyLimitReceiver = $singleTransactionLimit - $monthlyUsage[1];

    $monthlyLimit = array($monthlyLimit,$monthlyLimitReceiver);
    return $monthlyLimit;
  }
  public static function getCountTransactionLimitIndia($senderId,$receiverId,$coin= null,$bank_account)
  {
    if ($bank_account == false){
      $bank_account = '';
    }

    $trasactionLimit = 30;
    $monthlyUsage         = Model_MoneyTransferTable::getIndiaMonthlyLimitCount($senderId,$receiverId,null,$bank_account);
    $monthlyLimit = $trasactionLimit - $monthlyUsage[0];
    $monthlyLimitReceiver = $trasactionLimit - $monthlyUsage[1];

    return array($monthlyLimit,$monthlyLimitReceiver);
  }

  public static function getBankAccountTransactionLimit($bankName,$bankAccountNumber,$coin= null)
  {

    $singleTransactionLimit = 10000;//Model_SystemSettings::get('single_transaction_limit', 10000, 'INDIA');

    $monthlyUsage         = Model_MoneyTransferTable::getBankAccountTransactionMonthlyLimitUsage($bankName,$bankAccountNumber);
    $monthlyLimit         = $singleTransactionLimit - $monthlyUsage;

    return $monthlyLimit;
  }


}

function getUsdToEuro($amount)
{
  $conn = Model_ConnectionManager::getConnection();

  $res = mysql_query("SELECT * FROM  branches WHERE rate_nis_update='auto' and rate_eur_update='auto' LIMIT 1");
  $row = mysql_fetch_assoc($res);
  $rateEurUsd = $row['rate_nis']/$row['rate_eur'];
  return $amount * $rateEurUsd;

}

function getShekelToUsd($amount)
{
  $conn = Model_ConnectionManager::getConnection();

  $res = mysql_query("SELECT * FROM  branches WHERE rate_nis_update='auto' and rate_eur_update='auto' LIMIT 1");
  $row = mysql_fetch_assoc($res);
  $rateEurUsd = $row['rate_nis'];
  return $amount / $rateEurUsd;

}

function getShekelToEuro($amount)
{
  $conn = Model_ConnectionManager::getConnection();

  $res = mysql_query("SELECT * FROM  branches WHERE rate_nis_update='auto' and rate_eur_update='auto' LIMIT 1");
  $row = mysql_fetch_assoc($res);
  $rateEurUsd = $row['rate_eur'];
  return $amount / $rateEurUsd;

}


function getUSDAmountFromHKD1($amount)
{
  $con = wic_db_connect();
  // Getting the Rates from DB
  $res = mysql_query("SELECT *   FROM  `branches` WHERE  `branch_name` LIKE  'h.k'");
  $row = mysql_fetch_assoc($res);
  $rateHkdUsd = $row['rate_hkd'] ;
  return $amount / $rateHkdUsd;

}

?>
