<?php


/**
 * Function checkIfClientIpCanEnter
 *
 * Check if the user can enter the system
 * with his IP address (from the branch IPs list)
 *
 * @return int - 1: YES, 2: NO
 */
function checkIfClientIpCanEnter($branchNum){
    /* Check if the IP address is allowed to enter the website */
    $sqlResult =  mysql_fetch_array(mysql_query('SELECT ip_address FROM branches WHERE id_num=' . $branchNum));
    $ipArray = explode(",", $sqlResult['ip_address']);
    $ip = getClientIp();

    for ($i = 0; $i < count($ipArray); $i++){
        if ($ipArray[$i] == '' || $ipArray[$i] == $ip) {
            return 1;
        }
    }
    return 0;
}
function checkIfClientIpHK(){
    /* Check if the IP address is allowed to enter the website */
    $sqlResult =  mysql_fetch_array(mysql_query('SELECT ip_address FROM branches WHERE id_num= 775'));
    $ipArray = explode(",", $sqlResult['ip_address']);
    $ip = getClientIp();

    for ($i = 0; $i < count($ipArray); $i++){
        if ($ipArray[$i] == $ip) {
            return 1;
        }
    }
    return 0;
}


/**
 * Function getClientIp
 *
 * The function get the client IP address
 * Don't ask me how I just took it from StackOverflow..
 *
 * @return string - the ip address
 */
function getClientIp() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
