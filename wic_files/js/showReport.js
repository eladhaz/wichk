/// build the filter by coin select drop list.


var tableFlag = false;




function buildAutonomyCoins(coinArr){
  var arr = coinArr.split(",");
  var html = "";
  for (var i =0; i < arr.length ; i++){
    html+= " <option value='" + arr[i] + "'>" + arr[i] + "</option>"
  }
  console.log(html);
  $("#id_currency").html(html);
}

$("#bookkeepingGeneralUpdatedWicStore").on("click",".update",function(){
  var comment = $(this).closest("tr").find("textarea").val();

  loadPopupBox(comment);
});


//hk hong kong payment

var $loading = $('#loadingDiv').hide();
$(document)

  .ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });


//-------- Hong Kong Payment upload excel wwith transactions by AJAX
$("#HongKongPaymentForm").submit( hongKongPayment );


function hongKongPayment (e){
  e.preventDefault();
  var postData = new FormData($(this)[0]);
  $.ajax({
    url: "reports/hongKongPayment.php",
    data:  postData,
    type: 'POST',
    contentType: false,
    processData: false
  }).success(function (response) {
      $("#report").text("");
      var json = JSON.parse(response);
      var action = json[0].action;
      switch (action){
        case "show":
          delete  json[0];
          var jsonAgain = JSON.stringify(json) ;
          jsonAgain = jsonAgain.replace("null,","");
          buildTableFromJason(jsonAgain,$("#report"));
          break;
        case "upload":
          var res = json.result;
          loadPopupBox(res);
          break;
      }

    }
  )
}


//-------- fixed headers-----
$.fn.dataTableExt.sErrMode = 'throw'
function fixHeader() {
  var iStart = new Date().getTime();
  var oTable = $('.table').dataTable(
    {
      "sScrollY": "400px",
      "sScrollX": "85vw",
      "sScrollXInner": "89px",
      "bScrollCollapse": true,
      "bPaginate": false,
      "bFilter": false ,
      "autoWidth": false
    });
  new FixedColumns(oTable,
    {
      "sHeightMatch": "none"
    });

}
function fixIT(){
  var row = $("tbody > tr td")
}

function makeISSMaller(){
  $("table tbody tr td:nth-child(1)") .css("max-width","49px");
  $("table tbody tr td:nth-child(2)") .css("max-width","49px");
  $(".sorting:nth-child(1)") .css("max-width","49px");
  $(".sorting:nth-child(2)") .css("max-width","49px");
}
//-----------------------
var nav = $("li");

//---------------listener for nav change;
nav.on("click", function () {
    var me = $(this).find("a").attr("name");
//      if (me == "#thailandadmin"){
//              window.location.replace("thailandadmin.php");
//          }
    if (me == "#thailand_transaction") {
      window.location.replace("thailand_transaction.php");
    }
    nav.removeClass("active");
    $(this).addClass("active");
    $('.reportTable').html("")
    $($('[name="report"]')).hide();

    $(me).show();
    $("#report").html("");
    $("#reportExcel").html("");
    xajax_report_single_page_application(me);
  }
)


//------- checks report-------------------
$("#showCheckPaymentReport, #searchCheckReport , #searchCheckBankAccount").on("click",function(){
  var name  = "showCheckPaymentReport" //this.name;
  var callback ="";// loadPopupBox("load it from the callback");
  submitFormElement("Server/reportsServer.php",$("form"),"action="+name,tableIt ) ;
  return false;
});

//------- transaction report-------------------
$("#reportTest").on("click", function () {
    var name = "reportTest" //this.name;
    var callback = "";// loadPopupBox("load it from the callback");
    show('loading', true);
    submitFormElement("../../Server/reportsServer.php", $("form"), "action=" + name, tableIt, 'true', 1);
    return false;
});

//----------- bookkeepping HK autonomy
$("#bookkeepingHKForm :submit").on("click",function(){
  var name  = this.name;   /// there to types of submit button.. one for make excel and second make table
  var callback = (name === "bookkeepingHkExcel" ? getBooggeepingExcel : printTableFromServer );
  submitFormElement("Server/reportsServer.php",$("#bookkeepingHKForm"),"action="+name,callback) ;
  return false;
});
$("#bookkeepingMonthHKForm :submit").on("click",function(){
  var name  = this.name;
  var callback = (name === "bookkeepingMonthHkExcel" ? getBooggeepingExcel : printTableFromServer );
  submitFormElement("Server/reportsServer.php",$("#bookkeepingMonthHKForm"),"action="+name,callback) ;
  return false;
});





function getBooggeepingExcel(path){
  console.log(path);
  window.open(window.location.href = path);

}

function printTableFromServer(data){
  console.log(data);
   $("#report").html(data);
}

//-----------regulation report---------------------------------
$(document).on("click","#regulationReport", function () {
  $("#downloadRegulation").text("");

  var from_year    = $("#id_from_year").val();
  var from_month   = $("#id_from_month").val();
  /*var from_day     = $("#id_from_day").val();
   var to_year  = $("#id_to_year").val();
   var to_month = $("#id_to_month").val();
   var to_day   = $("#id_to_day").val();*/
  var from_date = from_year + "-" + from_month ;//+ "-" + from_day;
  //var to_date = to_year + "-" + to_month + "-" + to_day;

  xajax_callRegulationReport(from_date);

});
// regulation for HK!
$(document).on("click","#regulationReportHK", function () {
  $("#downloadRegulation").text("");

  var year    = $("#id_from_year").val();
  var querter   = $("#id_from_month").val();

  xajax_callRegulationReportHK(querter,year);

});

$("#report_single_page").on("click","#regulationReportDownloadAll",function(){
  var downLoadAll = document.getElementsByClassName("downloadIT");
  for(var i = 0 ; i < downLoadAll.length ; i++){
    downLoadAll[i].click();
  }
});

$("#report_single_page").on("click","#downloadRegulation a",function(){
  $(this).css({"color":"black","text-decoration":"none"});
});


// -----------transdaction report  table  ------------------------------
$(document).on("click", '.transaction_report_table tbody tr', function (e) {
    var flag = true;
    var inputString = $(this).find('td:nth-child(4)').text();     ///gives the transdaction id;
    inputString = inputString.replace(/\s/g, "");
    var newRow = inputString + "_tr";// id+_tr the row;
    // id_details the more details new row;
    var className = $(this).attr('class');

    if (typeof className != 'undefined') {
        if (className.indexOf("selected") > -1) {
            $("#" + newRow).remove();
            $(this).removeClass("selected");
            flag = false;
        }

    }
    if (flag) {
        // drop dwon more details;
        $(this).addClass("selected");
        $(this).after("<tr class='selected_tr' id='" + inputString + "_tr' ><td class='selected_details' colspan='16' id='" + inputString + "_details'></td> </tr>");
        console.log(inputString);
        if (inputString != '') {
            xajax_transactions_report_details(inputString);
        }
    }

});


/*$(document).on('click', '#id_agent', function () {

  xajax_users_branch(this.value);
})*/;

$(document).on('click', '#id_sender_country', function () {
  console.log(this.value);
  if (this.value == 'ISRAEL' || this.value == 'HONG KONG'){
    xajax_receiver_country(this.value);

  }
});


function reportChange(page, coin, flag) {
    $("#page").val(page);
    console.log("the page is:" + page);
    $("#excel").val("false");
    $("#search").val("");
    if (flag == 'report1')
        transactions_report(coin, page);
    else
        transactions_report_test(coin, page);
}

function reportExcel() {
  $("#excel").val("true");
  $("#search").val("");
  transactions_report();
}

$(document).on("click", "#searchReport", function () {
  transactions_report();
});
$(document).on("click", "#report1", function () {
    debugger;
    var coin = $("#id_currency :selected").val();
    reportChange('1', coin, 'report1');
    //window.open("http://www.w3schools.com");
})
$(document).on("click", "#report2", function () {
  reportExcel();
})

$(document).on("click", "#searchBankAccount", function () {
  xajax_transactions_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_receiver_country').value
    , document.getElementById('id_sender_country').value, document.getElementById('id_user').value
    , document.getElementById('id_agent').value, document.getElementById('id_status').value, document.getElementById('id_transfer_via').value, document.getElementById('id_currency').value, document.getElementById('page').value, document.getElementById('excel').value, document.getElementById('search').value = "", document.getElementById('bankAccount').value);
})


$(document).on("click", '#thailand_transaction', function thailand_transaction() {

  xajax_thailand_transaction(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value);
})


$(document).on("click", ".update", function updatdteOldTransactionComents() {

  var transactionID = this.value;
  var name =  "address."+transactionID;
  var  textArerat = $("[name='"+name+"']").val();
  xajax_updateWicStoreSubmit(transactionID, textArerat);
})


function transactions_report(coin, page) {
// postIt("Server/reportsServer.php","","action=getAutonomyCoins&coin="+coin,buildAutonomyCoins);


    xajax_transactions_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_receiver_country').value
        , document.getElementById('id_sender_country').value, document.getElementById('id_user').value
        , document.getElementById('id_agent').value, document.getElementById('id_status').value, document.getElementById('id_transfer_via').value, document.getElementById('id_currency').value, document.getElementById('page').value, document.getElementById('excel').value, document.getElementById('search').value, document.getElementById('bankAccount').value = "");


    var name = "report1" //this.name;
    var callback = "";// loadPopupBox("load it from the callback");
    //show('loading',true);
    //ssubmitFormElement("Server/reportsServer.php",$("form"),"action="+name,tableIt, 'false',page) ;
    return false;
}




//--------------------------------------------------
//------------------easypay--------------------------
// easypay button click
$(document).on("click", '#easypay_report', function easypay_report() {
  xajax_easypay_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_receiver_country').value
    , document.getElementById('id_agent').value, document.getElementById('id_user').value, document.getElementById('id_status').value, document.getElementById('id_transfer_via').value, null);
  //, '<?php echo $_SESSION['myusername']; ?>',
});


// -----------------------------------------------------


//---------------------   --------------------------------------
$("#report_single_page").on("click", "#currency_report_button", function currency_report() {


  var id_branch = $("#id_agent").val();
  var id_clerk = $("#id_user").val();
  var id_status = $("#id_status").val();
  xajax_payout_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_receiver_country').value,
    document.getElementById('id_transfer_via').value, id_clerk, id_status, id_branch);
  /*document.getElementById('id_status').value*/
})

//---------------------  currency_report --------------------------------------
$("#report_single_page").on("click", "#currency_button", function currency_report() {
  xajax_currency_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value);

})


//-----------#payout_report_priority------------------------------------------------------
$(document).on("click", '#payout_report_priority', function cash_report() {
  xajax_payout_pariority_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_agent').value);

});
//----------------------------------------------------------------------------

//-----------cash report------------------------------------------------------
$(document).on("click", '#cash_report', function cash_report() {

  xajax_cash_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_receiver_country').value, document.getElementById('id_sender_country').value
    , document.getElementById('id_agent').value, document.getElementById('id_user').value, document.getElementById('id_status').value, document.getElementById('id_transfer_via').value, document.getElementById('id_currency').value);
});

/*
 xajax_payout_report(document.getElementById('id_from_day').value,document.getElementById('id_from_month').value,document.getElementById('id_from_year').value,document.getElementById('id_to_day').value,document.getElementById('id_to_month').value,document.getElementById('id_to_year').value, document.getElementById('id_receiver_country').value,
 document.getElementById('id_transfer_via').value,document.getElementById('id_agent').value,document.getElementById('id_status').value);*/


function XLExport() {
  var i;
  var j;
  var mycell;
  var tableID = "reporttab";

  var objXL = new ActiveXObject("Excel.Application");
  var objWB = objXL.Workbooks.Add();
  var objWS = objWB.ActiveSheet;

  for (i = 0; i < document.getElementById(tableID).rows.length; i++) {
    for (j = 0; j < document.getElementById(tableID).rows(i).cells.length; j++) {
      mycell = document.getElementById(tableID).rows(i).cells(j)
      objWS.Cells(i + 1, j + 1).Value = mycell.innerText;


    }
  }

//objWS.Range("A1", "L1").Font.Bold = true;

  objWS.Range("A1", "Z1").EntireColumn.AutoFit();

//objWS.Range("C1", "C1").ColumnWidth = 50;

  objXL.Visible = true;

}
//----------------------------------------------------------------------------

$(document).on('click', '#transactionReportPrint', function(){
  console.log("bla-bla2");
  $('th').css('font-size', '8px');
  $('th').css('line-height', '8px');
  $('td').css('font-size', '8px');
  $('td').css('line-height', '8px');
  $('.border_left').children().children().css('height', '20');
  $('.print-preview').printPreview();
});

//-------------------------------------  bookkeepingUpdatedReportWicStore
$(document).on("click", "#bookkeepingGeneralReportWicStore_button, #bookkeepingGeneralReportWicStore_buttonAcount ", function bookkeepingUpdatedReportWicStore() {
  console.log("bookkeepingGeneralReportWicStore_button");
  xajax_bookkeepingGeneralReportWicStore(
    document.getElementById('id_from_day').value,
    document.getElementById('id_from_month').value,
    document.getElementById('id_from_year').value,
    document.getElementById('id_to_day').value,
    document.getElementById('id_to_month').value,
    document.getElementById('id_to_year').value,
    document.getElementById('id_receiver_country').value,
    document.getElementById('id_status').value,
    document.getElementById('id_currency').value,
    document.getElementById('id_transfer_via').value,
    document.getElementById('bankAccount').value);
})
//------------------------------------ bookkeepingUpdatedReportAgents
$(document).on("click", "#bookkeepingUpdatedReportWicStore", function bookkeepingUpdatedReportAgents() {
  //two
  xajax_bookkeepingGeneralUpdatedWicStore(
    document.getElementById('id_to_day').value,
    document.getElementById('id_to_month').value,
    document.getElementById('id_to_year').value,
    document.getElementById('id_receiver_country').value,
    document.getElementById('id_status').value,
    document.getElementById('id_currency').value,
    document.getElementById('id_transfer_via').value,
    document.getElementById('bankAccount').value);
})


//--------------------------------------------------------------
//---------------------------bookkeepingGeneralReportAgents-------------->


$(document).on("click", "#bookkeepingGeneralReportAgents", function bookkeepingGeneralReportAgents() {
  xajax_bookkeepingGeneralReportAgents(
    document.getElementById('id_from_day').value,
    document.getElementById('id_from_month').value,
    document.getElementById('id_from_year').value,
    document.getElementById('id_to_day').value,
    document.getElementById('id_to_month').value,
    document.getElementById('id_to_year').value,
    document.getElementById('id_receiver_country').value,
    document.getElementById('id_status').value,
    document.getElementById('id_currency').value,
      document.getElementById('id_transfer_via').value);
});
//-------------------------------------------------------------------------
//--------------------------- bookkeepingGeneralUpdatedAgents -----------------
$(document).on("click", "#bookkeepingUpdatedReportAgents", function bookkeepingGeneralUpdatedAgents() {
  //one
  xajax_bookkeepingGeneralUpdatedAgents(
    document.getElementById('id_to_day').value,
    document.getElementById('id_to_month').value,
    document.getElementById('id_to_year').value,
    document.getElementById('id_receiver_country').value,
    document.getElementById('id_status').value,
    document.getElementById('id_currency').value,
      document.getElementById('id_transfer_via').value,
    document.getElementById('bankAccount').value)
});
//-------------------------------------------------------------------------------

//-----------------------------GBG report ------------------------------------

$(document).on("click", "#gbg_report_button", function () {
  xajax_gbg_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value);

})


function gbg_report() {
  xajax_gbg_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value);
  //, '<?php echo $_SESSION['myusername']; ?>',
}
//-----------------------------------------------------------------------------
// -----------------------payout report----------------------------------------
function currency_report() {
  if ("<?php echo $_GET['praiority'] ?>" != "p") {
    xajax_payout_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_receiver_country').value,
      document.getElementById('id_transfer_via').value, document.getElementById('id_agent').value, document.getElementById('id_status').value);
  } else {
    xajax_payout_pariority_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_agent').value);
  }
}
//--------------------------------------------------------------------------------


//--------------bookkeepingGeneralReportWicStore -------------------------------------
/*$(document).on("click", "#bookkeepingGeneralReportWicStore_button", function () {
  xajax_bookkeepingGeneralReportWicStore(document.getElementById('id_from_day').value,
    document.getElementById('id_from_month').value,
    document.getElementById('id_from_year').value,
    document.getElementById('id_to_day').value,
    document.getElementById('id_to_month').value,
    document.getElementById('id_to_year').value,
    document.getElementById('id_receiver_country').value,
    document.getElementById('id_status').value,
    document.getElementById('id_currency').value);

})*/

//-------------------------------------------------------------------------------------


//------------------------------vip report----------------------------------------
function reportVipChange() {
  //var num = $("#vip_page").val();
  xajax_vip_report(1);
}
$(document).on("click", "#vip_report_button", function reportVipChange() {
  var num = $("#vip_page").val();
  xajax_vip_report(num);
})

function Excel() {
  $.ajax({
    url: "report_vip_excel.php",
    data: {},
    type: 'post',
    success: function (output) {

    }
  });
}
function report_vip_exel() {
  var page = 'report_vip_exel.php';
  $.ajax({
    url: page,
    data: {},
    type: 'POST',
    success: function (e) {
      console.log(e);
    },
    error: function (e) {
      console.log(e);
      alert("booo");
    }
  })
}


//--------------------------------------------------------------------------------
$(document).ready(function () {
  /* setTimeout(function(){
   xajax_sessionShoutDown();
   }, 10);

   // if the user is bookeeping start with payout report;
   if ("
   <?php echo $_SESSION['branch']; ?>" == "bookkeeping") {
   xajax_report_single_page_application("#cash_report");
   } else if ("
   <?php echo $_SESSION['myusername']; ?>" === "kay") {
   xajax_report_single_page_application("#chinaReport");
   } else {
   // else start with transdaction report;
   xajax_report_single_page_application("#transaction_report");
   }
   */

  $(document).on('click', '.fancybox', function () {
    $.fancybox.open({
      href: 'showtransaction.php?id=' + $(this).attr("tid"),
      type: 'iframe',
      padding: 5,
      width: 1100
    });

  });
});

$('#report_single_page').on('click', '#payoutSearchReport', function () {
  xajax_payoutSearchButton($('#payoutSearch').val());
});

$('#report_single_page').on('click', '#searchReport5', function () {
  xajax_inc_book($('#id_from_day').val(), $('#id_from_month').val(), $('#id_from_year').val(), $('#id_to_day').val(),
    $('#id_to_month').val(), $('#id_to_year').val(), $('#search').val());
});

$('#report_single_page').on('click', '#report5', function () {
  xajax_inc_book($('#id_from_day').val(), $('#id_from_month').val(), $('#id_from_year').val(), $('#id_to_day').val(),
    $('#id_to_month').val(), $('#id_to_year').val(), $('#search').val());
});

$('#report_single_page').on('click', '#report4', function () {
  xajax_report_inc_china($('#id_from_day').val(), $('#id_from_month').val(), $('#id_from_year').val(), $('#id_to_day').val(),
    $('#id_to_month').val(), $('#id_to_year').val(), $('#id_transfer_via').val(), $('#id_status').val(), $('#id_currency').val());
});

$("#report").on('click', '#SubmitB', function () {

  /* var id = $(this).val();
   var name = $(this).parent().parent().children().eq(6)[0].innerHTML;
   var num = $(this).parent().parent().children().eq(5).children()[0].value;
   var name_local = $(this).parent().parent().children().eq(7).children()[0].value;
   var phone = $(this).parent().parent().children().eq(8).children()[0].value;
   var branch = $(this).parent().parent().children().eq(13).children()[0].value;
   var address = $(this).parent().parent().children().eq(15).children()[0].value;
   console.log(address);
   var bank = $(this).parent().parent().children().eq(11).children()[0].value;
   xajax_update_china(id, num, name_local, bank, name, phone, branch, address);*/

  /* var id = $(this).val();
   var name         = $(this).parents("tr").find(".name").innerHTML;
   var num          = $(this).parents("tr").find(".num").innerHTML;
   var name_local   = $(this).parents("tr").find(".name_local")   .innerHTML;
   var phone        = $(this).parents("tr").find(".phone")        .innerHTML;
   var branch       = $(this).parents("tr").find(".branch")       .innerHTML;
   var address      = $(this).parents("tr").find(".address")      .innerHTML;
   console.log(address);
   var bank = $(this).parent().parent().children().find(".bank").innerHTML;
   xajax_update_china(id, num, name_local, bank, name, phone, branch, address);
   */



  var id = $(this).val();
  var name = $(this).parent().parent().children().eq(7)[0].innerHTML;
  var num = $(this).parent().parent().children().eq(6).children()[0].value;
  var name_local = $(this).parent().parent().children().eq(8).children()[0].value;
  var phone = $(this).parent().parent().children().eq(9).children()[0].value;
  var branch = $(this).parent().parent().children().eq(14).children()[0].value;
  var address = $(this).parent().parent().children().eq(16).children()[0].value;

  var bank = $(this).parent().parent().children().eq(12).children()[0].value;

  console.log(address);
  xajax_update_china(id, num, name_local, bank, name, phone, branch, address);

});

$("#report").on('click', "#SubmitC", function () {
  var id = $(this).val();
  xajax_update_china_status(id);
});

