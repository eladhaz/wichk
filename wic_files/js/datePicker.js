/**
 * Created by user on 8/3/2016.
 */

var datePicker = (function () {

    var from, to;
    var date = new Date();

    from = document.createElement('div');
    to = document.createElement('div');
   // from.setAttribute('class','col-sm-5');
   // to.setAttribute('class','col-sm-5');


    dayMaker = function (id) {
        var day = date.getUTCDate();
        var selectDay = document.createElement('select');
        selectDay.setAttribute('id',id);
        selectDay.setAttribute('name',id);
        selectDay.setAttribute('class','selectWidth1');
        for(var i = 1 ; i<=31;i++){
          var option = document.createElement('option');
          option.setAttribute('value',i);
            option.innerHTML=i;
            if(day==i){
                option.setAttribute('selected','selected');
            }
            selectDay.appendChild(option);
      }
        return selectDay;
        
    };

    monthMaker = function (id) {
        var month = date.getUTCMonth();
        var selectMonth = document.createElement('select');
        selectMonth.setAttribute('id',id);
        selectMonth.setAttribute('name',id);
        selectMonth.setAttribute('class','selectWidth2');
        for(var i = 1 ; i<=12;i++){
            var option = document.createElement('option');
            option.setAttribute('value',i);
            option.innerHTML=i;
            if(month==i+1){
                option.setAttribute('selected','selected');
            }
            selectMonth.appendChild(option);
        }
        return selectMonth;
    };

    yearMaker = function (id) {
        var year = date.getUTCFullYear();
        var selectYear = document.createElement('select');
        selectYear.setAttribute('id',id);
        selectYear.setAttribute('name',id);
        selectYear.setAttribute('class','selectWidth2');
        for(var i = 2009 ; i<=date.getFullYear();i++){
            var option = document.createElement('option');
            option.setAttribute('value',i);
            option.innerHTML=i;
            if(year==i){
                option.setAttribute('selected','selected');
            }
            selectYear.appendChild(option);
        }
        return selectYear;
    };

    receivingCountries = function (countries){
        var selectRecevingCountry = document.createElement('select');
        selectRecevingCountry.setAttribute('id','id_receiver_country');
        selectRecevingCountry.setAttribute('name','id_receiver_country');
        selectRecevingCountry.setAttribute('class','form-control selectWidth');
        var option = document.createElement('option');
        option.setAttribute('value','all');
        option.innerHTML='ALL';
        option.setAttribute('selected','selected');
        selectRecevingCountry.appendChild(option);
        for(var i=0;i<countries.length;i++){
            var option = document.createElement('option');
            option.setAttribute('value',countries[i]);
            option.innerHTML=countries[i];
            selectRecevingCountry.appendChild(option);
        }
        return selectRecevingCountry;

    };

    transferTypesList = function (transferTypes){
        var selectTransferTypes = document.createElement('select');
        selectTransferTypes.setAttribute('id','id_transfer_via');
        selectTransferTypes.setAttribute('name','id_transfer_via');
        selectTransferTypes.setAttribute('class','form-control selectWidth');
        var option = document.createElement('option');
        option.setAttribute('value','all');
        option.innerHTML='ALL';
        option.setAttribute('selected','selected');
        selectTransferTypes.appendChild(option);
        for(var i=0;i<transferTypes.length;i++){
            var option = document.createElement('option');
            option.setAttribute('value',transferTypes[i]);
            option.innerHTML=transferTypes[i];
            selectTransferTypes.appendChild(option);
        }
        return selectTransferTypes;

    };

    senderCountries = function (countries){
        var selectSenderCountries = document.createElement('select');
        selectSenderCountries.setAttribute('id','id_sender_country');
        selectSenderCountries.setAttribute('name','id_sender_country');
        if(countries.length>1){
            var option = document.createElement('option');
            option.setAttribute('value','ALL');
            option.innerHTML='ALL';
            option.setAttribute('selected','selected');
            selectSenderCountries.appendChild(option);
        }
        for(var i=0;i<countries.length;i++){
            var option = document.createElement('option');
            option.setAttribute('value',Object.keys(countries[i])[0]);
            option.innerHTML=countries[i][Object.keys(countries[i])[0]];
            selectSenderCountries.appendChild(option);
        }
        return selectSenderCountries;
    };

    agentsFilter = function (agents) {
        var selectAgents = document.createElement('select');
        selectAgents.setAttribute('id','id_agent');
        selectAgents.setAttribute('name','id_agent');
        var option = document.createElement('option');
        option.setAttribute('value','all-(active and archived)');
        option.innerHTML='All-(active and archived)';
        option.setAttribute('selected','selected');
        selectAgents.appendChild(option);
        for(var i=0;i<agents.length;i++){
            var option = document.createElement('option');
            option.setAttribute('value', Object.keys(agents[i])[0]);
            option.innerHTML=agents[i][Object.keys(agents[i])[0]];
            selectAgents.appendChild(option);
        }
        return selectAgents;

    };
    
    clerksFilter = function (clerks) {
        var selectClerks = document.createElement('select');
        selectClerks.setAttribute('id','id_user');
        selectClerks.setAttribute('name','id_user');
        var option = document.createElement('option');
        option.setAttribute('value','all-(active and archived)');
        option.innerHTML='All-(active and archived)';
        option.setAttribute('selected','selected');
        selectClerks.appendChild(option);
        for(var i=0;i<clerks.length;i++){
            var option = document.createElement('option');
            option.setAttribute('value', Object.keys(clerks[i])[0]);
            option.innerHTML=clerks[i][Object.keys(clerks[i])[0]];
            selectClerks.appendChild(option);
        }
        return selectClerks;
    };

    hiddenElement = function (name, value) {
        var input = document.createElement('input');
        input.setAttribute('name',name);
        input.setAttribute('type','hidden');
        input.setAttribute('id',name);
        input.setAttribute('value',value);
        return input;
    };

    statusList = function (status) {
        var selectStatus = document.createElement('select');
        selectStatus.setAttribute('id','id_status');
        selectStatus.setAttribute('name','id_status');
        selectStatus.setAttribute('class','form-control selectWidth');
        var option = document.createElement('option');
        option.setAttribute('value','all');
        option.innerHTML='ALL';
        option.setAttribute('selected','selected');
        selectStatus.appendChild(option);
        for(var i=0;i<status.length;i++){
            var option = document.createElement('option');
            option.setAttribute('value', status[i]);
            option.innerHTML= status[i];
            selectStatus.appendChild(option);
        }
        return selectStatus;
    };

    currenciesList = function (currencies) {
        var currenciesList = document.createElement('select');
        currenciesList.setAttribute('id', 'id_currency');
        currenciesList.setAttribute('name', 'id_currency');
        var keys = Object.keys(currencies);
        for (var i = 0; i <Object.keys(currencies).length; i++) {
            var option = document.createElement('option');
            option.setAttribute('value', keys[i]);
            option.innerHTML= keys[i];
            currenciesList.appendChild(option);
        }
        return currenciesList;
    };

    return {

        generateFromDate: function () {
            from.appendChild(dayMaker('id_from_day'));
            from.appendChild(monthMaker('id_from_month'));
            from.appendChild(yearMaker('id_from_year'));
            return from;
        },
        generateToDate: function () {
            to.appendChild(dayMaker('id_to_day'));
            to.appendChild(monthMaker('id_to_month'));
            to.appendChild(yearMaker('id_to_year'));
            return to;

        },
        generateReceivingCountries: function (countries) {
            return receivingCountries(countries);
        },
        generateAgentsSelection: function (agents) {
            return agentsFilter(agents);
        },
        generateTransferTypes: function (transferTypes) {
            return transferTypesList(transferTypes);
        },
        generateClerksSelection: function (clerks) {
            return clerksFilter(clerks);
        },
        generateHiddenInput: function (name,value) {
            return hiddenElement(name,value);
        },
        generateSenderCountries: function (countries) {
            return senderCountries(countries);
        },
        generateStatusSelection: function (status) {
            return statusList(status);
        },
        generateCurrencies: function (currencies) {
            return currenciesList(currencies);
        }
    };

})();
