/**
 * Created by zlil on 6/13/2016.
 */
/** building  generic table from json
 *   and  print it in id="report"
 *
 * @param json - json contains head,body,footer
 */


function tableIt(json) {

    var obj = JSON.parse(json);
    obj = obj.result;
    var header = obj.header;
    var body = obj.body;
    var footer = obj.footer;

    var table = document.createElement("table");
    table.className = obj.tableClass;

    var thead = table.thead;
    /*    var heads = jsonLineMove(header, "th", "thead");
     var bodys = jsonLineMove(body, "td", "tbody");
     var footers = jsonLineMove(footer, "td", "tbody");

     table.appendChild(heads);
     table.appendChild(bodys);
     table.appendChild(footers);*/

    //keep this to use the previous version of the report if necessary
    // $("#report").html(table);
    /*    var branchName = obj.user[0]['branch']['value'];*/

    var data = [];
    var getNumForFooter;

    var userName = obj.user;


    for (var i = 0; i < obj.body.length; i++) {

        var no = obj.body[i]["No"];

        data.push(obj.body[i]);

        if (i == obj.body.length - 1) {
            getNumForFooter = parseInt(no) + 1;
        }

    }


    setFooterContent(obj.footer, data, getNumForFooter);

    var columns = JSON.parse(obj.columns);
    if (!$.isEmptyObject(obj.headerInfo)) {
    var headingInfo = JSON.parse(obj.headerInfo);
    }
    var paging = obj.footer[2]["pages"]["value"].toString();
    //set "kind of footer" to the table to keep paging under the table
    var sizeForFooter = obj.columns.length;
    if (body.length != 0) {

        addFooterelement(sizeForFooter);
        
        var table = $("#ddtable").DataTable({


            data: data,
            //deferRender: true,
            columns: columns,
            "bAutoWidth": false,
            order: [[0, "asc"]],
            fixedHeader: true,
            alwaysCloneTop: true,
            scrollY: "500px",
            scrollX: true,
            scrollCollapse: true,
            select: true,
            "bDestroy": true,
            //"asStripeClasses": [ 'witheish2', 'yellowish2', 'greenish2' ],

            fnFooterCallback: function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var api = this.api();
                var totalAmount = 0;
                var totalCommission = 0;
                aaData.forEach(function (x) {
                    if (x['Status'] != 'FAILED') {
                        totalAmount += parseInt(x['Amount']);
                        totalCommission += parseInt(x['Commission']);
                    }
                });
                //$(api.column(10).footer()).html('<p style="text-align: left; margin-right: 10px">' + totalAmount + '</p>');
                //$(api.column(11).footer()).html(totalCommission);
                $(api.column(0).footer()).html('<div style="margin-top : 10px;">' + paging + '</div>');
            },

            //set the row background to red color is the status of the transaction is failed
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData['Status'] == 'FAILED' || aData['Status'] == 'ABORTED') {
                    $(nRow).addClass('redIT');
                    $(nRow).css('background', '#FF706E');
                }
            },
            //turn off the selection menu of page size
            bLengthChange: false,
            //set each page to display 20 results
            pageLength: 20,
            //turn off the paging option
            paging: false,
            //turn off the "showing x to y entries" option in the bottom of the table
            bInfo: false,


        });
        /*        table.columns().iterator( 'column', function (ctx, idx) {
         $( table.column(idx).header() ).append('<span class="sort-icon"/>');
         } );*/


        removeRowHeader();
        addInfo();
        clearAdditionInfoButton();
       // addRowHeader(userName);
        addRowHeaders(headingInfo);
        changeFailedColor();
        $('tbody').attr('align', 'center');
        $('thead').attr('align', 'center');
    }

    //setFirstCoulmnWidth();
    show('loading', false);
    $('.sorting_asc').css('text-align', 'center');
    $('.sorting').css('text-align', 'center');
    tableFlag = true;


    /*this inner function is to generate the child rows (details rows) for each main row -> the + and - icons on each row in the table open the child row*/
    function addInfo() {
        // Array to track the ids of the details displayed rows
        var detailRows = [];

        $('#ddtable tbody').on('click', 'tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var index = row.index();
            var idx = $.inArray(tr.attr('id'), detailRows);

            if (row.child.isShown()) {
                tr.removeClass('details');
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice(idx, 1);
            }
            else {
                tr.addClass('details');
                //row.child(format(row.data()["No"])).show();
                row.child(format(index , obj.statusChange[index]['0'])).show();
                addOptionsToNewStatus(obj.statusChange[index]);
                // Add to the 'open' array
                if (idx === -1) {
                    detailRows.push(tr.attr('id'));
                }
            }

        });

        // On each draw, loop over the `detailRows` array and show any child rows
        table.on('draw', function () {
            $.each(detailRows, function (i, id) {
                $('#' + id + ' td.details-control').trigger('click');
            });
        });
    }

    /*this function get called from the addInfo Function,
     this inner function is to generate the information inside the
     child rows for each main row -> the + and - icon on each row in the table*/
/*
    function format2(d , flag) {
        var postData = [{name: "transaction_id", value: obj["body"][d]["Transaction ID"]}];

        var indexRow = d;
        data = obj.additionalInfo[d];
        var maindata = obj.body[d];
        var x = "../IDs/" + data["sender_id"]["value"] + ".jpg"; //stand for the receiver substring in the a href

        if(flag){
        if (userName == 'restart' || userName == 'simon ' || userName == 'david' || userName == 'dina' || userName == 'customerservice' ) {
            if (obj["body"][d]["Status"] == 'FAILED' || obj["body"][d]["Status"] == 'ABORTED' || obj["body"][d]["Status"]=='CONFIRMED'|| obj["body"][d]["Status"]=='WAIT FOR PROCESS(Obligo)') {
                return createInformationUnvailableDiv(postData,obj,d);
            }
            else if (obj["body"][d]["Status"] == "WAIT FOR PROCESS(Obligo)"){
                return createInformation(x,data,maindata);
            }
            else {
                var div = document.createElement("div");
                div.appendChild(createStatusChanger(postData , obj, indexRow, d));
                div.appendChild(createInformation(x,data,maindata));
                return  div ;
            }
        }

        else if (userName=='yuval'){
            if (obj["body"][d]["Status"]=='FAILED' || obj["body"][d]["Status"]=='ABORTED'|| obj["body"][d]["Status"]=='CONFIRMED'|| obj["body"][d]["Status"]=='WAIT FOR PROCESS(Obligo)') {
                return '<div id=\"sInfoT\" style=\"float: left; margin-right: 7px; width: 20%;  text-align: left !important;" name='+postData[0]["value"]+'>' +
                '<span class=\"details_headers\" style=\"font-weight: 900;  \">Transaction Update | ID:'+postData[0]["value"]+' | Current Status: '+obj["body"][d]["Status"] +' <br> Status Update is currently unavailable due to current transaction status</span>';
            }
            else if (obj["body"][d]["Status"] == "WAIT FOR PROCESS(Obligo)"){
                return createInformation(x,data,maindata);
            }
            else{
                return createStatusChanger(postData , obj, indexRow, d);
            }
        }
        }
        else{
            if (obj["body"][d]["Status"] == 'FAILED' || obj["body"][d]["Status"] == 'ABORTED' || obj["body"][d]["Status"]=='CONFIRMED'|| obj["body"][d]["Status"]=='WAIT FOR PROCESS(Obligo)' || obj["body"][d]["Status"]=='CHECK DOCUMENTS') {
                return createInformationUnvailableDiv(postData,obj,d);
            }
            else{
                return createStatusChanger(postData , obj, indexRow, d);
            }
        }
    }
*/

    function format(d , flag) {
        var postData = [{name: "transaction_id", value: obj["body"][d]["Transaction ID"]}];

        var indexRow = d;
        data = obj.additionalInfo[d];
        var maindata = obj.body[d];
        var x = "../IDs/" + data["sender_id"]["value"] + ".jpg"; //stand for the receiver substring in the a href

        if(flag){

            if (userName=='yuval'){
                if (obj["body"][d]["Status"] == 'FAILED' || obj["body"][d]["Status"] == 'ABORTED' || obj["body"][d]["Status"]=='CONFIRMED'|| obj["body"][d]["Status"]=='WAIT FOR PROCESS(Obligo)' ) {
                    return createInformationUnvailableDiv(postData,obj,d);
                }
                else{
                    return createStatusChanger(postData , obj, indexRow, d,0);
                }
            }

            else if (userName == 'restart' || userName == 'simon ' || userName == 'david' || userName == 'dina' || userName == 'customerservice' )
            {

                    var div = document.createElement("div");
                    div.appendChild(createStatusChanger(postData , obj, indexRow, d,0));
                    div.appendChild(createInformation(x,data,maindata));
                    return  div ;

            }
            else{

                var div = document.createElement("div");
                var flagForStatusChangeOnly=1;
                div.appendChild(createStatusChanger(postData , obj, indexRow, d, flagForStatusChangeOnly));
                div.appendChild(createInformation(x,data,maindata));
                return  div ;
            }


        }
        //when transaction status update is unavailable
        else{

            var div = document.createElement("div");
            div.appendChild(createInformationUnvailableDiv(postData,obj,d));
            div.appendChild(createInformation(x,data,maindata));
            return  div ;
            
        }
    }

}
/**
 * creates the div that notify the user that status change is currently unavailable
 * @param postData
 * @param obj
 * @param d
 * @returns {Element}
 */
function createInformationUnvailableDiv(postData,obj,d){
    var sInfo = document.createElement("div");
    sInfo.setAttribute("id","sInfo");
    sInfo.setAttribute("name",postData[0]["value"]);
    var div2 = document.createElement("div");
    div2.setAttribute("class","details_headers2");
    div2.innerHTML = 'Transaction Update | ID:'+postData[0]["value"]+' | Current Status: '+obj["body"][d]["Status"];
    var p = document.createElement("p");
    p.innerHTML = 'Status Update is currently unavailable due to current transaction status';

    sInfo.appendChild(div2);
    sInfo.appendChild(p);
    return sInfo;

}
/**
 * creates the div containing information for the transaction
 * the dn goes into the child row when clicking + or - in the report table
 * @param x
 * @param data
 * @param maindata
 * @returns {Element}
 */
function createInformation(x,data,maindata){

    var sInfo = document.createElement("div");
    sInfo.setAttribute("id","sInfo");
    var table = document.createElement("table");
    var tdContent = ['Address: ' + data["sender_address"]["value"] + '', 'Address: ' + data["receiver_address"]["value"] + '', 'ID number: ' + data["sender_id"]["value"] + '', 'ID number: ' + data["receiver_id"]["value"] + '', 'Israel Citizen: ' + maindata["Israeli Citizen"] + '', 'Country: ' + maindata["Country"] + '', 'Citizenship: ' + maindata["Citizenship"] + '', 'City: ' + maindata["City2"] + '' , 'Phone: ' + maindata["Phone"] + '', 'Phone: ' + maindata["Phone2"] + '' ];
    var trFirst = document.createElement("tr");
    var thL = document.createElement("th");
    thL.setAttribute("class","thL");

    thL.innerHTML='Sender';
    var thR = document.createElement("th");
    thR.setAttribute("class","thR");
    thR.innerHTML = 'Receiver';
    trFirst.appendChild(thL);
    trFirst.appendChild(thR);
    table.appendChild(trFirst);
    
    
    for(var i=0;i<6;i+=2){
        
        var tr = document.createElement("tr");
        var tdL = document.createElement("td");
        var tdR = document.createElement("td");
        tdR.setAttribute("class","tdR");
        tdL.innerHTML = tdContent[i];
        tr.appendChild(tdL);
        tdR.innerHTML = tdContent[i+1];
        tr.appendChild(tdL);
        tr.appendChild(tdR);
        if(i+1==1){
            var td = document.createElement("td");
            var aHref = document.createElement("a");
            aHref.setAttribute("href",x);
            aHref.setAttribute("id","lightBoxImg");
            aHref.setAttribute("data-toggle","lightbox");
            var img = document.createElement("img");
            img.setAttribute("src","../images/unnamed.png");
            img.setAttribute("id","imgSenderInfoReport");
            aHref.appendChild(img);
            td.appendChild(aHref);
            tr.appendChild(td);
        }
        table.appendChild(tr);

    }
    sInfo.appendChild(table);
    return sInfo;
    
    

}

/**
 *
 * @param postData
 * @param obj
 * @param indexRow
 * @param d
 * @returns {Element}
 *
 * creates the Div containing the status change options in the child row when click the plus or minus icon in the report
 */
function createStatusChanger(postData , obj, indexRow, d, regular){
    
    var sInfoT = document.createElement("div");
    sInfoT.setAttribute("id","sInfoT");
    sInfoT.setAttribute("class","sInfoT");
    sInfoT.setAttribute("name",postData[0]["value"]);
    var span = document.createElement("span");
    span.innerHTML = 'Transaction Update | ID:' + postData[0]["value"] + ' | Current Status: ' + obj["body"][d]["Status"];
    var div2 = document.createElement("div");
    div2.innerHTML = 'New Status:';
    div2.setAttribute("class","details_headers2");
    var select = document.createElement("select");
    select.setAttribute("id","newStatusPick");
    select.setAttribute("name","newStatusPick");
    var div3 = document.createElement("div");
    div3.innerHTML = 'Comment:';
    div3.setAttribute("class","details_headers2");
    var textArea = document.createElement("textarea");
    textArea.setAttribute("name","comment");
    textArea.setAttribute("class","commentStatusChange");
    textArea.setAttribute("rows","2");
    textArea.setAttribute("cols","30");
    textArea.setAttribute("placeholder","This Field is mandatory, please enter comment");
    var fileInput = document.createElement("input");
    fileInput.setAttribute("type","file");
    fileInput.setAttribute("id","file");
    fileInput.setAttribute("name","TransactionFile");
    fileInput.setAttribute("class","TransactionFile");
    fileInput.innerHTML = "Choose a file to upload";
    var subDiv = document.createElement("div");
    subDiv.setAttribute("class","TransactionFile");
    var aHref = document.createElement("a");
    aHref.setAttribute("class","downloadIT");
    aHref.setAttribute("target","blank");
    aHref.setAttribute("href","./TransactionFiles/irregulerActions.doc");
    aHref.innerHTML = "DownloadReported irreguler actions";
    var submit = document.createElement("input");
    submit.setAttribute("type","submit");
    submit.setAttribute("class","submitStatusChangeReport");
    submit.setAttribute("id","btn_submit");
    submit.setAttribute("value","Update");

    sInfoT.appendChild(span);
    sInfoT.appendChild(div2);
    sInfoT.appendChild(select);
    if(!regular){
    sInfoT.appendChild(div3);
    sInfoT.appendChild(textArea);
    sInfoT.appendChild(fileInput);
    subDiv.appendChild(aHref);
    submit.setAttribute("onclick",'sendStatus($(this), '+indexRow+', '+regular+' )');
    subDiv.appendChild(submit);
    sInfoT.appendChild(subDiv);
        return sInfoT;

    }
    submit.setAttribute("onclick",'sendStatus($(this), '+indexRow+', '+regular+')');
    sInfoT.appendChild(submit);
    return sInfoT;




}

/**
 * change the background color of he row if the transaction status is failed
 */
function changeFailedColor(){
    $('.odd.redIT').find('td').css('background','#FF706E','important');
    $('.sorting_1').css('background','none');

}

/**
 * @param button
 * @param index
 * handle the new transaction changing, the function gets called when
 * the user clicking update button, that button located in the the child row for
 * each transaction row in the report table and available if the status or permissions
 * are valid and update status is ok
 */
function sendStatus(button,index , regular){

        if(!regular) {
            var rows = button.parent().parent().parent().parent().parent().siblings();
            var row=rows[index];
            var child=row['children'][6];
            var file_data = $('#file').prop('files')[0];
            var comment = $('textarea').val();
        }
        else{
            var rows = button.parent().parent().parent().parent().siblings();
            var row=rows[index];
            var child=row['children'][6];
        }

        var transaction = $('#sInfoT').attr('name');
        var val= $('#newStatusPick').val();
        var form_data = new FormData();
        form_data.append('file', file_data);
        if(!regular){
        var postData = [{name: "new_status", value: val}, {name: "transaction_id", value:transaction}, {name: "comment", value: comment}];
        }
        else {
        var postData = [{name: "new_status", value: val}, {name: "transaction_id", value:transaction}, {name: "function", value: "update_status"}];
        }
        for(var i=0;i<postData.length;i++){
        form_data.append(postData[i]['name'],postData[i]['value']);
        }
        if(comment!="" && val !=0){
        $.ajax({
            processData: false,
            contentType: false,
            data: form_data,
            type: 'POST',
            url: 'Server/updateTransactionServer.php',
            success: function (data) {
                var json = JSON.parse(data);

                if(json["result"]=='Transaction was updated successfully'){
                    alert('Transaction was updated successfully');
                    child['textContent']=json['status'];
                    var nodes = row.childNodes;
                    if(json['status']=="FAILED" || json['status']=="ABORTED"){
                        row.style.backgroundColor="#FF706E";
                        for(var i=0; i<nodes.length; i++) {
                                nodes[i].style.background = "#FF706E";
                        }
                    }
                    else{
                        var nodes=row['cells'];
/*
                        row.style.backgroundColor="none";
*/
                        for(var i=0; i<nodes.length; i++) {
                            nodes[i].style.backgroundColor = "";
                        }
                    }

                }

            }
        });
        }
        else{
            $('textarea').css("border-color","red")
        }
    
}


/**
 * add select key values when change the status of the transaction
 */
function addOptionsToNewStatus(data){
    if(data[0]==true){
        //create first empty option
        $('#newStatusPick').append($("<option></option>").attr("value","").text(""));
    $.each(data[1], function(key, value) {
        $('#newStatusPick')
            .append($("<option></option>")
                .attr("value",key)
                .text(value));
    });
    }
}


/**
 * this function add the 2 row to the header table -> adds the "sender" and "receiver" header section
 */
function addRowHeaders(headingInfo){

    if(headingInfo!=undefined) {
        var tr = document.createElement("tr");
        tr.setAttribute("id", "sectionsHeaders");

        for (var i = 0; i < headingInfo.length; i++) {
            var thObj = document.createElement("th");
            thObj.setAttribute("rowspan", headingInfo[i]["rowspan"]);
            thObj.setAttribute("colspan", headingInfo[i]["colspan"]);
            thObj.innerHTML = headingInfo[i]["text"];
            tr.appendChild(thObj);
        }

        $(".dataTables_scrollHead").children().children().children().prepend(tr);
    }
}


/**
 * this function remove the row header that split the table to sections (sender | receiver)
 * we're adding this row on each report generation so we need to remove first the old one
 * to prevent make this row duplicate  */
function removeRowHeader(){
    $('#sectionsHeaders').remove();
}


/**
 * this function clears the + and - icon that holds the additional information
 * this function made to clear the 2 last rows (total page and total)
 */
function clearAdditionInfoButton() {
    //clear the last row additional information icon
    $('tbody tr td:nth-child(2)').eq(-1).removeClass('details-control');
    //clear the before-last row additional information icon
    $('tbody tr td:nth-child(2)').eq(-2).removeClass('details-control');

}


/**
 *
 * @param obj - the data from the db as an array
 * @param data - the data is injected to the table
 * @param getNumForFooter - the row number the footer should get because the rows
 * in the table sort by the number54
 */
function setFooterContent(obj, data, getNumForFooter) {

    var i = 0;

   // data.push(obj[i]);
    data.push({
        "Service": obj[i]["total"]["value"],
        "No": getNumForFooter,
        "Date": "",
        "Transaction ID": "",
        "Receipt No": "",
        "VIP": "",
        "Status": "",
        "Client": "",
        "Clerk": "",
        "Full Name": "",
        "City": "",
        "Address": "",
        "Israeli Citizen": "",
        "Citizenship": "",
        "ID Type": "",
        "ID Number": "",
        "Phone": "",
        "Date of Birth": "",
        "Amount": obj[i]["totalpage"]["value"],
        "Commission": obj[i]["totalcommissionPAGE"]["value"],
        "Full Name2": "",
        "ID Number2": "",
        "Address": "",
        "City": "",
        "Phone": "",
        "Country": "",
        "Coin ID": "",
        "Deliver Via": "",
        "Bank Name": "",
        "Account Number": "",
        "Transaction Natures": "",
        "Amount To Receiver": "",
        "Agent Commission": "",
        "Rate": "",
        "Buy Rate": "",
        "Amount NIS": "",
        "WIC Amount": obj[i]["totalwicamountPAGE"]["value"],
        "WIC Cost": obj[i]["totalwiccostPAGE"]["value"],
        "Currency Difference": obj[i]["totalcurrdiffPAGE"]["value"],
        "AT services cost": obj[i]["totalATServicesCostPAGE"]["value"],
        "Currency Income":  obj[i]["totalcurrdiffPAGE"]["value"],
        "Comments": "",
        "WIC Rate": "",
        "Full Name Local": "",
        "Bank Local": "",
        "Client Local": "",
        "Address Local": ""
    });

    getNumForFooter += 1;

    data.push({
        "Service": obj['1']["total"]["value"],
        "No": getNumForFooter,
        "Date": "",
        "Transaction ID": "",
        "Receipt No": "",
        "VIP": "",
        "Status": "",
        "Client": "",
        "Clerk": "",
        "Full Name": "",
        "City": "",
        "Address": "",
        "Israeli Citizen": "",
        "Citizenship": "",
        "ID Type": "",
        "ID Number": "",
        "Phone": "",
        "Date of Birth": "",
        "Amount": obj['1']["totalamount"]["value"],
        "Commission": obj['1']["totalcommission"]["value"],
        "Full Name2": "",
        "ID Number2": "",
        "Address": "",
        "City": "",
        "Phone": "",
        "Country": "",
        "Coin ID": "",
        "Deliver Via": "",
        "Bank Name": "",
        "Account Number": "",
        "Transaction Natures": "",
        "Amount To Receiver": "",
        "Agent Commission": "",
        "Rate": "",
        "Buy Rate": "",
        "Amount NIS": "",
        "WIC Amount": obj['1']["totalwicamount"]["value"],
        "WIC Cost": obj['1']["totalwiccost"]["value"],
        "Currency Difference": obj['1']["totalcurrdiff"]["value"],
        "AT services cost": obj['1']["totalATServices"]["value"],
        "Currency Income":  obj['1']["totalcurrdiff"]["value"],
        "Comments": "",
        "WIC Rate": "",
        "Full Name Local": "",
        "Bank Local": "",
        "Client Local": "",
        "Address Local": ""
    });
    
}

/**
 * @param sizeForFooter - the size we need to make the footer\table width, match
 * to the obj.columns.length returned from the server
 * append footer to the table
 */

function addFooterelement(sizeForFooter){
    var tfoot = document.createElement("tfoot");
    var tr = document.createElement("tr");
    var tdObj = document.createElement("td");

    for (var i = 0; i < sizeForFooter; i++) {
        tr.appendChild(tdObj)
    }
    tfoot.appendChild(tr);
    $("#ddtable").append(tfoot);

}




/**
 *  Parsing json and making tr with his tds values and styles
 *
 * @param obj    - object of headers/bodys/footers
 * @param td     - td/th
 * @param type   - aka : thead/tbody
 * @returns {Element}
 */
function jsonLineMove(obj, td, type) {
    var typeElement = document.createElement(type);
    var rowNum;
    for (rowNum in obj) {
        var objRow = obj[parseInt(rowNum)];
        var tr = document.createElement("tr");
        for (column in objRow) {
            var columObj = objRow[column];
            var value = columObj.value;
            var style = columObj.style;

            var tdObj = document.createElement(td);
            tdObj.className = style;
            tdObj.innerHTML = value;
            (columObj.colspan > 0 ? tdObj.setAttribute("colspan", columObj.colspan) : "" );
            tr.appendChild(tdObj);
        }
        typeElement.appendChild(tr);
    }
    return typeElement;
}



function transactions_report_test(coin, page) {
    // postIt("Server/reportsServer.php","","action=getAutonomyCoins&coin="+coin,buildAutonomyCoins);

    /*
     xajax_transactions_report(document.getElementById('id_from_day').value, document.getElementById('id_from_month').value, document.getElementById('id_from_year').value, document.getElementById('id_to_day').value, document.getElementById('id_to_month').value, document.getElementById('id_to_year').value, document.getElementById('id_receiver_country').value
     , document.getElementById('id_sender_country').value, document.getElementById('id_user').value
     , document.getElementById('id_agent').value, document.getElementById('id_status').value, document.getElementById('id_transfer_via').value, document.getElementById('id_currency').value, document.getElementById('page').value, document.getElementById('excel').value, document.getElementById('search').value, document.getElementById('bankAccount').value = "");
     */

    var name = "reportTest" //this.name;
    var callback = "";// loadPopupBox("load it from the callback");
    show('loading', true);
    submitFormElement("../../Server/reportsServer.php", $("form"), "action=" + name, tableIt, 'false', page);
    return false;
}