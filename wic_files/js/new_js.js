

var modalBody = $(".popUpContainer");
var modalTitle = $(".wicSystemTitle");
var modal =$(".wicPopup");
var printButton =   $('<button type="button" id="print_button" class="btn btn-info print-preview" data-dismiss="modal">Print</button>');
var confirmButton = $('<button type="button" id="confirmButton" class="btn btn-info" data-dismiss="modal">Confirm and edit</button>');
var confirmOnlyButton = $('<button type="button" id="confirmOnlyButton" class="btn btn-info" data-dismiss="modal">Confirm</button>');

function numberWithCommas(x) {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

function formatPriceWtihComma(listElement){
          listElement.each(function(){
         $(this).text(numberWithCommas($(this).text()));
   })
}
function formatPriceWtihoutComma(listElement){
     listElement.each(function(){
     $(this).text(removeCommas($(this).text()));
   })
}


function removeCommas(str) {
  return(str.replace(/,/g,''));
};



function postIt(url,dataArr,get,callback){
  var formURL = url + ( get != "" ? "?"+get : "" );
  postData1 = [];
  postData1 = dataArr;
  $.ajax({
    url: formURL,
    type:"POST",
    data:postData1,
    success:function(data, textStatus, jqXHR){
      if (typeof(callback) != "undefined"){
        callback(data);
      }
    }
  });
}

function submitFormElement(url,formElement,get,callback, firstreq, page){
  var formURL = url + ( get != "" ? "?"+get : "" );
  //var formURL = url;

  var postData = new FormData();
  var firstReq='FirstReq';
  postData = formElement.serializeArray();
  postData.push({name: 'firstReq', value :firstreq});
  postData.push({name: 'page', value :page});

  $.ajax({
    url: formURL,
    type:"POST",
    data:postData,
    success:function(data, textStatus, jqXHR){
      console.log("success");
      console.log(data);
      if (typeof(callback) != "undefined"){
        callback(data);
      }
    }
  }).done(function( html ) {

  });
}

/**
 * author: zlil 20/6/16
 *
 * clear whitespace in form text fields to improve validation
 * send the form name when you calling the function
 * function needs to get call on form submit before showValidation function if exists
 */

function clearWhiteSpace(formname){
  //var nodes = document.querySelectorAll("#"+formname+" input[type=text]");
  var nodes = $('#'+formname).find('input[type=text]').filter(':visible');
  for (var i=0; i<nodes.length; i++){
    var val=nodes[i].value;
    val=val.replace(/\s+/g, " ");
    val=val.replace(/^\s+|\s+$/g, "");
    nodes[i].value=val;
  }
}

/**
 * general popup display. the default is : text - none, title - WIC system, button - non
 *
 * @param data
 * @param title    - default is "WIC system";
 * @param button   - for print purpose;
 */
function loadPopupBox(data,title,button) {    // To Load the Popupbox

  // reset the title
  if(title){
    modalTitle.text(title);
  }else{
    modalTitle.text("WIC system");
  }
  // reset the body
  if(data){
    modalBody.text(data);
  }else{
    modalBody.text("");
  }

  // reset the buttons
  $('.modal-footer').find ('#print_button').remove();
  $('.modal-footer').find ('#confirmButton').remove();
  $('.modal-footer').find ('#confirmOnlyButton').remove();

  if(button === "print"){
      $('#noShowForPrint').show();
      $('.modal-footer').find ('#print_button').remove();
      $('.modal-footer').append(printButton);
      $('.print-preview').printPreview();
  }else if(button == "confirm"){
    $('.modal-footer').append(confirmButton);
  }else if(button == "confirmOnlyButton"){
    $('.modal-footer').append(confirmOnlyButton);
  }
  modal.modal('show')


}

function errorPageDisplay(text){
     $("#errorShortDescText").text(text);
}

//for side bar
$(".office").hide();
$(".settings").click(function(){
    $(".office").slideToggle();
});

$(".officeHk").hide();
$(".settings-hk").click(function(){
  $(".officeHk").slideToggle();
});

//select amount


function submitFormAndBuildTable(formElement, container, functionDone) {
  var postData = new FormData();
  postData = formElement.serializeArray();
  postData.push({name: 'form', value: formElement.attr("id")});
  var formURL = formElement.attr("action");
  /*formElement.attr("action");*/

  $.ajax({
    url: formURL,
    type: "POST",
    data: postData,
    success: function (data, textStatus, jqXHR) {
      container.text("");
      buildTableFromJason(data, container);
      return true;
    },
    error: function (jqXHR, textStatus, errorThrown) {
      return false;
    }
  })
    .promise().done(function () {

      if(functionDone)
        window[functionDone]();
    });

}


function buildTableFromJason(json, contianer) {
  var j = JSON.parse(json);
  var tr = "";
  var tds = "";
  var ths = "";
  var content = "<table class='table'>";
  var id = 1 ;
  $.each(j, function (i, v) {
    tr += "<tr>";
    tds = "";
    ths = "";
    $.each(this, function (header, value) {
      ths += "<th>" + header + "</th>";

      tr += "<td>" + (value == null ? (id) : value ) + "</td>";
    })
    id++;
    tr += "</tr>" ;
  });
  content += "<thead><tr> " + ths + "</tr></thead>"
  content += "<tbody>" + tr + "</tbody>";
  content += "</table>";
  console.log(content);

  contianer.append(content);
}
//next stage
function calcFee(receiveAmount) {
  currency = $('#id_currency').val();
  if (currency === "") {
    alert("Please choose currency");
  } else {
      setrate(currency, receiveAmount);
      $('#calculated').val('calculated');
  }
}

$('.side').bind('contextmenu', 'a', function(){
event.preventDefault();
});

function prepareCountryCurrency(currency, curr) {

  var rate = '';
  if (curr == 'USD') {
    if (currency != 'USD') {
      rate = document.getElementById('id_rate').value;
      document.getElementById('currency_sign').innerHTML = currency;

    } else {
      rate = "1";
      document.getElementById('currency_sign').innerHTML = "$";
    }
  }
  // Using EUR
  else {
    if (curr == 'EUR') {
      if (currency != 'EUR') {
        rate = document.getElementById('id_rate_eur').value;
        document.getElementById('currency_sign').innerHTML = currency;
      } else {
        rate = "1";
        document.getElementById('currency_sign').innerHTML = "€";
      }
    } else {
      if (currency != 'EUR') {
        rate = document.getElementById('id_rate').value;
        document.getElementById('currency_sign').innerHTML = currency;
      }
    }
  }

  // using HKD
  //if (curr == 'EUR') {
  //  rate = 1;
  //}


  $('#id_commission').val('');
  $('#id_charge').val('');
  //$('#id_transferred_amount').val('');
  $('#calculated').val('');


  return rate;
}

//change curreny of country
function changeCurrency(curr, country, via) {
  if (((country != 'INDIA') && (country != 'THAILAND') && (via != 'Galaxy Deposit') && (via != 'Galaxy Pickup')) || country == 'HONG KONG') {
    alert('h');
    document.getElementById('usd_eur').value = curr;
    document.getElementById('usd_eur').innerHTML = curr;
  }
  // USD or EUR
  if (curr == 'USD') {
    $("#id_amount").css("background-image", "url('../images/dollar.png')");
    $("#id_commission").css("background-image", "url('../images/dollar.png')");
    $("#id_charge").css("background-image", "url('../images/dollar.png')");
  }
  else {
    $("#id_amount").css("background-image", "url('../images/Euro.png')");
    $("#id_commission").css("background-image", "url('../images/Euro.png')");
    $("#id_charge").css("background-image", "url('../images/Euro.png')");
  }
  if (via != 'Swift (Deposit)') {
    $('#id_currency').change();
  }
}

//end select amount

//send money
function nextSendMoney() {
  xajax_checkSender($('#id_passport_or_identity_number').val(), $('#id_sender_full_name').val(), $('#id_sender_address').val(),
    $('#id_sender_city').val(), $('#id_sender_phone_number_0').val(), $('#id_sender_phone_number_1').val(), $('#id_sender_phone_number_2').val(),
    $('#id_sender_birth_day').val(), $('#id_sender_birth_month').val(), $('#id_sender_birth_year').val(), $('#id_sender_gender').val(),
    $("input[name='receiverInformation']:checked").val(), $('#id_receipt_no').val(), $('#id_type').val());

  setTimeout(function () {
    if (document.getElementById("pass").value == 1) {
      loadPopupBox('the sender id is already exist. would you like to overwrite it? ', 'WIC system', 'confirmOnlyButton');
      $('#confirmOnlyButton').on('click', function () {
        $('#next').click();
      });
      return false;
    }
    else{
      $('#next').click();
    }
  },1000);
}


// validation
function validateEmail(email) {
  var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return re.test(email);
}
function isPhoneNumber(phone){
  var re = /^\d+(-\d+)*$/;
  return re.test(phone);
}

// html5 validation
function checkElementValidity(byID){
  return document.getElementById(byID).checkValidity() ;
}

function IsNumeric(input)
{
  return (input - 0) == input && (''+input).trim().length > 0;
}


/**
*  loader
*
*/

function show(id, value) {
  document.getElementById(id).style.display = value ? 'block' : 'none';
}

function checkReady() {

    window.clearInterval(intervalID);
    callback.call(this);

}

/**
 * clear input fields in the main page of the send money process (selectamount_new.php)
 */
$('#clearAmount').click(function() {
  $('#id_amount').val('');
  $('#id_transferred_amount').val('');
  $('#id_commission').val('');
  $('#id_charge').val('');
  $('#transfer_amount').val('');
});

/**
 * clear input fields in sender information form on sendmoney.php 
 */
$('#clearSenderInfo').click(function(){
  $('#card_num ').val('');
  //$('#id_sender_full_name').val('');
  $('#id_sender_first_name').val('');
  $('#id_sender_middle_name').val('');
  $('#id_sender_last_name').val('');
  $('#id_sender_gender').val('');
  $('#id_sender_birth_day').val('1');
  $('#id_sender_birth_month').val('1');
  $('#id_sender_birth_year').val('1950');
  $('#id_type').val('');
  $('#id_sender_israeli_resident').val('');
  $('#id_passport_or_identity_number').val('');
  $('#id_sender_address').val('');
  $('#id_sender_city').val('');
  $('#id_sender_phone_number_1').val('');
  $('#id_sender_phone_number_2').val('');

  $('#receiverImage').empty();
  //$('#chooseReceiver').empty();
  $('input[type=Radio]').attr('checked',false);
});

/**
 * check natures selection and natures approved combinations
 * if not allowed load popup box
 */
function check_natures() {
  if (!$('.row-natures input[type="checkbox"]').is(":checked")) {
    loadPopupBox("Please choose Nature ");
    return false;
  }

  // generic if to make sure at least 1 required nature is checked
  if ((!$('.row-natures input[name="natures[1]"]').is(":checked")) && (!$('.row-natures input[name="natures[6]"]').is(":checked")) && (!$('.row-natures input[name="natures[7]"]').is(":checked")) && (!$('.row-natures input[name="natures[2]"]').is(":checked")) &&
      (!$('.row-natures input[name="natures[3]"]').is(":checked"))) {
    loadPopupBox("Natures 1 , 2 or 3 or 6 or 7 is must");
    return false;
  }

  //nature 1 rules
  if (($('.row-natures input[name="natures[1]"]').is(":checked")) && (($('.row-natures input[name="natures[3]"]').is(":checked")) ||
      ($('.row-natures input[name="natures[2]"]').is(":checked")) || ($('.row-natures input[name="natures[7]"]').is(":checked")) || ($('.row-natures input[name="natures[6]"]').is(":checked")))) {
    loadPopupBox("if you choose nature 1 you can choose only natures 4 or 5");
    return false;
  }

  //nature 2 rules
  if ((($('.row-natures input[name="natures[2]"]').is(":checked"))) &&
      (($('.row-natures input[name="natures[1]"]').is(":checked")) || ($('.row-natures input[name="natures[4]"]').is(":checked")) || ($('.row-natures input[name="natures[3]"]').is(":checked")) || ($('.row-natures input[name="natures[7]"]').is(":checked")) ||
      ($('.row-natures input[name="natures[5]"]').is(":checked")) || ($('.row-natures input[name="natures[6]"]').is(":checked")))) {
    loadPopupBox("if you choose nature 2 you cannot choose another nature");
    return false;
  }

  //nature 3 rules
  if ((($('.row-natures input[name="natures[3]"]').is(":checked"))) &&
      (($('.row-natures input[name="natures[1]"]').is(":checked")) || ($('.row-natures input[name="natures[4]"]').is(":checked")) ||
      ($('.row-natures input[name="natures[5]"]').is(":checked")) || ($('.row-natures input[name="natures[6]"]').is(":checked")))) {
    loadPopupBox("if you choose nature 3 you can choose only nature 7 with it ");
    return false;
  }

  //nature 4 and 5 rules
  if ((($('.row-natures input[name="natures[4]"]').is(":checked")) || ($('.row-natures input[name="natures[5]"]').is(":checked"))) && (
      (!$('.row-natures input[name="natures[1]"]').is(":checked")) && (!$('.row-natures input[name="natures[6]"]').is(":checked")) && (!$('.row-natures input[name="natures[7]"]').is(":checked")) )) {
    loadPopupBox("if you choose natures 4 or 5 you must choose nature 1 or 6 or 7");
    return false;
  }

  //nature 6 rules
  if ((($('.row-natures input[name="natures[6]"]').is(":checked"))) &&
      (($('.row-natures input[name="natures[1]"]').is(":checked")) || ($('.row-natures input[name="natures[2]"]').is(":checked")) || ($('.row-natures input[name="natures[3]"]').is(":checked")) || ($('.row-natures input[name="natures[7]"]').is(":checked")))) {
    loadPopupBox("if you choose nature 6 you can choose only natures 4 or 5 with it ");
    return false;
  }

  //nature 7 rules
  if ((($('.row-natures input[name="natures[7]"]').is(":checked"))) &&
      (($('.row-natures input[name="natures[1]"]').is(":checked")) || ($('.row-natures input[name="natures[2]"]').is(":checked")) || ($('.row-natures input[name="natures[6]"]').is(":checked")))) {
    loadPopupBox("if you choose nature 7 you can choose only nature 3 , or natures 4/5 with it.");
    return false;
  }

  return true;
}

/**
 * Prevent going back on back space
 *
 */
$(document).unbind('keydown').bind('keydown', function (event) {
  var doPrevent = false;
  if (event.keyCode === 8) {
    var d = event.srcElement || event.target;
    if ((d.tagName.toUpperCase() === 'INPUT' &&
        (
        d.type.toUpperCase() === 'TEXT' ||
        d.type.toUpperCase() === 'PASSWORD' ||
        d.type.toUpperCase() === 'FILE' ||
        d.type.toUpperCase() === 'SEARCH' ||
        d.type.toUpperCase() === 'EMAIL' ||
        d.type.toUpperCase() === 'NUMBER' ||
        d.type.toUpperCase() === 'DATE' )
      ) ||
      d.tagName.toUpperCase() === 'TEXTAREA') {
      doPrevent = d.readOnly || d.disabled;
    }
    else {
      doPrevent = true;
    }
  }

  if (doPrevent) {
    event.preventDefault();
  }
});