/**
 * Created by user on 8/4/2016.
 */

$( document ).ready(function() {
    buildFilters();


});

$(document).on('change', '#id_agent', function () {
    var data=$('#id_agent :selected').text();
    var form_data = new FormData();
    var postData = [{name: 'clerksReq', value :true} , {name: 'clientName', value :data}];
    for(var i=0;i<postData.length;i++){
        form_data.append(postData[i]['name'],postData[i]['value']);
    };

    $.ajax({
        processData: false,
        contentType: false,
        url: '../../Server/reportsFilterServer.php',
        type: 'POST',
        data: form_data,
        success: function (data) {
            var json = JSON.parse(data);
            $('#id_user').remove();
            $('#clerksSelectFrame').append(datePicker.generateClerksSelection(json));

        }
    });



});


function buildFilters(){
    $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: '../../Server/reportsFilterServer.php',
        success: function (data) {
            var json = JSON.parse(data);

            $('#fromDates').append(datePicker.generateFromDate());
            $('#toDates').append(datePicker.generateToDate());

            if(json['receivingCountries']!=null){
                $('#receivingCountriesFilter').append(datePicker.generateReceivingCountries(json['receivingCountries']));
                $('#receivingCountriesFrame').show();
                $('.id_receiver_country').show();
            }
            if(json['senderCountry']!=null){
                $('#sendingCountriesFilter').append(datePicker.generateSenderCountries(json['senderCountry']));
                $('#senderCountriesFrame').show();
            }
            if(json['senderCountryHidden']!=null){
                $('#sendingCountriesFilter').append(datePicker.generateHiddenInput('id_sender_country',Object.keys(json['senderCountryHidden'])))
            }
            if(json['transferVia']!=null){
                $('#transferTypeFilter').append(datePicker.generateTransferTypes(json['transferVia']));
                $('#id_transfer_via').show();
                $('.id_transfer_via').show();
                $('#transferTypeFilter').show();
            }
            if(json['transferViaHidden']!=null){
                $('#agentSelectFrame').append(datePicker.generateHiddenInput('id_transfer_via',json['transferViaHidden']));
                var parent = $('#report2').parent();
                parent.removeClass('col-sm-offset-1');
                parent.addClass('col-sm-offset-3');
            }
            if(json['transferViaHidden']!=null && json['receivingCountries']== null){
                var parent = $('#report2').parent();
                parent.removeClass('col-sm-offset-3');
                parent.addClass('col-sm-offset-5');
            }
            if(json['agents']!=null){
                $('#agentSelectFrame').append(datePicker.generateAgentsSelection(json['agents']));
                $('.agentSelectFrame').show();
            }
            if(json['clerks']!=null){
                $('#clerksSelectFrame').append(datePicker.generateClerksSelection(json['clerks']));
                $('.clerksSelectFrame').show();
            }
            if(json['agentsHidden']!=null){
                $('#agentSelectFrame').append(datePicker.generateHiddenInput('id_agent',json['agentsHidden']));
                var parent = $('#reportTest').parent();
                parent.removeClass('col-sm-offset-1');
                parent.addClass('col-sm-offset-3');
            }
            if(json['clerksHidden']!=null){
                $('#clerksSelectFrame').append(datePicker.generateHiddenInput('id_user',json['clerksHidden']));
            }
            if(json['clerksHidden']!=null && json['agentsHidden']!=null){
                var parent = $('#reportTest').parent();
                parent.removeClass('col-sm-offset-3');
                parent.addClass('col-sm-offset-5');
            }
            if(json['status']!=null){
                $('#statusSelectFrame').append(datePicker.generateStatusSelection(json['status']));
            }
            if(json['currencies']!=null){
                $('#currencySelectFrame').append(datePicker.generateCurrencies(json['currencies']));

            }
            
        }

        
    });
    
};

