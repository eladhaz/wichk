<?php
error_reporting(0);

require_once("./site_config.php");
require_once "ipClearance.php";
//include "head_common_new.php";
$_SESSION['main'] = '';


// Info from mainLogin screen
$branch = $_POST['branchName'];
$user = $_POST['userName'];
$pass = $_POST['pass'];
$conn = wic_db_connect();       // Connect to server and select databse.

/* upgrading  */
/*if (($_POST['branchName'] != 'administrator') &&
        ($_POST['branchName'] != 'bookkeeping')  &&
          ($_POST['branchName'] != 'payoutAgent') &&
              ($_POST['userName'] != "test") &&
                    ($_POST['branchName'] != 'change.now')  &&
                      ($_POST['branchName'] != 'wicstore107') &&
                          ($_POST['branchName'] != 'TEST LAST') ) {
  // header("location:mainLogin.php");

  header("location: client/upgradingPage.php");

  ///echo "Down for maintenance";
  exit;
}*/

// TODO: To not allow multiple logins (simulations session for the same user)

if (strtolower($_POST['branchName']) == "contractor") {
    $_SESSION['nextORback'] = 5;
    header("location:mainLogin.php");
    exit;
}

session_start();

/* Get the salt of the user (for password protection) */
$saltSQL = 'SELECT salt,session_login,branch_num FROM agents_hk INNER JOIN `branches_hk` ON (branches_hk.id_num = agents_hk.id) WHERE name="' . $user . '" AND UPPER(branch_name)=UPPER("' . $branch . '")';
//$saltSQL = 'SELECT salt FROM agents_hk WHERE name="'. $user .'"';
$saltResult = mysql_fetch_array(mysql_query($saltSQL));
$salt = $saltResult['salt'];
$user_session = $saltResult['session_login'];
$passToCheck = hash('sha512', $salt . $pass);      // calculate the password with the salt and the hash
$branch_num = $saltResult['branch_num'];
// Don't allow simultaneous logins
$session_id = session_id();
$saltSQL = 'UPDATE agents_hk SET session_login="' . $session_id . '" WHERE branch_num="' . $branch_num . '" AND name="' . $user . '" ';
mysql_query($saltSQL);

$sqlBranches = mysql_query('SELECT id_num, active_branch,branch_type,autonomy_fk FROM branches_hk INNER JOIN agents_hk ON (branches_hk.id_num = agents_hk.id) WHERE UPPER(branch_name)=UPPER("' . $branch . '")');
if ($rowBranch = mysql_fetch_array($sqlBranches)) {
    $sql = 'SELECT * FROM agents_hk WHERE id ="' . $rowBranch['id_num'] . '" AND LOWER(name) ="' . strtolower($user) . '"';
    $sqlAgents = mysql_query($sql);
    if ($rowAgent = mysql_fetch_array($sqlAgents)) {
        /* check if agent and branch are active */
        if (strtolower($rowAgent['active_agent']) != "active" && strtolower($sqlBranches['active_branch']) != "active") {
            $_SESSION['nextORback'] = 7;
            header("location:./client/mainLogin.php");
            exit();
        }


        /* Check if the password isn't match and exit */
        if ($rowAgent['password'] !== $passToCheck) {
            $_SESSION['nextORback'] = 6;
            header("location:./client/mainLogin.php");
            exit;
        }

        /* Check if the user can enter with his IP address */
        if (checkIfClientIpCanEnter($rowAgent['branch_num']) == 0) {
            $_SESSION['nextORback'] = 8;
            header("location:./client/mainLogin.php");
            exit;
        }

        /* Check password date and if 3 months passed,
            redirect the uer to change password screen */
        /*        if (strtotime($rowAgent['updated_at']) < strtotime(date("Ymd H:i:s"))) {
                    header("location:updatePassword.php?report=1");
                    exit;
                }*/
        echo $branch;
        echo $user;

        session_destroy();
        session_start();
        echo $branch;
        echo $user;
        /* Enter all to session */
        $_SESSION['branch'] = strtolower($branch);
        $_SESSION['myusername'] = strtolower($user);
        $_SESSION['country'] = $rowAgent['current_country_id'];
        $_SESSION['sender_country'] = $rowAgent['sender_country_id'];
        $_SESSION['required_amount'] = $rowAgent['required_collected_amount'];
        $_SESSION['agent_phone_number'] = $rowAgent['phone'];
        $_SESSION["user_status"] = $rowAgent['type'];
        $_SESSION["culture"] = $rowAgent['culture'];
        $_SESSION["branch_type"] = strtolower($rowBranch['branch_type']);
        $_POST['nextORback'] = 1;
        $_SESSION['first_name'] = $rowAgent['first_name'];
        $_SESSION['last_name'] = $rowAgent['last_name'];
        $_SESSION["required_recipt"] = $rowAgent['required_recipt'];
        $_SESSION["required_amount"] = $rowAgent['required_collected_amount'];
        $_SESSION["required_exchange"] = $rowAgent['required_exchange'];
        $_SESSION["required_easy_pay"] = $rowAgent['required_easy_pay'];
        $_SESSION["required_send_money_corporation"] = $rowAgent['required_send_money_corporation'];
        $_SESSION["autonomy"] = $rowAgent['autonomy_fk'];


        // reedirect to selectamount_new.php
        echo $branch;
        echo $user;
        header("location:./client/transaction_report_test2.php");
        exit;
    }
}
else {
    $_SESSION['nextORback'] = 3;
    header("location:./client/mainLogin.php");
    exit;
}

?>
