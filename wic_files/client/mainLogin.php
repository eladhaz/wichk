<?php
error_reporting(0);

require_once "head_common_new.php";
require_once "../ipClearance.php";
session_start();

$isHK = checkIfClientIpHK();

// Don't allow simultaneous logins

//$branch = $_SESSION['branch'];
//$user = $_SESSION['myusername'];
//$saltSQL = 'SELECT branch_num FROM agents INNER JOIN `branches` ON (branch_num = id_num) WHERE name="' . $user . '" AND UPPER(branch_name)=UPPER("' . $branch . '")';
//$saltResult = mysql_fetch_array(mysql_query($saltSQL));
//$branch_num = $saltResult['branch_num'];
//$saltSQL = 'UPDATE agents SET session_login="" WHERE branch_num="' .$branch_num . '"';
//mysql_query($saltSQL);
$_SESSION['main'] = 'manu';
$_SESSION['globalButton'] = 1;
?>
<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Log in | WIC Money Transfers</title>
    <meta name="description" content="WORLDCOM Finance website">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../css/mainLogin.css"/>
<!--    <script type="text/javascript" src="../js/jquery-1.11.2.min.js" ></script>
-->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>

    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick-theme.css"/>

<!--    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
-->    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.2.1/mustache.min.js"></script>
<!--    <script src="../thailanadmin/jquery.cookie.js"></script>
-->    <script type="text/javascript" src="../js/new_js.js"></script>

</head>
<body class="mainLoginBody">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="container">
    <img src="../../images/New_Logo1.png" class="img-responsive" alt="WIC Logo">


    <!-- News modal -->
    <div id="mainLoginNews">
        <div id="carousel" data-slick='{"slidesToShow": 4, "slidesToScroll": 4}'></div>

        <div class="controls">

        </div>
    </div>









    <form class="form-horizontal formLoginWithNews <?php /*echo($isHK ? "formLogin" : "formLoginWithNews"); */?> col-md-8 col-lg-6"
          method="post" action="../checkLogin.php">
        <div class="form-group">
            <div class="col-sm-4 col-md-3">
                <label for="inputBrnach" class="control-label">Client</label>
            </div>
            <div class="col-sm-8 col-md-9">
                <input type="branch-login" class="form-control" placeholder="Enter Branch Name" name="branchName"
                       value="<?php echo $_COOKIE['branch'] ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-3">
                <label for="inputUsername" class="control-label">Clerk</label>
            </div>
            <div class="col-sm-8 col-md-9">
                <input type="userName-login" class="form-control" placeholder="Enter Username" name="userName"
                       value="<?php echo $_COOKIE['myusername'] ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-3">
                <label for="inputPassword3" class="control-label">Password</label>
            </div>
            <div class="col-sm-8 col-md-9">
                <input type="userName-login" autocomplete="off" class="form-control " id="pass"
                       placeholder="Enter Password" name="pass"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button id="sign-in-btn" type="submit" class="btn btn-primary">Sign in</button>
            </div>
        </div>
    </form>


    <!---- slick news container---->
    <div id="mainLoginNews" class="formLoginWithNews slick" >

    </div>






    <?php include_once "popupPage.php" ?>
    <script type="text/javascript">

        // loadPopupBox("", "WIC system");


        <?php if($_SESSION['nextORback'] == 2){ ?>
        loadPopupBox("Clerk name is not correct! Please try again.", "Warning!");
        <?php } ?>
        <?php if($_SESSION['nextORback'] == 3){ ?>
        loadPopupBox("Client is not correct! Please try again.", "Warning!");

        <?php } ?>
        <?php if($_SESSION['nextORback'] == 4){ ?>
        loadPopupBox("The system doesn't support simultaneous logins, please log out and try again.", "Warning!");
        <?php } ?>
        <?php if($_SESSION['nextORback'] == 5){ ?>
        loadPopupBox("You don't have permission to login the system. Please contact the support team for help.", "Warning!");
        <?php } ?>
        <?php if($_SESSION['nextORback'] == 6){ ?>
        loadPopupBox("Password is not correct! Please try again.", "Warning!");
        <?php } ?>
        <?php if($_SESSION['nextORback'] == 7){ ?>
        loadPopupBox("agent is not active! Please contact the support team for help.", "Warning!");
        <?php } ?>
        <?php if($_SESSION['nextORback'] == 8){ ?>

        loadPopupBox("You IP address isn't allowed to login to the system using this branch and username!", "Warning!");
        <?php } ?>

        <?php
        unset($_SESSION['nextORback']);
        session_destroy();
        session_start();
        session_unset();
        ?>
    </script>

    <script type="text/javascript" src="../js/mainLogin.js"></script>


</body>
</html>
