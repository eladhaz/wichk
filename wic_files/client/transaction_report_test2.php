<?php

include "head_common_new.php";

?>
<!DOCTYPE html>
<html>
<head>

    <style>
        @page {
            size: landscape;
        }
    </style>


</head>
<body>
<div class="container">
    <div class="row noPrint">
        <div id="report_single_page">
            <div class="row">
                <div class="reportTitle">
                    Transaction Report
                </div>
            </div>
            <form>
                <div class="row">
                    <!--serach input test for free  id seach--->
                    <div class="col-sm-6">
                        <div class="col-sm-3  marginTop10">
                            Search for a transaction:
                        </div>
                        <div class="col-sm-4 ">
                            <input type="text" class="tftextinput " id="search" name="search" value="" style="background-image: none">
                        </div>
                        <div class="col-sm-2">
                            <input type="button" id="searchReport" class="button-info " value="Find transaction"/>
                        </div>
                    </div>
                    <?php if (strtolower($_SESSION['myusername']) != "book" || true) { ?>
                        <div class="col-sm-6 movePadding">
                            <div class="col-sm-2 marginTop10 ">
                                Enter bank account number:
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="bankAccount" id="bankAccount"/>
                            </div>

                            <div class="col-sm-3">
                                <input type="button" id="searchBankAccount" class="button-info " value="Find bank"/>
                            </div>
                        </div>
                    <?php } else { ?>
                        <input type="hidden" value="" name="bankAccount" id="bankAccount"/>

                    <?php } ?>

                </div>

                <hr class="thin">

                <div class="row">
                    <!-- FROM -->
                    <div class="col-sm-2">
                        <div class="row">
                            <div class="col-sm-2 marginTop10">
                                From:
                            </div>
                            <div class="col-sm-10" id="fromDates">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="row">
                            <div class="col-sm-2 marginTop10">
                                To:
                            </div>
                            <div class="col-sm-10" id="toDates">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2 id_receiver_country" hidden>
                        <div class="row">
                            <div class="col-sm-3">
                                Receiving country:
                            </div>
                            <div id="receivingCountriesFilter" class="col-sm-8 countryWidth">
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-2" id="senderCountriesFrame" hidden>
                        <div class="row">
                            <div class="col-sm-4">
                                Sending country:
                            </div>
                            <div id="sendingCountriesFilter" class="col-sm-8 sender_select">
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-2 id_transfer_via" id="transferTypeFrame" hidden>
                        <div class="row">
                            <div class="col-sm-4">
                                Transfer type:
                            </div>
                            <div id="transferTypeFilter" class="id_transfer_via col-sm-8">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2 col-sm-offset-1" name="reportExcel" id="reportExcel1"> <!--col-sm-offset-5-->
                        <input type='button' id='report2' class='button-info' value='Export to Excel'/>
                    </div>
                </div>

                <div class="row">
                    <!-- agents filter---->
                    <div class="col-sm-2 agentSelectFrame" hidden>
                        <div class="row">
                            <div class="col-sm-3">
                                Client:
                            </div>
                            <div class="col-sm-9" id="agentSelectFrame">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 clerksSelectFrame" hidden>
                        <div class="row">
                            <div class="col-sm-3 AgentTransaction">
                                Clerk:
                            </div>
                            <div class="clerksSelectFrame col-sm-9 agentSelectTransaction" id="clerksSelectFrame" hidden>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="row">
                            <div class="col-sm-4">
                                Status:
                            </div>
                            <div id="statusSelectFrame" class="col-sm-8 agentSelectTransaction">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="row">
                            <div class="col-sm-4">
                                Currency:
                            </div>
                            <div id="currencySelectFrame" class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-sm-offset-1"> <!--col-sm-offset-5-->
                        <input type="button" id='reportTest' name='reportTest' class='button-info' value='Generate report'/>
                    </div>
                </div>
            </form>
            
            <input type="hidden" id="page" name="page" value="1"/>
            <input type="hidden" id="excel" name="excel" value="false"/>

        </div>
    </div>
    <div class="row">
        <table id="ddtable" class="stripe row-border order-column" cellspacing="0"></table>
    </div>
</div>
</body>
<div id="loading" style="display: none"></div>
</html>
<script src="../js/showReport.js"></script>
<script src="../js/showTransactionReport.js"></script>
<script src="../js/new_js.js"></script>
<script>

    <?php if(!is_null($_GET['reportVipChange'])): ?>
    reportVipChange('2');
    <?php endif ?>
</script>