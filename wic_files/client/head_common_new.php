<head>
<!--    <link rel="stylesheet" href="../js/jquery-ui/css/jquery-ui-1.8.18.custom.css">
    <script src="../js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>-->
    <script type="text/javascript" src="../../js/jquery-1.11.2.min.js"></script>
    <link rel="stylesheet" href="../../js/jquery-ui/css/jquery-ui-1.8.18.custom.css">
    <script src="../../js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css?<?php echo time()?>" type="text/css" media="screen" />
    <link rel="stylesheet" href="../../css/bootstrap-theme.min.css?<?php echo time()?>" type="text/css" media="screen" />
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap-filestyle.js"> </script>

    <script src="../js/datePicker.js"></script>
    <script src="../js/reportFilters.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript">
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();


    </script>

    <link href="../../css/lightbox.css" rel="stylesheet"/>
    <link type="text/css" href="../../css/send_individual/report.css" rel="stylesheet">
    <link type="text/css" href="../../css/send_individual/send_money.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12,b-1.2.1,cr-1.3.2,fc-3.2.2,fh-3.1.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12,b-1.2.1,cr-1.3.2,fc-3.2.2,fh-3.1.2/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.0/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.6.0/jszip.min.js"></script>

</head>