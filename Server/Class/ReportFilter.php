<?php
include_once "../wic_files/site_config.php";

/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/3/2016
 * Time: 16:52
 */
class ReportFilter extends PDOService
{

    private $reportFilters;
    private $countries = array();
    private $branch;
    private $HKDFilter;
    private $HKDFilterOnly;
    private $senderCounries = array();
    private $autonomy;

    function __construct($table)
    {
        parent::__construct($table);
        $this->branch = strtolower($_SESSION['branch']);
        $officeArr = array("bookkeeping", "operation", "administrator", "h.k", "customerservice");
        $this->HKDFilter = (in_array($this->branch, $officeArr) ? true : false);
        $this->HKDFilterOnly = $_SESSION['autonomy'] == 2 ? true : false;
        $this->autonomy = $_SESSION['autonomy'];

    }

    public function selectDB()
    {
        // TODO: Implement selectDB() method.
    }


    function getFilters($branch)
    {
        $arrWhere = array();
        $id = $this->getBranchID($branch);
        $arrWhere['administartion_transactionreportfilter.branch'] = $id;
        $filters = $this->selectWhereVal('administartion_transactionreportfilter', 'administartion_transactionreportfilter.*', $arrWhere);
        $this->reportFilters = $filters[0];
        /*        if ($filters[0]['receiving_country'] == 1) {
                    $this->getReceivingCountry();
                }
                if ($filters[0]['transfer_type'] == 1) {
                    $this->getTransferType();
                }
                    $this->getAgentsAndClerks();*/

        $this->getReceivingCountry();
        $this->getSenderCountry();
        $this->getTransferType();
        $this->getAgentsAndClerks();
        $this->getSendingCountry();
        $this->getStatus();
        $this->getCurrencies();

        return $this->reportFilters;
    }

    function getBranchID($branch)
    {
        $arrWhere = array();
        $arrWhere['branches.branch_name'] = $branch;
        $id = $this->selectWhereVal('branches', 'branches.id_num', $arrWhere);
        return $id[0];
    }

    function getReceivingCountry()
    {
        if (strtolower($_SESSION["myusername"]) != 'chinanet' && strtolower($_SESSION["myusername"]) != 'ecpay' && strtolower($_SESSION["branch"]) != 'sudan' && strtolower($_SESSION["branch"]) != 'thailand' && strtolower($_SESSION["branch"]) != 'thailand11' && strtolower($_SESSION["branch"]) != 'ethiopia' && strtolower($_SESSION["branch"]) != 'iforex') {
            if (strtolower($_SESSION["branch"]) == 'h.k' || $this->HKDFilterOnly) {
                $countriesHK = array("CHINA", "INDIA", "PHILIPPINES", "THAILAND", "SRI LANKA");
                for ($i = 0; $i < sizeof($countriesHK); $i++) {
                    array_push($this->countries, $countriesHK[$i]);
                }
                $this->reportFilters["receivingCountries"] = $this->countries;
                return;
            } else if (strtolower($_SESSION["myusername"]) == 'galaxy') {
                $this->countries = array("CHINA", "NEPAL");
            } else {
                $query = $this->pdo->from('administration_country', null, "v")->select('administration_country.*')->orderBy('description')->fetchAll();
                for ($i = 0; $i < sizeof($query); $i++) {
                    array_push($this->countries, $query[$i]['description']);
                }
            }
            ///array_push($this->reportFilters,array( "countries" => $this->countries));
            $this->reportFilters["receivingCountries"] = $this->countries;
        }

    }

    function getSenderCountry()
    {
        if (($_SESSION['branch'] == "administrator" ||
                $_SESSION['branch'] == "operation" ||
                $_SESSION['branch'] == "customerservice" ||
                strtolower($_SESSION['myusername']) == "book" ||
                $_SESSION['branch'] == "office2") &&
            strtolower($_SESSION["branch"]) != 'sudan'
            && strtolower($_SESSION["branch"]) != 'thailand'
            && strtolower($_SESSION["branch"]) != 'thailand11'
            && strtolower($_SESSION["branch"]) != 'ethiopia'
            && !($_SESSION['myusername'] == "ecpay"
                || $_SESSION['myusername'] != strtolower("Galaxy")
                || $_SESSION['myusername'] == strtolower("iforex"))
        ) {


            if (strtolower($_SESSION["branch"]) == 'h.k' || $this->HKDFilterOnly) {
                $this->senderCounries = array("HONG KONG");
            } else {
                $query = $this->pdo->from('administration_exchangecountrylist', null, "v")->select('administration_exchangecountrylist.*')->orderBy('name')->fetchAll();
                for ($i = 0; $i < sizeof($query); $i++) {
                    array_push($this->senderCounries, $query[$i]['name']);
                }
            }
            $this->reportFilters["senderCountries"] = $this->senderCounries;
        }

    }

    function getTransferType()
    {
        if (strtolower($_SESSION["branch"]) != 'h.k' && !$this->HKDFilterOnly && strtolower($_SESSION["myusername"]) != 'galaxy' && strtolower($_SESSION["myusername"]) != 'ecpay') {
            $conn = wic_db_connect();

            $res1 = mysql_query("select description as transfer_via from administration_moneytransfertype where ACTIVE=1
                                   UNION select name as transfer_via from administration_exchangebanklist order by transfer_via asc");
            if ($_SESSION['autonomy'] == 2) {
                $res1 = mysql_query("select description as transfer_via from administration_moneytransfertype
                                   UNION select name as transfer_via from administration_exchangebanklist order by transfer_via asc");
            }

            $transferTypes = array();
            while ($row = mysql_fetch_array($res1)) {
                array_push($transferTypes, $row['transfer_via']);
            }
            $this->reportFilters['transferVia'] = $transferTypes;
        } else {
            $this->reportFilters['transferViaHidden'] = 'all';
        }
    }

    function getAgentsAndClerks()
    {
        if ((($_SESSION["branch"] == "administrator") || ($_SESSION["branch"] == "bookkeeping") || ($_SESSION["branch"] == "operation") || ($_SESSION["branch"] == "customerservice") || $_SESSION["user_status"] == 'supervisor')
        ) {
            $conn = wic_db_connect();
            mysql_query("set character_set_results='utf8'", $conn);
            if ($_SESSION["user_status"] == 'supervisor') {
                $sql = "select * from branches INNER JOIN  network_branches ON  network_branches.branch_num=branches.id_num
                        INNER JOIN  agents  ON agents.name='" . $_SESSION['myusername'] . "' AND network_branches .id_num=agents .network "
                    . ($this->autonomy == 1 ? "" : " AND  autonomy_fk = " . $this->autonomy . " ") . "
                        group by branch_name order by branch_name ASC ";

                $agent_id = 0;
                $res = mysql_query($sql, $conn);
                $agentsArray = array();
                while ($row = mysql_fetch_array($res)) {
                    array_push($agentArray, array($row[0] => $row['branch_name']));
                }
                $this->reportFilters['agents'] = $agentsArray;

            } else {
                $sql = "SELECT agent_id FROM `administration_moneytransfer` WHERE usd_eur = 'HKD' GROUP BY agent_id LIMIT 0 , 30";
                $agent_id = 0;
                $res = mysql_query($sql, $conn);
                $agentsArray = array();
                while ($row = mysql_fetch_array($res)) {
                    array_push($agentsArray, array($row[0] => $row['agent_id']));
                }
                $this->reportFilters['agents'] = $agentsArray;

            }
            $this->reportFilters['clerks'] = array('all-(active and archived)' => 'All-(active and archived)');
        } else {
            if (strtolower($_SESSION['branch']) == strtolower("payoutAgent")) {
                $this->reportFilters['agentsHidden'] = "All-active";
                $this->reportFilters['clerksHidden'] = "All-active";
            } else {
                if (strtolower($_SESSION['myusername']) == "book") {
                    $this->reportFilters['agentsHidden'] = "All-active";
                    $this->reportFilters['clerksHidden'] = "All-active";
                } else {

                    $this->reportFilters['agentsHidden'] = $_SESSION["branch"];

                    if ($_SESSION['user_status'] == "manager") {
                        $conn = wic_db_connect();
                        mysql_query("set character_set_results='utf8'", $conn);
                        $sql = "select * from agents JOIN branches on branches.id_num = agents.branch_num where branches.branch_name='$_SESSION[branch]' ORDER BY  `agents`.`name` ASC  ";
                        $res = mysql_query($sql, $conn);
                        $agentsArray = array();

                        while ($row = mysql_fetch_array($res)) {
                            array_push($agentsArray, array($row['name'] => $row['name']));
                        }
                        $this->reportFilters['clerks'] = $agentsArray;

                    } else {
                        $this->reportFilters['clerksHidden'] = $_SESSION["myusername"];
                    }
                }
            }
        }
    }

    function getClerksOnly($client){
        $conn = wic_db_connect();
        $sql = "SELECT user_id FROM `administration_moneytransfer` WHERE usd_eur = 'HKD' AND agent_id ='".  $client ."' GROUP BY user_id";
        $res = mysql_query($sql, $conn);
        $agentsArray = array();
        while ($row = mysql_fetch_array($res)) {
            array_push($agentsArray, array($row['user_id'] => $row['user_id']));
        }
        return $agentsArray;

    }

    function getSendingCountry()
    {
        if (($_SESSION['branch'] == "administrator" ||
                $_SESSION['branch'] == "operation" ||
                $_SESSION['branch'] == "customerservice" ||
                strtolower($_SESSION['myusername']) == "book" ||
                $_SESSION['branch'] == "office2") &&
            strtolower($_SESSION["branch"]) != 'sudan' &&
            strtolower($_SESSION["branch"]) != 'thailand' &&
            strtolower($_SESSION["branch"]) != 'thailand11' &&
            strtolower($_SESSION["branch"]) != 'ethiopia'
            && !($_SESSION['myusername'] == "ecpay" ||
                $_SESSION['myusername'] != strtolower("Galaxy") || $_SESSION['myusername'] == strtolower("iforex"))
        ) {
            if (strtolower($_SESSION["branch"]) == 'h.k' || $this->HKDFilterOnly) {
                $this->reportFilters['senderCountry'] = array('HONG KONG' => "");
            } else {
                $sender_country = $_SESSION['sender_country'];
                if ($sender_country == "") {
                    $sender_country = 'Israel';
                }
                $conn = wic_db_connect();
                $res1 = mysql_query("select * from administration_exchangecountrylist  order by name asc");
                $sendingCountry = array();
                //where name in ('Israel','Guyana')
                while ($row = mysql_fetch_array($res1)) {
                    array_push($sendingCountry, array($row['name'] => $row['name']));
                }
                $this->reportFilters['senderCountry'] = $sendingCountry;
            }

        } else {
            $sender_country = 'all';
            if ($sender_country == "") {
                $sender_country = 'Israel';
            }
            $this->reportFilters['senderCountryHidden'] = array($sender_country => "");
        }

    }

    function getStatus()
    {

        $conn = wic_db_connect();
        $sql = "select  * from administration_moneytransferstatus";
        if ($_SESSION['myusername'] == "ecpay" || strtolower($_SESSION["myusername"]) == 'galaxy' || strtolower($_SESSION["myusername"]) == 'chinanet') {
            $sql .= " where description ='POSTED'  OR description ='CONFIRMED' OR description ='FOR VERIFICATION'";
        }
        $res = mysql_query($sql, $conn);
        $statusArray = array();
        while ($row = mysql_fetch_array($res)) {
            array_push($statusArray, $row['description']);
        }
        $this->reportFilters['status'] = $statusArray;
    }

    function getCurrencies()
    {
        if (strtolower($_SESSION["branch"]) == 'h.k') {
            $currencies=array("HKD"=>"HKD","USD"=>"USD");
        }
        elseif ($this->HKDFilterOnly) {
            $currencies=array("HKD"=>"HKD");
        }
        elseif ($this->HKDFilter) {
            $currencies=array("USD"=>"USD","EUR"=>"EUR","HKD"=>"HKD");
        }
        else{
            $currencies=array("USD"=>"USD","EUR"=>"EUR");
        }
        $this->reportFilters['currencies']=$currencies;
    }

}



