<?php
/**
 * Created by PhpStorm.
 * User: zlil
 * Date: 7/6/2016
 * Time: 11:31
 * this class build the columns of the transaction report 
 * each user or branch has his unique structure 
 * the returned value from each buildUserStructure() function for ex. is 
 * array that includes all the data we need to build the report, this include 
 * the columns, headers, section headers, data, paging
 */


class DataColumnBuilder
{

    protected $userName;
    protected $branchName;
    protected $data;

    function __construct($userName, $branch = null, $data)
    {
        $this->userName = $userName;
        $this->branchName = $branch;
        $this->data = $data;
    }

    /**
     * @return array - build the transaction report structure dynamically by user or branch 
     */
    function buildDataByUser()
    {
        if ($this->branchName != 'customerservice') {
            switch ($this->userName) {
                case 'restart':
                    return $this->buildRestartStructure();
                    break;
                case 'yuval':
                    return $this->buildYuvalStructure();
                    break;
                case 'tsahi':
                    return $this->buildYuvalStructure();
                    break;
                case 'simon':
                    return $this->buildRestartStructure();
                    break;
                case 'david':
                    return $this->buildRestartStructure();
                    break;
                case 'dina':
                    return $this->buildRestartStructure();
                    break;
                case 'shaffi':
                    return $this->buildShaffiStructure();
                    break;
                case 'book':
                    return $this->buildBookStructure();
                    break;
                case 'april':
                    return $this->buildAprilStructure();
                    break;
                case 'galaxy':
                    return $this->buildGalaxyStructure();
                    break;
                default:
                    return $this->buildYuvalStructure();
                    break;
            }
        } else {
            switch ($this->branchName) {
                case 'customerservice':
                    return $this->buildCustomerServiceStructure();
                    break;
                default:
                    return $this->buildAgentStructure();
                    break;
            }

        }
    }


    function buildRestartStructure()
    {
        $size = sizeof($this->data["body"]);
        $array = array();

        for ($i = 0; $i < $size; $i++) {
            $body = array(
                "Service" => $this->data["body"][$i]["service"]["value"],
                "No" => (string)$this->data["body"][$i]["no"]["value"],
                "Date" => $this->data["body"][$i]["date"]["value"],
                "Transaction ID" => $this->data["body"][$i]["transaction_id"]["value"],
                "Receipt No" => $this->data["body"][$i]["reciept_no"]["value"],
                "Status" => $this->data["body"][$i]["status"]["value"],
                "Client" => $this->data["body"][$i]["client"]["value"],
                "Clerk" => $this->data["body"][$i]["clerk"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_sender"]["value"],
                "ID Number" => $this->data["body"][$i]["IdNumber"]["value"],
                "Israeli Citizen" => $this->data["body"][$i]["israeli_resident"]["value"],
                "Citizenship" => $this->data["body"][$i]["resident"]["value"],
                "Phone" => $this->data["body"][$i]["sender_phone"]["value"],
                "Amount" => $this->data["body"][$i]["amount"]["value"],
                "Commission" => $this->data["body"][$i]["commission"]["value"],
                "Full Name2" => $this->data["body"][$i]["full_name_receiver"]["value"],
                "ID Number" => $this->data["body"][$i]["id_number"]["value"],
                "Address2" => $this->data["body"][$i]["receiver_address"]["value"],
                "City2" => $this->data["body"][$i]["receiver_city"]["value"],
                "Phone2" => $this->data["body"][$i]["receiver_phone"]["value"],
                "Country" => $this->data["body"][$i]["country"]["value"],
                "Deliver Via" => $this->data["body"][$i]["deliver_type"]["value"],
                "Amount To Receiver" => $this->data["body"][$i]["amount_receiver"]["value"],
                "WIC Amount" => $this->data["body"][$i]["wic_amount"]["value"],
                "WIC Cost" => $this->data["body"][$i]["wic_cost"]["value"],
                "Currency Difference" => $this->data["body"][$i]["currency_difference"]["value"],
                "Comments" => $this->data["body"][$i]["comment"]["value"]);

            array_push($array, $body);
        }


        return array("body" => $array, "footer" => $this->data["footer"], "additionalInfo" => $this->data["additionalInfo"], "columns" => $this->buildColumnsForRestart(), "user" => $this->userName, "statusChange"=>$this->data["statusChange"], "headerInfo" => $this->buildRowHeaderRestart());


    }

    function buildRowHeaderRestart()
    {
        $information = array();
        $firstSpace = array("rowspan" => 1, "colspan" => 9, "text" => "");
        $sender = array("rowspan" => 1, "colspan" => 4, "text" => "Sender");
        $receiver = array("rowspan" => 1, "colspan" => 9, "text" => "Receiver");

        array_push($information, $firstSpace, $sender, $receiver);
        return json_encode($information);

    }

    function buildColumnsForRestart()
    {
        $coloumns = array();
        $No = array("data" => 'No', "title" => 'No');
        $Info = array("class" => "details-control", "orderable" => false, "data" => null, "defaultContent" => "", "title" => "Info");
        $Service = array("data" => 'Service', "title" => 'Service', "render" => function ($data, $type, $full, $meta) {
            $data = "hhhhh";
            if ($data == "Total") {
                return 'Total:';
            } else if ($data == "Total Page") {
                return 'Total Page:';
            } else
                return '<a href="customerService.php?transaction_id=' + data + '&agent_name=administrator" target="_blank"><img src="../images/CustomerServiceIcon.png" style="height: 40px"."></a>';
        });
        $date = array("data" => "Date", "title" => "Date");
        $transaction = array("data" => "Transaction ID", "title" => "Transaction ID");
        $receipt = array("data" => "Receipt No", "title" => "Receipt No");
        $status = array("data" => "Status", "title" => "Status");
        $client = array("data" => "Client", "title" => "Client");
        $clerk = array("data" => "Clerk", "title" => "Clerk");
        $senderName = array("data" => "Full Name", "title" => "Full Name");
        $senderID = array("data" => "ID Number", "title" => "ID Number");
        $amount = array("data" => "Amount", "title" => "Amount");
        $commission = array("data" => "Commission", "title" => "Commission");
        $receiverName = array("data" => "Full Name", "title" => "Full Name");
        $receiverID = array("data" => "ID Number", "title" => "ID Number");
        $country = array("data" => "Country", "title" => "Country");
        $deliverVia = array("data" => "Deliver Via", "title" => "Deliver Via");
        $amountToReceiver = array("data" => "Amount To Receiver", "title" => "Amount To Receiver");
        $wicAmount = array("data" => "WIC Amount", "title" => "WIC Amount");
        $wicCost = array("data" => "WIC Cost", "title" => "WIC Cost");
        $currDiff = array("data" => "Currency Difference", "title" => "Currency Difference");
        $comments = array("data" => "Comments", "title" => "Comments");

        array_push($coloumns, $No, $Info, $Service, $date, $transaction, $receipt, $status, $client, $clerk, $senderName, $senderID, $amount, $commission, $receiverName, $receiverID, $country, $deliverVia, $amountToReceiver, $wicAmount, $wicCost, $currDiff, $comments);
        return json_encode($coloumns);

    }

    function buildYuvalStructure()
    {

        $size = sizeof($this->data["body"]);
        $array = array();

        for ($i = 0; $i < $size; $i++) {

            $body = array(
                "No" => (string)$this->data["body"][$i]["no"]["value"],
                "Date" => $this->data["body"][$i]["date"]["value"],
                "Transaction ID" => $this->data["body"][$i]["transaction_id"]["value"],
                "Receipt No" => $this->data["body"][$i]["reciept_no"]["value"],
                "Status" => $this->data["body"][$i]["status"]["value"],
                "Client" => $this->data["body"][$i]["client"]["value"],
                "Clerk" => $this->data["body"][$i]["clerk"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_sender"]["value"],
                "City" => $this->data["body"][$i]["sender_city"]["value"],
                "Address" => $this->data["body"][$i]["sender_address"]["value"],
                "ID Number" => $this->data["body"][$i]["IdNumber"]["value"],
                "Phone" => $this->data["body"][$i]["sender_phone"]["value"],
                "Amount" => $this->data["body"][$i]["amount"]["value"],
                "Commission" => $this->data["body"][$i]["commission"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_receiver"]["value"],
                "Address" => $this->data["body"][$i]["receiver_address"]["value"],
                "City" => $this->data["body"][$i]["receiver_city"]["value"],
                "Phone" => $this->data["body"][$i]["receiver_phone"]["value"],
                "Country" => $this->data["body"][$i]["country"]["value"],
                "Coin ID" => $this->data["body"][$i]["coin_id"]["value"],
                "Deliver Via" => $this->data["body"][$i]["deliver_type"]["value"],
                "Bank Name" => $this->data["body"][$i]["bank_description"]["value"],
                "Account Number" => $this->data["body"][$i]["bank_account_direct"]["value"],
                "Amount To Receiver" => $this->data["body"][$i]["amount_receiver"]["value"],
                "Agent Commission" => $this->data["body"][$i]["agent_commission"]["value"],
                "Rate" => $this->data["body"][$i]["rate"]["value"],
                "Buy Rate" => $this->data["body"][$i]["buy_rate"]["value"],
                "Amount NIS" => (string)$this->data["body"][$i]["amount_nis"]["value"],
                "WIC Amount" => $this->data["body"][$i]["wic_amount"]["value"],
                "WIC Cost" => $this->data["body"][$i]["wic_cost"]["value"],
                "Currency Difference" => $this->data["body"][$i]["currency_difference"]["value"],
                "Comments" => $this->data["body"][$i]["comment"]["value"]);

            array_push($array, $body);
        }


        return array("body" => $array, "footer" => $this->data["footer"], "additionalInfo" => $this->data["additionalInfo"], "columns" => $this->buildColumnsForYuval(), "user" => $this->userName, "statusChange"=>$this->data["statusChange"], "headerInfo" => $this->buildRowHeaderYuval());

    }

    function buildRowHeaderYuval()
    {
        $information = array();
        $firstSpace = array("rowspan" => 1, "colspan" => 7, "text" => "");
        $sender = array("rowspan" => 1, "colspan" => 7, "text" => "Sender");
        $receiver = array("rowspan" => 1, "colspan" => 5, "text" => "Receiver");
        $moneyInfo = array("rowspan" => 1, "colspan" => 13, "text" => "Money Info.");

        array_push($information, $firstSpace, $sender, $receiver, $moneyInfo);
        return json_encode($information);

    }

    function buildColumnsForYuval()
    {
        $coloumns = array();
        $No = array("data" => 'No', "title" => 'No');
        $date = array("data" => "Date", "title" => "Date");
        $transaction = array("data" => "Transaction ID", "title" => "Transaction ID");
        $receipt = array("data" => "Receipt No", "title" => "Receipt No");
        $status = array("data" => "Status", "title" => "Status");
        $client = array("data" => "Client", "title" => "Client");
        $clerk = array("data" => "Clerk", "title" => "Clerk");
        $senderName = array("data" => "Full Name", "title" => "Full Name");
        $city = array("data" => 'City', "title" => 'City');
        $address = array("data" => 'Address', "title" => 'Address');
        $senderID = array("data" => "ID Number", "title" => "ID Number");
        $phone = array("data" => 'Phone', "title" => 'Phone');
        $amount = array("data" => "Amount", "title" => "Amount");
        $commission = array("data" => "Commission", "title" => "Commission");
        $receiverName = array("data" => "Full Name", "title" => "Full Name");
        $receiverAddress = array("data" => "Address", "title" => "Address");
        $receiverCity = array("data" => "City", "title" => "City");
        $receiverPhone = array("data" => "Phone", "title" => "Phone");
        $country = array("data" => "Country", "title" => "Country");
        $coin = array("data" => "Coin ID", "title" => "Coin ID");
        $deliverVia = array("data" => "Deliver Via", "title" => "Deliver Via");
        $bankName = array("data" => "Bank Name", "title" => "Bank Name");
        $bankAccount = array("data" => "Account Number", "title" => "Account Number");
        $amountToReceiver = array("data" => "Amount To Receiver", "title" => "Amount To Receiver");
        $agentCommission = array("data" => "Agent Commission", "title" => "Agent Commission");
        $rate = array("data" => "Rate", "title" => "Rate");
        $buyRate = array("data" => "Buy Rate", "title" => "Buy Rate");
        $amountNIS = array("data" => "Amount NIS", "title" => "Amount NIS");
        $wicAmount = array("data" => "WIC Amount", "title" => "WIC Amount");
        $wicCost = array("data" => "WIC Cost", "title" => "WIC Cost");
        $currDiff = array("data" => "Currency Difference", "title" => "Currency Difference");
        $comments = array("data" => "Comments", "title" => "Comments");

        array_push($coloumns, $No, $date, $transaction, $receipt, $status,  $client, $clerk, $senderName, $city, $address, $senderID, $phone,  $amount, $commission, $receiverName,  $receiverAddress, $receiverCity, $receiverPhone, $country, $coin, $deliverVia, $bankName, $bankAccount, $amountToReceiver, $agentCommission, $rate, $buyRate, $amountNIS, $wicAmount, $wicCost, $currDiff, $comments);
        return json_encode($coloumns);

    }

    function buildShaffiStructure()
    {

        $size = sizeof($this->data["body"]);
        $array = array();

        for ($i = 0; $i < $size; $i++) {
            $body = array(
                "Service" => $this->data["body"][$i]["service"]["value"],
                "No" => (string)$this->data["body"][$i]["no"]["value"],
                "Date" => $this->data["body"][$i]["date"]["value"],
                "Transaction ID" => $this->data["body"][$i]["transaction_id"]["value"],
                "VIP" => (string)$this->data["body"][$i]["vip_no"]["value"],
                "Status" => $this->data["body"][$i]["status"]["value"],
                "Client" => $this->data["body"][$i]["client"]["value"],
                "Clerk" => $this->data["body"][$i]["clerk"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_sender"]["value"],
                "ID Number" => $this->data["body"][$i]["IdNumber"]["value"],
                "Citizenship" => $this->data["body"][$i]["resident"]["value"],
                "ID Type" => $this->data["body"][$i]["id_type"]["value"],
                "City" => $this->data["body"][$i]["sender_city"]["value"],
                "Address" => $this->data["body"][$i]["sender_address"]["value"],
                "Phone" => $this->data["body"][$i]["sender_phone"]["value"],
                "Date of Birth" => $this->data["body"][$i]["sender_birth"]["value"],
                "Amount" => $this->data["body"][$i]["amount"]["value"],
                "Commission" => $this->data["body"][$i]["commission"]["value"],
                "Full Name2" => $this->data["body"][$i]["full_name_receiver"]["value"],
                "ID Number" => $this->data["body"][$i]["id_number"]["value"],
                "Address2" => $this->data["body"][$i]["receiver_address"]["value"],
                "City2" => $this->data["body"][$i]["receiver_city"]["value"],
                "Phone2" => $this->data["body"][$i]["receiver_phone"]["value"],
                "Country" => $this->data["body"][$i]["country"]["value"],
                "Coin ID" => $this->data["body"][$i]["coin_id"]["value"],
                "Deliver Via" => $this->data["body"][$i]["deliver_type"]["value"],
                "Bank Name" => $this->data["body"][$i]["bank_description"]["value"],
                "Account Number" => $this->data["body"][$i]["bank_account_direct"]["value"],
                "Transaction Natures" => $this->data["body"][$i]["natures"]["value"],
                "Amount To Receiver" => $this->data["body"][$i]["amount_receiver"]["value"],
                "Agent Commission" => $this->data["body"][$i]["agent_commission"]["value"],
                "AT services cost" => $this->data["body"][$i]["ATServicesCost"]["value"],
                "Currency Income" => $this->data["body"][$i]["currency_difference"]["value"],
                "Comments" => $this->data["body"][$i]["comment"]["value"]);

            array_push($array, $body);
        }


        return array("body" => $array, "footer" => $this->data["footer"], "additionalInfo" => $this->data["additionalInfo"], "columns" => $this->buildColumnsForShaffi(), "user" =>$this->userName, "statusChange"=>$this->data["statusChange"], "headerInfo" => $this->buildRowHeaderShaffi());


    }

    function buildRowHeaderShaffi()
    {
        $information = array();
        $firstSpace = array("rowspan" => 1, "colspan" => 9, "text" => "");
        $sender = array("rowspan" => 1, "colspan" => 10, "text" => "Sender");
        $receiver = array("rowspan" => 1, "colspan" => 6, "text" => "Receiver");
        $moneyInfo = array("rowspan" => 1, "colspan" => 10, "text" => "Money Info.");

        array_push($information, $firstSpace, $sender, $receiver, $moneyInfo);
        return json_encode($information);

    }


    function buildColumnsForShaffi()
    {
        $coloumns = array();
        $No = array("data" => 'No', "title" => 'No');
        $Service = array("data" => 'Service', "title" => 'Service', "render" => function ($data, $type, $full, $meta) {
            if ($data == "Total") {
                return 'Total:';
            } else if ($data == "Total Page") {
                return 'Total Page:';
            } else
                return 'TESTTT';
        });
        $date = array("data" => "Date", "title" => "Date");
        $transaction = array("data" => "Transaction ID", "title" => "Transaction ID");
        $Vip = array("data" => 'VIP', "title" => 'VIP');
        $status = array("data" => "Status", "title" => "Status");
        $client = array("data" => "Client", "title" => "Client");
        $clerk = array("data" => "Clerk", "title" => "Clerk");
        $senderName = array("data" => "Full Name", "title" => "Full Name");
        $city = array("data" => 'City', "title" => 'City');
        $address = array("data" => 'Address', "title" => 'Address');
        $citizenship = array("data" => 'Citizenship', "title" => 'Citizenship');
        $idtype = array("data" => 'ID Type', "title" => 'ID Type');
        $senderID = array("data" => "ID Number", "title" => "ID Number");
        $phone = array("data" => 'Phone', "title" => 'Phone');
        $birth = array("data" => 'Date of Birth', "title" => 'Date of Birth');
        $amount = array("data" => "Amount", "title" => "Amount");
        $commission = array("data" => "Commission", "title" => "Commission");
        $receiverName = array("data" => "Full Name", "title" => "Full Name");
        $receiverID = array("data" => "ID Number", "title" => "ID Number");
        $receiverAddress = array("data" => "Address", "title" => "Address");
        $receiverCity = array("data" => "City", "title" => "City");
        $receiverPhone = array("data" => "Phone", "title" => "Phone");
        $country = array("data" => "Country", "title" => "Country");
        $coin = array("data" => "Coin ID", "title" => "Coin ID");
        $deliverVia = array("data" => "Deliver Via", "title" => "Deliver Via");
        $bankName = array("data" => "Bank Name", "title" => "Bank Name");
        $bankAccount = array("data" => "Account Number", "title" => "Account Number");
        $natures = array("data" => "Transaction Natures", "title" => "Transaction Natures");
        $amountToReceiver = array("data" => "Amount To Receiver", "title" => "Amount To Receiver");
        $agentCommission = array("data" => "Agent Commission", "title" => "Agent Commission");
        $ATCost = array("data" => "AT services cost", "title" => "AT services cost");
        $currIncome = array("data" => "Currency Income", "title" => "Currency Income");
        $comments = array("data" => "Comments", "title" => "Comments");

        array_push($coloumns, $No, $Service, $date, $transaction, $Vip, $status, $client, $clerk, $senderName, $city, $address, $citizenship, $idtype, $senderID, $phone, $birth, $amount, $commission, $receiverName, $receiverID, $receiverAddress, $receiverCity, $receiverPhone, $country, $coin, $deliverVia, $bankName, $bankAccount, $natures, $amountToReceiver, $agentCommission, $ATCost, $currIncome, $comments);
        return json_encode($coloumns);

    }

    function buildBookStructure()
    {

        $size = sizeof($this->data["body"]);
        $array = array();

        for ($i = 0; $i < $size; $i++) {
            $body = array(
                "No" => (string)$this->data["body"][$i]["no"]["value"],
                "Date" => $this->data["body"][$i]["date"]["value"],
                "Transaction ID" => $this->data["body"][$i]["transaction_id"]["value"],
                "Receipt No" => $this->data["body"][$i]["reciept_no"]["value"],
                "Status" => $this->data["body"][$i]["status"]["value"],
                "Amount" => $this->data["body"][$i]["amount"]["value"],
                "Commission" => $this->data["body"][$i]["commission"]["value"],
                "Country" => $this->data["body"][$i]["country"]["value"],
                "Deliver Via" => $this->data["body"][$i]["deliver_type"]["value"],
                "Amount To Receiver" => $this->data["body"][$i]["amount_receiver"]["value"],
                "WIC Rate" => $this->data["body"][$i]["buy_rate"]["value"],
                "WIC Amount" => $this->data["body"][$i]["wic_amount"]["value"],
                "Currency Income" => $this->data["body"][$i]["currency_difference"]["value"]);

            array_push($array, $body);
        }


        return array("body" => $array, "footer" => $this->data["footer"], "additionalInfo" => $this->data["additionalInfo"], "columns" => $this->buildColumnsForBook(), "user" => 'book', "statusChange"=>$this->data["statusChange"], "headerInfo" => "");

    }


    function buildColumnsForBook()
    {
        $coloumns = array();
        $No = array("data" => 'No', "title" => 'No');
        $date = array("data" => "Date", "title" => "Date");
        $transaction = array("data" => "Transaction ID", "title" => "Transaction ID");
        $receipt = array("data" => "Receipt No", "title" => "Receipt No");
        $status = array("data" => "Status", "title" => "Status");
        $amount = array("data" => "Amount", "title" => "Amount");
        $commission = array("data" => "Commission", "title" => "Commission");
        $country = array("data" => "Country", "title" => "Country");
        $deliverVia = array("data" => "Deliver Via", "title" => "Deliver Via");
        $amountToReceiver = array("data" => "Amount To Receiver", "title" => "Amount To Receiver");
        $wicRate = array("data" => "WIC Rate", "title" => "WIC Rate");
        $wicAmount = array("data" => "WIC Amount", "title" => "WIC Amount");
        $currIncome = array("data" => "Currency Income", "title" => "Currency Income");

        array_push($coloumns, $No, $date, $transaction, $receipt, $status, $amount, $commission, $country, $deliverVia, $amountToReceiver, $wicRate, $wicAmount, $currIncome);
        return json_encode($coloumns);

    }

    function buildAgentStructure()
    {

        $size = sizeof($this->data["body"]);
        $array = array();

        for ($i = 0; $i < $size; $i++) {

            $body = array(
                "Service" => $this->data["body"][$i]["service"]["value"],
                "No" => (string)$this->data["body"][$i]["no"]["value"],
                "Date" => $this->data["body"][$i]["date"]["value"],
                "Transaction ID" => $this->data["body"][$i]["transaction_id"]["value"],
                "Receipt No" => $this->data["body"][$i]["reciept_no"]["value"],
                "Status" => $this->data["body"][$i]["status"]["value"],
                "Client" => $this->data["body"][$i]["client"]["value"],
                "Clerk" => $this->data["body"][$i]["clerk"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_sender"]["value"],
                "ID Number" => $this->data["body"][$i]["IdNumber"]["value"],
                "Amount" => $this->data["body"][$i]["amount"]["value"],
                "Commission" => $this->data["body"][$i]["commission"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_receiver"]["value"],
                "ID Number" => $this->data["body"][$i]["id_number"]["value"],
                "Country" => $this->data["body"][$i]["country"]["value"],
                "Deliver Via" => $this->data["body"][$i]["deliver_type"]["value"],
                "Comments" => $this->data["body"][$i]["comment"]["value"]);

            array_push($array, $body);
        }


        return array("body" => $array, "footer" => $this->data["footer"], "additionalInfo" => $this->data["additionalInfo"], "columns" => $this->buildColumnsForAgent(), "user" => $this->userName, "statusChange"=>$this->data["statusChange"], "headerInfo" => $this->buildRowHeaderAgent());

    }

    function buildRowHeaderAgent()
    {
        $information = array();
        $firstSpace = array("rowspan" => 1, "colspan" => 9, "text" => "");
        $sender = array("rowspan" => 1, "colspan" => 4, "text" => "Sender");
        $receiver = array("rowspan" => 1, "colspan" => 5, "text" => "Receiver");

        array_push($information, $firstSpace, $sender, $receiver);
        return json_encode($information);

    }


    function buildColumnsForAgent()
    {
        $coloumns = array();
        $No = array("data" => 'No', "title" => 'No');
        $Info = array("class" => "details-control", "orderable" => false, "data" => null, "defaultContent" => "", "title" => "Info");
        $Service = array("data" => 'Service', "title" => 'Service', "render" => function ($data, $type, $full, $meta) {
            if ($data == "Total") {
                return 'Total:';
            } else if ($data == "Total Page") {
                return 'Total Page:';
            } else
                return 'TESTTT';
        });
        $date = array("data" => "Date", "title" => "Date");
        $transaction = array("data" => "Transaction ID", "title" => "Transaction ID");
        $receipt = array("data" => "Receipt No", "title" => "Receipt No");
        $status = array("data" => "Status", "title" => "Status");
        $client = array("data" => "Client", "title" => "Client");
        $clerk = array("data" => "Clerk", "title" => "Clerk");
        $senderName = array("data" => "Full Name", "title" => "Full Name");
        $senderID = array("data" => "ID Number", "title" => "ID Number");
        $amount = array("data" => "Amount", "title" => "Amount");
        $commission = array("data" => "Commission", "title" => "Commission");
        $receiverName = array("data" => "Full Name", "title" => "Full Name");
        $receiverID = array("data" => "ID Number", "title" => "ID Number");
        $country = array("data" => "Country", "title" => "Country");
        $deliverVia = array("data" => "Deliver Via", "title" => "Deliver Via");
        $comments = array("data" => "Comments", "title" => "Comments");

        array_push($coloumns, $No, $Info, $Service, $date, $transaction, $receipt, $status, $client, $clerk, $senderName, $senderID, $amount, $commission, $receiverName, $receiverID, $country, $deliverVia, $comments);
        return json_encode($coloumns);
    }

    function buildAprilStructure()
    {
        $size = sizeof($this->data["body"]);
        $array = array();

        for ($i = 0; $i < $size; $i++) {

            $body = array(
                "Service" => $this->data["body"][$i]["service"]["value"],
                "No" => (string)$this->data["body"][$i]["no"]["value"],
                "Date" => $this->data["body"][$i]["date"]["value"],
                "Transaction ID" => $this->data["body"][$i]["transaction_id"]["value"],
                "Receipt No" => $this->data["body"][$i]["reciept_no"]["value"],
                "Status" => $this->data["body"][$i]["status"]["value"],
                "Client" => $this->data["body"][$i]["client"]["value"],
                "Clerk" => $this->data["body"][$i]["clerk"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_sender"]["value"],
                "ID Number" => $this->data["body"][$i]["IdNumber"]["value"],
                "Amount" => $this->data["body"][$i]["amount"]["value"],
                "Commission" => $this->data["body"][$i]["commission"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_receiver"]["value"],
                "ID Number" => $this->data["body"][$i]["id_number"]["value"],
                "Country" => $this->data["body"][$i]["country"]["value"],
                "Deliver Via" => $this->data["body"][$i]["deliver_type"]["value"],
                "Amount To Receiver" => $this->data["body"][$i]["amount_receiver"]["value"],
                "Comments" => $this->data["body"][$i]["comment"]["value"]);

            array_push($array, $body);
        }


        return array("body" => $array, "footer" => $this->data["footer"], "additionalInfo" => $this->data["additionalInfo"], "columns" => $this->buildColumnsForApril(), "user" => $this->userName, "statusChange"=>$this->data["statusChange"], "headerInfo" => $this->buildRowHeaderApril());

    }


    function buildRowHeaderApril()
    {
        $information = array();
        $firstSpace = array("rowspan" => 1, "colspan" => 9, "text" => "");
        $sender = array("rowspan" => 1, "colspan" => 4, "text" => "Sender");
        $receiver = array("rowspan" => 1, "colspan" => 6, "text" => "Receiver");

        array_push($information, $firstSpace, $sender, $receiver);
        return json_encode($information);

    }

    function buildColumnsForApril()
    {
        $coloumns = array();
        $No = array("data" => 'No', "title" => 'No');
        $Info = array("class" => "details-control", "orderable" => false, "data" => null, "defaultContent" => "", "title" => "Info");
        $Service = array("data" => 'Service', "title" => 'Service', "render" => function ($data, $type, $full, $meta) {
            if ($data == "Total") {
                return 'Total:';
            } else if ($data == "Total Page") {
                return 'Total Page:';
            } else
                return 'TESTTT';
        });
        $date = array("data" => "Date", "title" => "Date");
        $transaction = array("data" => "Transaction ID", "title" => "Transaction ID");
        $receipt = array("data" => "Receipt No", "title" => "Receipt No");
        $status = array("data" => "Status", "title" => "Status");
        $client = array("data" => "Client", "title" => "Client");
        $clerk = array("data" => "Clerk", "title" => "Clerk");
        $senderName = array("data" => "Full Name", "title" => "Full Name");
        $senderID = array("data" => "ID Number", "title" => "ID Number");
        $amount = array("data" => "Amount", "title" => "Amount");
        $commission = array("data" => "Commission", "title" => "Commission");
        $receiverName = array("data" => "Full Name", "title" => "Full Name");
        $receiverID = array("data" => "ID Number", "title" => "ID Number");
        $country = array("data" => "Country", "title" => "Country");
        $deliverVia = array("data" => "Deliver Via", "title" => "Deliver Via");
        $amountToReceiver = array("data" => "Amount To Receiver", "title" => "Amount To Receiver");
        $comments = array("data" => "Comments", "title" => "Comments");

        array_push($coloumns, $No, $Info, $Service, $date, $transaction, $receipt, $status, $client, $clerk, $senderName, $senderID, $amount, $commission, $receiverName, $receiverID, $country, $deliverVia, $amountToReceiver, $comments);
        return json_encode($coloumns);
    }

    function buildCustomerServiceStructure()
    {
        $size = sizeof($this->data["body"]);
        $array = array();

        for ($i = 0; $i < $size; $i++) {

            $body = array(
                "Service" => $this->data["body"][$i]["service"]["value"],
                "No" => (string)$this->data["body"][$i]["no"]["value"],
                "Date" => $this->data["body"][$i]["date"]["value"],
                "Transaction ID" => $this->data["body"][$i]["transaction_id"]["value"],
                "Receipt No" => $this->data["body"][$i]["reciept_no"]["value"],
                "Status" => $this->data["body"][$i]["status"]["value"],
                "Client" => $this->data["body"][$i]["client"]["value"],
                "Clerk" => $this->data["body"][$i]["clerk"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_sender"]["value"],
                "ID Number" => $this->data["body"][$i]["IdNumber"]["value"],
                "Amount" => $this->data["body"][$i]["amount"]["value"],
                "Commission" => $this->data["body"][$i]["commission"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_receiver"]["value"],
                "ID Number" => $this->data["body"][$i]["id_number"]["value"],
                "Country" => $this->data["body"][$i]["country"]["value"],
                "Deliver Via" => $this->data["body"][$i]["deliver_type"]["value"],
                "Amount To Receiver" => $this->data["body"][$i]["amount_receiver"]["value"],
                "Comments" => $this->data["body"][$i]["comment"]["value"]);

            array_push($array, $body);
        }


        return array("body" => $array, "footer" => $this->data["footer"], "additionalInfo" => $this->data["additionalInfo"], "columns" => $this->buildColumnsForCustomerService(), "user" => $this->branchName, "statusChange"=>$this->data["statusChange"], "headerInfo" => $this->buildRowHeaderCustomerService());

    }


    function buildRowHeaderCustomerService()
    {
        $information = array();
        $firstSpace = array("rowspan" => 1, "colspan" => 9, "text" => "");
        $sender = array("rowspan" => 1, "colspan" => 4, "text" => "Sender");
        $receiver = array("rowspan" => 1, "colspan" => 6, "text" => "Receiver");

        array_push($information, $firstSpace, $sender, $receiver);
        return json_encode($information);

    }

    function buildColumnsForCustomerService()
    {
        $coloumns = array();
        $No = array("data" => 'No', "title" => 'No');
        $Info = array("class" => "details-control", "orderable" => false, "data" => null, "defaultContent" => "", "title" => "Info");
        $Service = array("data" => 'Service', "title" => 'Service', "render" => function ($data, $type, $full, $meta) {
            if ($data == "Total") {
                return 'Total:';
            } else if ($data == "Total Page") {
                return 'Total Page:';
            } else
                return 'TESTTT';
        });
        $date = array("data" => "Date", "title" => "Date");
        $transaction = array("data" => "Transaction ID", "title" => "Transaction ID");
        $receipt = array("data" => "Receipt No", "title" => "Receipt No");
        $status = array("data" => "Status", "title" => "Status");
        $client = array("data" => "Client", "title" => "Client");
        $clerk = array("data" => "Clerk", "title" => "Clerk");
        $senderName = array("data" => "Full Name", "title" => "Full Name");
        $senderID = array("data" => "ID Number", "title" => "ID Number");
        $amount = array("data" => "Amount", "title" => "Amount");
        $commission = array("data" => "Commission", "title" => "Commission");
        $receiverName = array("data" => "Full Name", "title" => "Full Name");
        $receiverID = array("data" => "ID Number", "title" => "ID Number");
        $country = array("data" => "Country", "title" => "Country");
        $deliverVia = array("data" => "Deliver Via", "title" => "Deliver Via");
        $amountToReceiver = array("data" => "Amount To Receiver", "title" => "Amount To Receiver");
        $comments = array("data" => "Comments", "title" => "Comments");

        array_push($coloumns, $No, $Info, $Service, $date, $transaction, $receipt, $status, $client, $clerk, $senderName, $senderID, $amount, $commission, $receiverName, $receiverID, $country, $deliverVia, $amountToReceiver, $comments);
        return json_encode($coloumns);
    }


    function buildGalaxyStructure()
    {

        $size = sizeof($this->data["body"]);
        $array = array();

        for ($i = 0; $i < $size; $i++) {

            $body = array(
                "Service" => $this->data["body"][$i]["service"]["value"],
                "No" => (string)$this->data["body"][$i]["no"]["value"],
                "Date" => $this->data["body"][$i]["date"]["value"],
                "Transaction ID" => $this->data["body"][$i]["transaction_id"]["value"],
                "Receipt No" => $this->data["body"][$i]["reciept_no"]["value"],
                "Citizenship" => $this->data["body"][$i]["resident"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_sender"]["value"],
                "ID Number" => $this->data["body"][$i]["id_number"]["value"],
                "Full Name" => $this->data["body"][$i]["full_name_receiver"]["value"],
                "Phone" => $this->data["body"][$i]["receiver_phone"]["value"],
                "Full Name Local" => $this->data["body"][$i]["receiver_full_name_local"]["value"],
                "Country" => $this->data["body"][$i]["country"]["value"],
                "Deliver Via" => $this->data["body"][$i]["deliver_type"]["value"],
                "Bank Name" => $this->data["body"][$i]["bank_description"]["value"],
                "Bank Local" => $this->data["body"][$i]["receiver_bank_local_name"]["value"],
                "Client" => $this->data["body"][$i]["client"]["value"],
                "Client Local" => $this->data["body"][$i]["receiver_branch_local_name"]["value"],
                "Address" => $this->data["body"][$i]["receiver_address"]["value"],
                "Address Local" => $this->data["body"][$i]["receiver_branch_address_local"]["value"],
                "Account Number" => $this->data["body"][$i]["bank_account_direct"]["value"],
                "Amount To Receiver" => $this->data["body"][$i]["amount_receiver"]["value"],
                "Status" => $this->data["body"][$i]["status"]["value"],
                "WIC Amount" => $this->data["body"][$i]["wic_amount"]["value"],
                "WIC Cost" => $this->data["body"][$i]["wic_cost"]["value"],
                "Comments" => $this->data["body"][$i]["comment"]["value"]);

            array_push($array, $body);
        }


        return array("body" => $array, "footer" => $this->data["footer"],"additionalInfo" => $this->data["additionalInfo"], "columns" => $this->buildColumnsForGalaxy(), "user" =>$this->userName, "statusChange"=>$this->data["statusChange"], "headerInfo" => $this->buildRowHeaderGalaxy());

    }

    function buildRowHeaderGalaxy()
    {
        $information = array();
        $firstSpace = array("rowspan" => 1, "colspan" => 6, "text" => "");
        $sender = array("rowspan" => 1, "colspan" => 2, "text" => "Sender");
        $receiver = array("rowspan" => 1, "colspan" => 18, "text" => "Receiver");

        array_push($information, $firstSpace, $sender, $receiver);
        return json_encode($information);

    }

    function buildColumnsForGalaxy()
    {
        $coloumns = array();
        $No = array("data" => 'No', "title" => 'No');
        $Info = array("class" => "details-control", "orderable" => false, "data" => null, "defaultContent" => "", "title" => "Info");
        $Service = array("data" => 'Service', "title" => 'Service', "render" => function ($data, $type, $full, $meta) {
            if ($data == "Total") {
                return 'Total:';
            } else if ($data == "Total Page") {
                return 'Total Page:';
            } else
                return 'TESTTT';
        });
        $date = array("data" => "Date", "title" => "Date");
        $transaction = array("data" => "Transaction ID", "title" => "Transaction ID");
        $receipt = array("data" => "Receipt No", "title" => "Receipt No");
        $citizenship = array("data" => 'Citizenship', "title" => 'Sender Country');
        $senderName = array("data" => "Full Name", "title" => "Full Name");
        $receiverID = array("data" => "ID Number", "title" => "ID Number");
        $receiverName = array("data" => "Full Name", "title" => "Full Name");
        $phone = array("data" => 'Phone', "title" => 'Phone');
        $receiverNameLocal = array("data" => "Full Name Local", "title" => "Full Name Local");
        $country = array("data" => "Country", "title" => "Country");
        $deliverVia = array("data" => "Deliver Via", "title" => "Deliver Via");
        $bankName = array("data" => "Bank Name", "title" => "Bank");
        $bankNameLocal = array("data" => "Bank Local", "title" => "Bank Local");
        $client = array("data" => "Client", "title" => "Client");
        $clientLocal = array("data" => "Client Local", "title" => "Client Local");
        $receiverAddress = array("data" => "Address", "title" => "Address");
        $receiverAddressLocal = array("data" => "Address Local", "title" => "Address Local");
        $bankAccount = array("data" => "Account Number", "title" => "Account Number");
        $amountToReceiver = array("data" => "Amount To Receiver", "title" => "Amount To Receiver");
        $status = array("data" => "Status", "title" => "Status");
        $wicAmount = array("data" => "WIC Amount", "title" => "Amount to pay by WIC");
        $wicCost = array("data" => "WIC Cost", "title" => "WIC Cost");
        $comments = array("data" => "Comments", "title" => "Comments");


        array_push($coloumns, $No, $Info, $Service, $date, $transaction, $receipt, $citizenship, $senderName, $receiverID, $receiverName, $phone, $receiverNameLocal, $country, $deliverVia, $bankName, $bankNameLocal, $client, $clientLocal, $receiverAddress, $receiverAddressLocal, $bankAccount, $amountToReceiver, $status ,$wicAmount, $wicCost,  $comments);
        return json_encode($coloumns);

    }

}
