<?php
error_reporting(0);
/**
 * Class PDOService
 *
 * documentation  - https://github.com/fpdo/fluentpdo
 */

abstract class PDOService
{

    protected $pdo;
    protected $table;
    protected $valueArr;
    protected $valueArrWhere;


    function __construct($table)
    {
        $pdo = new PDO("mysql:dbname=wic_hk", "root", "basket05");
        $pdo->exec("set names utf8");
        $this->pdo = new FluentPDO($pdo);
        $this->table = $table;
        $this->valueArr = array();
        $this->valueArrWhere = array();
        // log queries to STDERR (for console debugging)

    }

    /**
     * insert to db values
     * @return int
     * @throws Exception
     */
    function insertToDB(){
        try{
            $res = $this->pdo->insertInto($this->table)->values($this->valueArr)->execute();
            return $res;
        }catch (Exception $e){
            throw new Exception($e->getMessage());

        }

    }

    /**
     * @param $table - table name
     * @param $vals = value to be inserted, array of value for example  $arr['bank_report.bank_name']=$bank_name;
     * @return int - success or not
     * @throws Exception
     */
    function insertToDBbyVal($table,$vals){
        try{
            $res = $this->pdo->insertInto($table)->values($vals)->execute();
            return $res;
        }catch (Exception $e){
            throw new Exception($e->getMessage());

        }

    }

    /** Generic update table with where values and set values
     *
     * @return bool|int|PDOStatement
     * @throws Exception
     */
    function updateDB(){
        try{
            if (count($this->valueArrWhere) > 0){
                $query = $this->pdo->update($this->table)->set($this->valueArr)->where($this->valueArrWhere);
            }else{
                $query = $this->pdo->update($this->table)->set($this->valueArr);
            }
            $res =  $query->execute();

            return $res;
        }catch (Exception $e){
            throw new Exception($e->getMessage());

        }

    }

    /**
     * @param $table - table name to be updated
     * @param $values - columns to be update for example  $arrVal['administration_agent.'.$field]=  new FluentLiteral('administration_agent.'. $field .  '+' . $creditUsedDelta);
     * @param $where - array of where conditions for example $ArrWhere[administraion_moneytransfer.id]=$id
     * @return bool|int|PDOStatement - if the update success or not
     * @throws Exception
     */

    function updateDBvals($table,$values,$where){
        try{
            if (count($where) > 0){
                $query = $this->pdo->update($table)->set($values)->where($where);
            }else{
                $query = $this->pdo->update($table)->set($values);
            }
            $res =  $query->execute();

            return $res;
        }catch (Exception $e){
            throw new Exception($e->getMessage());

        }

    }

    public abstract function selectDB();

    /**
     * select all db with where value
     * @return SelectQuery
     */

    public function selectWhereValDB()
    {
         $query=$this->pdo->from($this->table,null,"v")->select($this->table.'.*')->where($this->valueArrWhere)->fetchAll();
            return $query;
    }

    /**
     * @param $table - table name
     * @param $select - columns to be selected, should be for example: 'administration_moneytransfer.sender_id as sender_id'
     * @param $ArrWhere - where condition - should be as array for example: $ArrWhere[administraion_moneytransfer.id]=$id
     * @return array
     */
    public function selectWhereVal($table,$select,$ArrWhere)
    {
         $query=$this->pdo->from($table,null,"v")->select($select)->where($ArrWhere)->orderBy()->fetchAll();
            return $query;
    }

    /**
     * Handle object for transactions report json.
     *
     * @param $value
     * @param $style
     * @param int $colspan
     * @return array
     */
    function addValueStyle($value,$style,$colspan = 0){

        if($colspan == 0){
            $arr =  array(
              "value"=> $value,
              "style"=> $style
            );
        }else{
            $arr =  array(
              "value"=> $value,
              "style"=> $style,
              "colspan"=>$colspan
            );
        }

        return $arr;
    }

    /**
     * @return select all data from db
     */
    function selectDBAll(){
         $query = $this->pdo->from($this->table)->execute();
         return $query;
     }



    /** Global function for making json which JavaScript knows how to handle
     *  The the javascript parser is : $array['the Select to be implement'][any - selected for remember] = the value
     *
     * @param $paramsArr
     * @return array
     */
    function makeReturnJson($paramsArr)
    {
        $arr = array();
        foreach ($paramsArr as $params) {
            foreach ($params as $key => $value) {
                $arr[$key] = $value;
            }
        }

        return $arr;
    }

    /** function that change the position of keys in the array
     *
     * @param $array
     * @param $swap_a
     * @param $swap_b
     * @return mixed
     */
    function array_swap($array,$swap_a,$swap_b){
        list($array[$swap_a],$array[$swap_b]) = array($array[$swap_b],$array[$swap_a]);
        return  $array;
    }

    function updateClientOrClerk(){
        $res =  $this->pdo
          ->update($this->table)
          ->innerJoin(" branches on branches.id_num = agents.branch_num  ")
          ->set($this->valueArr)
          ->where($this->valueArrWhere)
          ->execute();
        return $res ;
    }
}