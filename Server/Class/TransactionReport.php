<?php
/**
 *  written by zlil;
 *  build the transaction report using fluent pdo
 *  this class gets called from the reportsServer.php file
 *  in the last case "report1"
 */



class TransactionReport extends Report {


    protected $maxPage=0;
    protected $firstRequest=true;
    protected $offsetIteration=0;

    function __construct($table)
    {
        parent::__construct($table);

    }

    public function selectDB()
    {
        // TODO: Implement selectDB() method.
    }

    /**
     * @param $_POST
     * @return query from the database filtered by the user inputs (valueArrWhere) include the current offset or limit
     */
    function getTransactionQuery($autonomy)
    {

        switch (true) {
            case $_POST['search'] != "":
                $this->valueArrWhere['id'] = $_POST['search'];
                break;
            case $_POST['bankAccount'] != "":
                $this->valueArrWhere['bankAccount'] = $_POST['bankAccount'];
                break;
            default:
                $this->checksInputs($autonomy);
                break;

        }

        if($_POST['firstReq']=='true' || true){

            $query=$this->calculatesTotalSum();
            $this->calcPages(($query[0]['count'])); //get the numbers of rows/results and calculates numbers of pages
            $this->firstRequest=false;
            //return $query;
        }


        /**this query using limit and offset to get only 20 rows each iteration*/
        $query2 = $this->pdo->from($this->table,null,"v")->disableSmartJoin()->select("administration_moneytransfer.*")->where($this->valueArrWhere)->limit(20)->offset((20*($_POST['page']-1)))->orderBy("created DESC ")->fetchAll();
        //->fetchAll();

        $this->offsetIteration+=1;
        return $query2;

    }



    /**
     *1. calculates the total sum of the required columns in the table
     *2. calculates and return the length(count) of the total rows
    to help up calculates the total number of pages in the table
     * @return array - the query */
        function calculatesTotalSum(){

        $query=$this->pdo->from($this->table,null,"v")
            ->select("SUM(administration_moneytransfer.amount) as totalAmount ,SUM(administration_moneytransfer.commission) as totalCommission , SUM(administration_moneytransfer.wic_cost) as totalWicCost,
            SUM(CASE WHEN administration_moneytransfer.coin_id ='USD' THEN administration_moneytransfer.amount ELSE (administration_moneytransfer.rate/administration_moneytransfer.buy_rate)*administration_moneytransfer.amount END) as totalWicAmount,
            SUM(CASE WHEN administration_moneytransfer.buy_rate=0 THEN 0 ELSE administration_moneytransfer.amount - (administration_moneytransfer.rate/administration_moneytransfer.buy_rate)*administration_moneytransfer.amount END) as totalCurrDiff, COUNT(*) as count")
            //->leftJoin("administration_moneysender on administration_moneytransfer.money_sender_id = administration_moneysender.passport_or_identity_number")
            //->leftJoin("administration_moneyreceiver on administration_moneytransfer.money_receiver_id = administration_moneyreceiver.id")
            //->leftJoin("administration_bankaccount on administration_moneytransfer.bank_account_id = administration_bankaccount.id")
            //->leftJoin("administration_branch on administration_bankaccount.branch_id = administration_branch.id")
            //->leftJoin("administration_bank on administration_bankaccount.bank_id = administration_bank.description and administration_moneytransfer.country_id=administration_bank.country_id")
            //->leftJoin("administration_agent on administration_agent.id=administration_moneytransfer.agent_id")
            //->leftJoin("branches on branches.branch_name = administration_agent.id")
            //->leftJoin("agents on branches.id_num = agents.branch_num and administration_moneytransfer.user_id = agents.name")
            ->where($this->valueArrWhere)->fetchAll();
        /*        $query1 = $query->execute();
                $query2 = $query->fetchAll();*/
        return $query;
    }


    /**
     * @return array - return the total sum of the required columns where the status of the transaction is NOT LIKE 'FAILED'
     */
    function calculatesTotalSumNoFailedStatus(){

        $newArr=array();
        $newArr=$this->valueArrWhere;
        $not = array('FAILED','ABORTED');

        $newArr['administration_moneytransfer.status_id NOT'] = $not ;

        $query=$this->pdo->from($this->table,null,"v")->select("administration_moneytransfer.usd_eur as usd_eur,SUM(administration_moneytransfer.amount) as totalAmount ,SUM(administration_moneytransfer.commission) as totalCommission , SUM(administration_moneytransfer.wic_cost) as totalWicCost,
            SUM(CASE WHEN administration_moneytransfer.coin_id ='USD' THEN administration_moneytransfer.amount ELSE (administration_moneytransfer.rate/administration_moneytransfer.buy_rate)*administration_moneytransfer.amount END) as totalWicAmount,
            SUM(CASE WHEN administration_moneytransfer.buy_rate=0 THEN 0 ELSE administration_moneytransfer.amount - (administration_moneytransfer.rate/administration_moneytransfer.buy_rate)*administration_moneytransfer.amount END) as totalCurrDiff, COUNT(*) as count")->whereFixed($newArr)->fetchAll();
        //  ->leftJoin("administration_moneysender on administration_moneytransfer.money_sender_id = administration_moneysender.passport_or_identity_number")
        //  ->leftJoin("administration_moneyreceiver on administration_moneytransfer.money_receiver_id = administration_moneyreceiver.id")
        //  ->leftJoin("administration_bankaccount on administration_moneytransfer.bank_account_id = administration_bankaccount.id")
        //  ->leftJoin("administration_branch on administration_bankaccount.branch_id = administration_branch.id")
        //  ->leftJoin("administration_bank on administration_bankaccount.bank_id = administration_bank.description and administration_moneytransfer.country_id=administration_bank.country_id")
        //  ->leftJoin("administration_agent on administration_agent.id=administration_moneytransfer.agent_id")
        //  ->leftJoin("branches on branches.branch_name = administration_agent.id")
        //  ->leftJoin("agents on branches.id_num = agents.branch_num and administration_moneytransfer.user_id = agents.name")

        return $query;
    }




    /**
     * @param $totalDBRows - the total length of the returned query from the database
     * this function calculates the total pages we need to generate the report
     * dividing the total length by 20 (20 rows per page)
     */
    function calcPages($totalDBRows){
        $page=1;
        // Paging data
        $rowsPerPage = 20;
        // Fetch total row num
        $totalRows = ($totalDBRows == 0 ? 1 : $totalDBRows);

        // Calc max page
        $this->maxPage = ceil($totalRows / $rowsPerPage);

        // Page 1 iss default, if got a page - use it
        $offset = 0;
        $delta = $rowsPerPage;
        $currPage = 1;
        if ($page != 1) {
            $offset = $offset + $page * $rowsPerPage - $rowsPerPage;
            $delta = $delta + $page * $rowsPerPage - $rowsPerPage;
            $currPage = $page;
            $i = $offset + 1;
        }

    }




    /**
     * @param $currPage - current page the user watching in the report
     * @return string - string stand for a href browsing the pages
     * this function creates the page filter under the table*/
    function paging($currPage){
        $table_html='';
        $currPage2 = $currPage - 1;
        if ($currPage > 1) {
            $table_html .= '<a onclick="reportChange(1,\'' . $this->valueArrWhere['administration_moneytransfer.usd_eur'] . '\')" style="color: black; height: 30px; width: 30px; font-weight: bold; background: DeepSkyBlue; border-radius: 5px;">&lt;&lt;</a>
        <a onclick="reportChange(' . $currPage2 . ',\'' . $this->valueArrWhere['administration_moneytransfer.usd_eur'] . '\')" style="color: black; height: 30px; width: 30px; font-weight: bold; background: DeepSkyBlue; border-radius: 5px;">&lt;</a>';
        }
        $table_html .= ' Pages:  ' . $currPage . ' of ' . $this->maxPage . '   ';
        $currentPage1 = $currPage + 1;
        if ($currPage <  $this->maxPage) {
            $table_html .= '<a onclick="reportChange(' . $currentPage1 . ',\'' . $this->valueArrWhere['administration_moneytransfer.usd_eur'] . '\');" style="color: black; height: 30px; width: 30px; font-weight: bold; background: OrangeRed; border-radius: 5px;" >&gt;</a>
        <a onclick="reportChange(' .  $this->maxPage . ',\'' . $this->valueArrWhere['administration_moneytransfer.usd_eur'] . '\')" style="color: black; height: 30px; width: 30px; font-weight: bold; background: OrangeRed; border-radius: 5px;">&gt;&gt;</a>';
/*            $table_html .= '<a onclick="getTransactionQuery('.$_POST.');">&gt;</a>
        <a onclick="getTransactionQuery('.$_POST.')">&gt;&gt;</a>';*/
        }
        return $table_html;
    }


    /**
     * @param $query is the result from the database
     * @return array contains the table information line, headers
     */
    function makeTransactionsJson($query)
    {
        $j = 1;
        $shekelSign = "₪";
        $dollarSign = "$";
        $none ='';
        $greenish = "greenish2";
        $witheish = "witheish2";
        $yellowish = "yellowish2";
        $border_right = "border_right";
        $border_left = "border_left";

        $json = array();
        $jsonInfo = array();
        $line = array();
        $header = array();
        $footer = array();
        $additionalInfo = array();
        $userDeatils=array();
        $userJson=array();
        $statusJson=array();

        /**
         * header[0] is the higher header
         * header[1] is under the headers mention as header[0]
         * header[] contains all the table header
         */
        $header[0]['blank'] = $this->addValueStyle('', '',8);
        $header[1]['service'] =  $this->addValueStyle("Service",$border_left);
        $header[1]['no'] = $this->addValueStyle('No.',  $none);
        $header[1]['date'] = $this->addValueStyle('Date', $none);
        $header[1]['transaction_id'] = $this->addValueStyle('Transaction ID', $none);
        $header[1]['reciept_no'] = $this->addValueStyle('Receipt No.', $none);
        $header[1]['status'] = $this->addValueStyle('Status', $none);
        $header[1]['client'] = $this->addValueStyle('Client', $none);
        $header[1]['receiver_branch_local_name'] = $this->addValueStyle('Client Local', $none);
        $header[1]['clerk'] = $this->addValueStyle('Clerk', $none);
        $header[1]['full_name_sender'] = $this->addValueStyle('Full Name', $none . " " . $border_left);
        $header[0]['sender'] = $this->addValueStyle('Sender', $border_left,4);
        $header[1]['IdNumber'] = $this->addValueStyle('ID number', $none);
        $header[1]['amount'] = $this->addValueStyle('Amount', $none);
        $header[1]['commission'] = $this->addValueStyle('Commission', $none);
        $header[1]['full_name_receiver'] = $this->addValueStyle('Full Name', $none . " " . $border_left);
        $header[1]['receiver_bank_local_name'] = $this->addValueStyle('Full Name Local', $none . " " . $border_left);
        $header[0]['receiver'] = $this->addValueStyle('Receiver',  $border_left);
        $header[1]['id_number'] = $this->addValueStyle('ID Number', $none);
        $header[1]['country'] = $this->addValueStyle('Country', $none);
        $header[1]['deliver_type'] = $this->addValueStyle('Deliver Via', $none);
        $header[1]['amount_receiver'] = $this->addValueStyle('Amount To Receiver', $none . " " . $border_left);
        $header[1]['wic_amount'] = $this->addValueStyle('WIC Amount', $none);
        $header[1]['wic_cost'] = $this->addValueStyle('WIC Cost', $none);
        $header[1]['currency_difference'] = $this->addValueStyle('Currency Difference', $none);
        $header[1]['comment'] = $this->addValueStyle('Comments', $none);
        $header[1]['vip_no'] = $this->addValueStyle('VIP', $none);
        $header[1]['sender_city'] = $this->addValueStyle('City', $none);
        $header[1]['sender_address'] = $this->addValueStyle('Address', $none);
        $header[1]['israeli_resident'] = $this->addValueStyle('Israeli Citizen', $none);
        $header[1]['resident'] = $this->addValueStyle('Citizenship', $none);
        $header[1]['id_type'] = $this->addValueStyle('ID Type', $none);
        $header[1]['sender_phone'] = $this->addValueStyle('Phone', $none);
        $header[1]['sender_birth'] = $this->addValueStyle('Date of Birth', $none);
        $header[1]['receiver_address'] = $this->addValueStyle('Address', $none);
        $header[1]['receiver_branch_address_local'] = $this->addValueStyle('Address Local', $none);
        $header[1]['receiver_city'] = $this->addValueStyle('City', $none);
        $header[1]['receiver_phone'] = $this->addValueStyle('Phone', $none);
        $header[1]['coin_id'] = $this->addValueStyle('Coin ID', $none);
        $header[1]['bank_description'] = $this->addValueStyle('Bank Name', $none);
        $header[1]['receiver_bank_local_name'] = $this->addValueStyle('Bank Name Local', $none);
        $header[1]['bank_account_direct'] = $this->addValueStyle('Bank Account', $none);
        $header[1]['natures'] = $this->addValueStyle('Transaction Natures', $none);
        $header[1]['Agent Commission'] = $this->addValueStyle('Agent Commission', $none);
        $header[1]['Rate'] = $this->addValueStyle('Rate', $none);
        $header[1]['buy_rate'] = $this->addValueStyle('Buy Rate', $none);
        $header[1]['amount_nis'] = $this->addValueStyle('Amount NIS', $none);




        //$this->calcPages(mysql_num_rows($query));
        if($_POST['page']==1)
        $i = 0;
        else
            $i = ($_POST['page']-1)*20;
        /**
         * gets the query information and add value to the line[] array. this array holds the table information from the database.
         */
        $total_amount = 0 ;
        $total_commission = 0;
        $total_wic_cost = 0;
        $total_currency_diff = 0;
        $total_wic_amount= 0;
        $totalATservicesCost = 0;


          for($z=0 ; $z < count($query); $z++){
              $row=$query[$z];
            $j = ($j == 1 ? 2 : 1);

            /*
             * this if/else condition makes
             * the table colors separation
             * one row with colors and the one's after
             * with no colors
             */
            if ($i % 2 == 0){
                $none ='';
                $greenish = "greenish2";
                $witheish = "witheish2";
                $yellowish = "yellowish2";
            }
            else{
                $greenish = "";
                $witheish = "";
                $yellowish = "";
            }


            if($row['status']!='FAILED' && $row['status']!='ABORTED'){
          $total_amount += $row['amount'];
          $total_commission += $row['commission'];
          $total_wic_cost += $row['wic_cost'];
          $total_currency_diff += $this->getCurrencyDifference($row);
          $total_wic_amount +=  $this->getWICAmount($row);
          $totalATservicesCost += $this->getATServicesCost($row);
            }


          $i++;
          $service='<a href="customerService.php?transaction_id=' . $row['transaction_id'] . '&agent_name=' . $row['agent_description'] . '" target="_blank"><img src="../images/CustomerServiceIcon.png" style="height: 40px"."></a>';
          $line['service'] =  $this->addValueStyle($service,$border_left);
          $line['no'] = $this->addValueStyle($i,  $yellowish);
          $line['date'] = $this->addValueStyle($row['created'], $yellowish);
          $line['transaction_id'] = $this->addValueStyle($row['id'], $yellowish);
          $line['reciept_no'] = $this->addValueStyle($row['refer_no'], $yellowish);
          $line['status'] = $this->addValueStyle($row['status_id'], $yellowish);
          $line['client'] = $this->addValueStyle($row['agent_id'], $yellowish);
         // $line['receiver_branch_local_name'] = $this->addValueStyle($row['receiver_branch_local_name'], $yellowish);
          $line['clerk'] = $this->addValueStyle($row['user_id'], $yellowish);
          $line['full_name_sender'] = $this->addValueStyle($row['sender_name'], $greenish . " " . $border_left);
          $line['IdNumber'] = $this->addValueStyle($row['sender_id'], $greenish);
          $line['amount'] = $this->addValueStyle($row['amount'].' '.$currencySign = $this->getCurrencySign($row), $greenish);
          $line['commission'] = $this->addValueStyle($row['commission'].' '.$currencySign = $this->getCurrencySign($row), $greenish);
          $line['full_name_receiver'] = $this->addValueStyle($row['receiver_name'], $witheish . " " . $border_left);
        //  $line['receiver_full_name_local'] = $this->addValueStyle($row['receiver_full_name_local'], $witheish . " " . $border_left);
         // $line['id_number'] = $this->addValueStyle($row['receiver_id'], $witheish);
          $line['country'] = $this->addValueStyle($row['country_id'], $witheish);
          $line['deliver_type'] = $this->addValueStyle($row['money_transfer_type_id'], $witheish);
          $line['amount_receiver'] = $this->addValueStyle($this->getAmountToReceiver($row), $witheish . " " . $border_left);
          $line['wic_amount'] = $this->addValueStyle($this->getWICAmount($row)."".$this->getCurrencySign($row), $witheish);
          $line['wic_cost'] = $this->addValueStyle($row['wic_cost']."".$this->getCurrencySign($row), $witheish);
          $line['currency_difference'] = $this->addValueStyle($this->getCurrencyDifference($row). $this->getCurrencySign($row), $witheish);
              if($_SESSION['autonomy']==2){
                            $line['currency_difference'] = $this->addValueStyle($this->getCurrencyDifference($row). $row['coin_id'], $witheish);
              }
          $line['comment'] = $this->addValueStyle($this->getComments($row), $witheish);
         // $line['vip_no'] = $this->addValueStyle($row['vip_number'], $witheish);
          $line['sender_city'] = $this->addValueStyle($row['sender_city'], $greenish);
          $line['sender_address'] = $this->addValueStyle($row['sender_address'], $greenish);
         // $line['resident'] = $this->addValueStyle($row['resident'], $greenish);
         // $line['israeli_resident'] = $this->addValueStyle($row['israeli_resident'], $greenish);
        //  $line['id_type'] = $this->addValueStyle($row['id_type'], $greenish);
          $line['sender_phone'] = $this->addValueStyle($row['sender_phone'], $greenish);
        //  $line['sender_birth'] = $this->addValueStyle($row['sender_birth'], $greenish);
          $line['receiver_address'] = $this->addValueStyle($row['receiver_address'], $greenish);
         // $line['receiver_branch_address_local'] = $this->addValueStyle($row['receiver_branch_address_local'], $greenish);
          $line['receiver_city'] = $this->addValueStyle($row['receiver_city'], $greenish);
          $line['receiver_phone'] = $this->addValueStyle($row['receiver_phone'], $greenish);
          $line['coin_id'] = $this->addValueStyle($row['coin_id'], $greenish);
          $line['bank_description'] = $this->addValueStyle($row['bank_name'], $greenish);
         // $line['receiver_bank_local_name'] = $this->addValueStyle($row['receiver_bank_local_name'], $greenish);
          $line['bank_account_direct'] = $this->addValueStyle($row['bank_account'], $greenish);
         // $line['natures'] = $this->addValueStyle($this->getNatures($row['natures']), $greenish);
          $line['agent_commission'] = $this->addValueStyle($row['agent_commission'], $greenish);
          $line['rate'] = $this->addValueStyle($row['rate'], $greenish);
          $line['buy_rate'] = $this->addValueStyle($row['buy_rate'], $greenish);
          $line['amount_nis'] = $this->addValueStyle($row['amount_nis'], $greenish);
          $line['ATServicesCost'] = $this->addValueStyle(($row["commission"] - $row["wic_cost"]).$this->getCurrencySign($row), $witheish);


              array_push($json, $line);


            $additionalInfo['sender_address']=$this->addValueStyle($row['sender_address'], $none);
            $additionalInfo['receiver_address']=$this->addValueStyle($row['receiver_address'], $none);
            $additionalInfo['sender_phone']=$this->addValueStyle($row['sender_phone'], $none);
           // $additionalInfo['receiver_id']=$this->addValueStyle($row['receiver_id'], $none);
            $additionalInfo['sender_id']=$this->addValueStyle($row['sender_id'], $none);

            array_push($jsonInfo, $additionalInfo);

            $availableStatusChange['statusArr'] = $this->generateStatusOptions($row['status_id']);
            array_push($statusJson, $availableStatusChange['statusArr']);

          }
        $currencySign = $this->getCurrencySign($_POST['id_currency']);

        $footer[0]['total'] = $this->addValueStyle(("Total Page"), $none,10);
        $footer[0]['totalpage'] = $this->addValueStyle(($total_amount . " " . $currencySign), $none,1);
        $footer[0]['totalcommissionPAGE'] = $this->addValueStyle(($total_commission . " " . $currencySign), $none,6);
        $footer[0]['totalwicamountPAGE'] = $this->addValueStyle(($total_wic_amount . " " . $currencySign), $none,1);
        $footer[0]['totalwiccostPAGE'] = $this->addValueStyle(($total_wic_cost . " " . $currencySign), $none,1);
        $footer[0]['totalcurrdiffPAGE'] = $this->addValueStyle(($total_currency_diff . " " . $currencySign), $none,1);
        $footer[0]['totalATServicesCostPAGE'] = $this->addValueStyle(($totalATservicesCost . " " . $currencySign), $none,1);
        $footer[1]['total'] = $this->addValueStyle(("Total"), $none,10);
        $footer[2]['pages'] = $this->addValueStyle($this->paging($_POST['page']), $none );

        $sum=$this->calculatesTotalSumNoFailedStatus(); //calaulating total sum of specific column mention in the query in this function
        foreach ($sum as $row) {
            $currencySign = $this->getCurrencySign($row);

            $footer[1]['totalamount'] = $this->addValueStyle((number_format($row['totalAmount'],2) . " " . $currencySign), $none,1);
            $footer[1]['totalcommission'] = $this->addValueStyle((number_format($row['totalCommission'],2) . " " . $currencySign), $none,6);
            $footer[1]['totalwicamount'] = $this->addValueStyle((number_format($row['totalWicAmount'],2) . " " . $currencySign), $none,1);
            $footer[1]['totalwiccost'] = $this->addValueStyle((number_format($row['totalWicCost'],2) . " " . $currencySign), $none,1);
            $footer[1]['totalcurrdiff'] = $this->addValueStyle((number_format($row['totalCurrDiff'],2) . " " . $currencySign), $none,1);
            $footer[1]['totalATServices'] = $this->addValueStyle((number_format(13*$row['count'],2) . " " . $currencySign), $none,1);

        }

        $userDeatils['user']=$this->addValueStyle($_SESSION['myusername'], $none);
        $userDeatils['branch']=$this->addValueStyle($_SESSION['branch'], $none);
        array_push($userJson, $userDeatils);


        return array("header"=>$header,"body"=>$json,"footer"=>$footer,"additionalInfo"=>$jsonInfo,"user"=>$userJson, "statusChange"=>$statusJson, "tableClass"=>"report table transaction_report_table table-fixed display fixHeaders");


    }


    function generateStatusOptions($row)
    {
        $user = $_SESSION["myusername"];
        $branch = $_SESSION["branch"];
        $autonomy = $_SESSION['autonomy'];
        $onlyWatch = false;
        if ($user == "michael" && $branch == "operation" && $autonomy == 2) {
            $branch = "regular";
            $onlyWatch = true;
        }
        if ($_SESSION['branch'] == "payoutagent") {
            $usertype = "receiver";
        } else {
            if ($_SESSION['branch'] == "administrator" || $_SESSION['branch'] == "customerservice" || $_SESSION['branch'] == "bookkeeping" || $_SESSION['branch'] == "operation") {
                $usertype = "admin";
            } else {
                $usertype = "user";
            }
        }

        $canUpdateTransaction = ($branch == "office2" || $branch == "operation" || $branch == "customerservice" || $branch == "administrator" && !$onlyWatch);
        $statusArray = array();

        if ($usertype == 'admin') {
            if ($canUpdateTransaction) {
                if ($row == "CONFIRMED" || $row == "FAILED" || $row == "ABORTED" || ($row == "WAIT FOR PROCESS(Obligo)" && ($row != "h.k"))
                ) {
                    $flag = false;
                    array_push($statusArray, $flag);
                    return $statusArray;

                } else {
                    $flag = true;
                    array_push($statusArray, $flag);
                    $status = array("CONFIRMED" => "CONFIRMED", "PENDING" => "PENDING", "POSTED" => "POSTED", "REQUEST CANCEL" => "REQUEST CANCEL", "FOR VERIFICATION" => "FOR VERIFICATION", "FAILED" => "FAILED", "CHECK DOCUMENTS" => "CHECK DOCUMENTS");
                    array_push($statusArray, $status);
                    return $statusArray;

                }
            } // Otherwise show status if final or allow status change via combo
            else {
                if ($row == "CONFIRMED" || $row == "FAILED" || $row == "ABORTED") {
                    $flag = false;
                    array_push($statusArray, $flag);
                    return $statusArray;

                } elseif (!$onlyWatch) {
                    //$table_html .= status_html($row['status_id'], $row["refer_no"], "moneytransfer");
                    $flag = true;
                    array_push($statusArray, $flag);
                    $status = $this->status_html($row);
                    array_push($statusArray, $status);
                    return $statusArray;

                }
            }
        } else if ($usertype == 'user') {

            if ($row != 'WAIT FOR PROCESS' && $row != 'WAIT' && $row != 'WAIT FOR PROCESS(Obligo)') {

                $flag = false;
                array_push($statusArray, $flag);
                return $statusArray;


            } else {
                //$table_html1 .= default_status_html($row['status_id'], $row["refer_no"], "moneytransfer");

                if ($row == 'WAIT FOR PROCESS' || $row == 'WAIT' || $row == 'WAIT FOR PROCESS(Obligo)') {
                    $flag = true;
                    array_push($statusArray, $flag);
                    $status = $this->default_status_html($row);
                    array_push($statusArray, $status);
                    return $statusArray;
                }

            }
        } else if ($usertype == 'receiver') {
            if ($canUpdateTransaction) {
                if ($row == "CONFIRMED" || $row == "FAILED" || $row == "ABORTED") {
                    //$table_html .= $row['status_id'] . " " . $row['updated'];
                } else {
                    // $table_html .= "<a href='updateTransaction.php?transaction_id=" . $row['transaction_id'] . "&transaction_type=TRANSFER' target='_blank'>" . $row['status_id'] . "</a>";
                }
            } // Otherwise show status if final or allow status change via combo
            else {
                if ($row == "CONFIRMED" || $row == "FAILED" || $row == "ABORTED") {
                    // $table_html .= $row['status_id'] . " " . $row['updated'];
                } else {
                    // $table_html .= default_status_html($row['status_id'], $row["refer_no"], "moneytransfer");
                    return $status = $this->default_status_html($row);
                }
            }
        }


    }

    function status_html($status)
    {
        if ($status == 'WAIT FOR PROCESS') {
            $types = array("CONFIRMED" => "CONFIRMED", "WAIT FOR PROCESS" => "WAIT FOR PROCESS", "PENDING" => "PENDING", "POSTED" => "POSTED", "REQUEST CANCEL" => "REQUEST CANCEL", "FOR VERIFICATION" => "FOR VERIFICATION", "ABORTED" => "ABORTED", "FAILED" => "FAILED");
        } else if ($status == 'WAIT') {
            $types = array("CONFIRMED" => "CONFIRMED", "WAIT" => "WAIT", "PENDING" => "PENDING", "POSTED" => "POSTED", "REQUEST CANCEL" => "REQUEST CANCEL", "FOR VERIFICATION" => "FOR VERIFICATION", "ABORTED" => "ABORTED", "FAILED" => "FAILED");
        } else if ($status == 'WAIT FOR PROCESS(Compliance)') {
            $types = array("CONFIRMED" => "CONFIRMED", "WAIT FOR PROCESS(Compliance)" => "WAIT FOR PROCESS(Compliance)", "PENDING" => "PENDING", "POSTED" => "POSTED", "REQUEST CANCEL" => "REQUEST CANCEL", "FOR VERIFICATION" => "FOR VERIFICATION", "ABORTED" => "ABORTED", "FAILED" => "FAILED");
        } else if ($status == 'FOR VERIFICATION') {
            $types = array("CONFIRMED" => "CONFIRMED", "FOR VERIFICATION" => "FOR VERIFICATION", "PENDING" => "PENDING", "POSTED" => "POSTED", "REQUEST CANCEL" => "REQUEST CANCEL", "ABORTED" => "ABORTED", "FAILED" => "FAILED");
        } else if ($status == 'CHECK DOCUMENTS') {
            $types = array("CONFIRMED" => "CONFIRMED", "CHECK DOCUMENTS" => "CHECK DOCUMENTS", "PENDING" => "PENDING", "POSTED" => "POSTED", "REQUEST CANCEL" => "REQUEST CANCEL", "ABORTED" => "ABORTED", "FAILED" => "FAILED");
        } else if ($status == 'WAIT FOR PROCESS(Obligo)') {
            $types = array("CONFIRMED" => "CONFIRMED", "WAIT FOR PROCESS(Obligo)" => "WAIT FOR PROCESS(Obligo)", "CHECK DOCUMENTS" => "CHECK DOCUMENTS", "PENDING" => "PENDING", "POSTED" => "POSTED", "REQUEST CANCEL" => "REQUEST CANCEL", "ABORTED" => "ABORTED", "FAILED" => "FAILED");
        } else {
            $types = array("CONFIRMED" => "CONFIRMED", "PENDING" => "PENDING", "POSTED" => "POSTED", "REQUEST CANCEL" =>"REQUEST CANCEL", "FOR VERIFICATION" => "FOR VERIFICATION", "ABORTED" => "ABORTED", "FAILED" => "FAILED");
        }

        return $types;
    }

    function default_status_html($status){
            /* 1.2.16 h.k can pending is transactions  */
            $isHKManager = (strtolower($_SESSION['branch']) === "h.k" && strtolower($_SESSION['user_status'] == "manager"));


            if ($status == 'WAIT FOR PROCESS') {
                $types = array("WAIT FOR PROCESS" => "WAIT FOR PROCESS", "ABORTED" => "ABORTED");
            } else if ($status == 'WAIT FOR PROCESS(Obligo)') {
                $types = array("WAIT FOR PROCESS(Obligo)" => "WAIT FOR PROCESS(Obligo)", "WAIT FOR PROCESS" => "WAIT FOR PROCESS", "ABORTED" => "ABORTED");
            } else {
                $types = array("WAIT" => "WAIT", "ABORTED" => "ABORTED");
            }

            if ($isHKManager) {
                array_push($types, "PENDING");
            }

            return $types;
    }


    /**
     * @param $_POST - the user input / filter setting when asking to generate form
     * @return array - the user inputs in the form before generating the report
     * hold the user inputs in valueArrWhere[]
     */
    function checksInputs($autonomy)
    {


        $from = $_POST['id_from_year'] . "-" . $_POST['id_from_month'] . "-" . $_POST['id_from_day'] . " 00:00:00";
        $to = $_POST['id_to_year'] . "-" . $_POST['id_to_month'] . "-" . $_POST['id_to_day'] . " 23:59:59";

        $this->valueArrWhere['administration_moneytransfer.created >= ?'] = $from;
        $this->valueArrWhere['administration_moneytransfer.created <= ?'] = $to;

        if ($_POST['id_receiver_country'] != "all") {
            $this->valueArrWhere['administration_moneytransfer.country_id'] = $_POST['id_receiver_country'];
        }
        if ($_POST['id_sender_country'] != "all") {
            $this->valueArrWhere['administration_moneytransfer.sender_country'] = $_POST['id_sender_country'];
        }
        if ($_POST['id_transfer_via'] != "all") {
            $this->valueArrWhere['administration_moneytransfer.money_transfer_type_id'] = $_POST['id_transfer_via'];
        }
/*        if ($_POST['id_agent'] != "all-(active and archived)" && !($_POST['id_agent']===null)) {
            $this->valueArrWhere['administration_moneytransfer.agent_id'] = $_SESSION["branch"];
        }
        if ($_POST['id_user'] != "all-(active and archived)") {
            $this->valueArrWhere['administration_moneytransfer.user_id'] = $_SESSION["myusername"];
        }*/
        if ($_POST['id_agent'] != "all-(active and archived)" && !is_numeric($_POST['id_agent']) ) {
            $this->valueArrWhere['administration_moneytransfer.agent_id'] = $_POST['id_agent'];
        }
        if (is_numeric($_POST['id_agent'])) {
            $this->valueArrWhere['administration_moneytransfer.agent_id'] = $this->getBranchNameByID($_POST['id_agent']);
        }
        if ($_POST['id_user'] != "all-(active and archived)") {
            $this->valueArrWhere['administration_moneytransfer.user_id'] = $_POST['id_user'];
        }
        if ($_POST['id_status'] != "all") {
            $this->valueArrWhere['administration_moneytransfer.status_id'] = $_POST['id_status'];
        }
        if($autonomy=='2')
        {
            //$this->valueArrWhere['agents.autonomy_fk'] = '2';
        }

        $this->valueArrWhere['administration_moneytransfer.usd_eur'] = $_POST['id_currency'];

        if($_POST['id_agent']=='iforex')
        {
            $this->valueArrWhere = array();
            $this->valueArrWhere['administration_moneytransfer.sender_name'] = 'FIH Formula Investment House Clearing LTD';
            $this->valueArrWhere['administration_moneytransfer.created >= ?'] = $from;
            $this->valueArrWhere['administration_moneytransfer.created <= ?'] = $to;
            $this->valueArrWhere['branches.active_branch'] =  'ACTIVE';
        }

        if(($_SESSION["myusername"]) == strtolower('Galaxy'))
        {
            $this->valueArrWhere = array();
            $this->valueArrWhere['administration_moneytransfer.created >= ?'] = $from;
            $this->valueArrWhere['administration_moneytransfer.created <= ?'] = $to;
            $this->valueArrWhere['administration_moneytransfer.usd_eur'] = $_POST['id_currency'];
            $this->valueArrWhere['branches.active_branch'] =  'ACTIVE';
            $this->valueArrWhere["status_id"] = array('POSTED','CONFIRMED','FOR VERIFICATION');
            $this->valueArrWhere["administration_moneytransfer.country_id"]=array('CHINA', 'NEPAL', 'HONG KONG');
            $this->valueArrWhere["administration_moneytransfer.money_transfer_type_id"]=array('Galaxy Deposit','Galaxy Pickup');

        }

        if(($_SESSION["myusername"]) == strtolower('Ecpay')){
            $this->valueArrWhere = array();
            $this->valueArrWhere['administration_moneytransfer.created >= ?'] = $from;
            $this->valueArrWhere['administration_moneytransfer.created <= ?'] = $to;
            $this->valueArrWhere['branches.active_branch'] =  'ACTIVE';
            $this->valueArrWhere["status_id"] = array('POSTED','CONFIRMED','FOR VERIFICATION');
            $this->valueArrWhere["administration_moneytransfer.country_id"]='CHINA';
            $this->valueArrWhere["administration_moneytransfer.money_transfer_type_id"]='ECPAY - deposit';
            $this->valueArrWhere['administration_moneytransfer.usd_eur'] = $_POST['id_currency'];
        }

    }
    
}