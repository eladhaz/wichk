<?php
/**
 * Created by zlil.
 * User: zlil
 * Date: 7/11/2016
 * Time: 14:30
 * 
 * this class contains all the main functions needed to build the reports
 * specific reports class such as TransactionReport.php will extend this class
 * 
 */



class Report extends PDOService{

    function __construct($table)
    {
        parent::__construct($table);

    }

    public function selectDB()
    {
        // TODO: Implement selectDB() method.
    }

    /**
     * @param $row - row in the database from the requested query
     * @return string - the comment from the database
     * get the comment for the transactions*/

    function getComments($row){
        $commentIcon = '';
        if ($row['comment'] != null && trim($row['comment']) != '') {
            $patterns = array("/\\\\/", '/\n/', '/\r/', '/\t/', '/\v/', '/\f/');
            $replacements = array('\\\\\\', '\n', '\r', '\t', '\v', '\f');
            $commentData = preg_replace($patterns, $replacements, $row['comment']);
            $commentIcon = '<a href="javascript:none" onclick="alert(' . "'" . $commentData . "'" . ');"><img src="../images/information.png"/></a>';
        }

        $comment = $commentIcon . $row['error_description'];
        if ($row['ime_refno'] != '0') {
            $comment .= 'Ref No.:' . $row['ime_refno'];
        }

        return  $comment;
    }

    /**
     * @param $row - row in the database from the requested query
     * @return the amount for the given row
     * calculates receiver amount */
    function getAmountToReceiver($row)
    {
        if ($row['coin_id'] == 'USD') {
            $amount_to_receiver = sprintf("%1.2f", $row['amount']);
        } else {
            $amount_to_receiver = sprintf("%1.2f", $row['amount'] * $row['rate']);
        }
        return $amount_to_receiver . " " . $row['coin_id'];
    }

    /**
     * @param $row - row in the database from the requested query
     * @return string - the amount of currency difference for the given row
     * calculates currency differnce */
    function getCurrencyDifference($row){
      if ($row['coin_id'] == 'USD') {
          $currency_earn1 = 0;
      }else{
        $currency_earn1 = ($row['buy_rate'] != 0) ? $row['amount'] - ($row['rate'] / $row['buy_rate']) * $row['amount'] : 0;
        $currency_earn1 = sprintf("%1.2f", $currency_earn1);
      }
        return $currency_earn1;

    }


    /**
     * @param $row
     * @return calculation for the AT Services cost column in HK transaction report
     */
    function getATServicesCost($row){
        return ($row["commission"] - $row["wic_cost"]);
    }

    /**
     * @param $row - row in the database from the requested query
     * @return string - the required currency sign for the proper coin
     ** get currency sign */
    function getCurrencySign($row){
        //this part when sending row returned from query as parameter
        if ($row['usd_eur'] == 'USD') {
            $currencySign = "$";
        } else if ($row['usd_eur'] == 'EUR') {
            $currencySign = "€";
        } else if ($row['usd_eur'] == 'HKD') {
            $currencySign = "HK$";
        //this part is when sending directly currency user pick from the report filter $_POST['id_currency']
        } else if ($row == 'USD') {
            $currencySign = "$";
        } else if ($row == 'EUR') {
            $currencySign = "€";
        } else if ($row == 'HKD') {
            $currencySign = "HK$";
        }

        return $currencySign;
    }



    /**
     * @param $row - row in the table
     * @return int|string - return wic_amount calculation
     * this function calculates the wic amount column in the report
     */
    function getWICAmount($row){
        if ($row['coin_id'] != 'USD'){
            $wic_amount = ($row['buy_rate'] != 0) ? sprintf("%1.2f", ($row['rate'] / $row['buy_rate']) * $row['amount']) : 0;
        }else{
            $wic_amount =   sprintf("%1.2f", $row['amount']); //$row['amount'];
        }
        return $wic_amount;
    }

    /**
     * @param $jsonaNatures froo the money_transfer table
     * @param bool $isArray
     * @return array|bool
     * this function gets the json in the nature column in the money_transfer table
     * and translate it to the required nature
     */

    function getNatures($jsonaNatures, $isArray = false)
    {
        $arrNatures = ($isArray) ? $jsonaNatures : json_decode($jsonaNatures, true);
        $strNatures = "";
        $natureString="";
        foreach ($arrNatures as $key => $value) {
            $strNatures .= "$key ,";
        }
        $strNatures = substr($strNatures, 0, strlen($strNatures) - 1);
        $arrWhere=array();
        array_push($arrWhere, "1");
        $natures = $this->pdo->from('transcation_natures')->select('transcation_natures.*')->where("id in (".$strNatures.") ")->fetchAll();

        for($i=0;$i<sizeof($natures);$i++){
            if(($i==0 && sizeof($natures)==1) || ($i==sizeof($natures)-1) ){
                $natureString.=$natures[$i]['nature'];
            }
            else{
                $natureString.=$natures[$i]['nature']." ,";
            }
        }
        return $natureString;

    }

    function getBranchNameByID($id){
        $where=array();
        $where['branches.id_num']=$id;
        $branch_name = $this->pdo->from('branches')->select('branches.branch_name')->where($where)->fetchAll();
        return $branch_name[0]['branch_name'];
    }
    
    
    
    

}