<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/4/2016
 * Time: 11:07
 */

include_once "FluentPDO/FluentPDO.php";
include_once "Class/PDOService.php";
include_once "Class/ReportFilter.php";
session_start();




$reportFilter = new ReportFilter('administartion_transactionreportfilter');
$branch = strtolower($_SESSION['branch']);
if(isset($_POST['clerksReq'])){
    $clerks=$reportFilter->getClerksOnly($_POST['clientName']);
    echo json_encode($clerks);
    exit;
}
$filters = $reportFilter->getFilters($branch);
echo json_encode($filters);