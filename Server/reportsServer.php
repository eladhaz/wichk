<?php

/**
 * Handle reports submits - get the transactions reports
 */


require_once "FluentPDO/FluentPDO.php";
require_once "Class/PDOService.php";
require_once "Class/Report.php";
require_once "Class/TransactionReport.php";
require_once "Class/DataColumnBuilder.php";


session_start();
$autonomy = $_SESSION['autonomy'];
$branch = $_SESSION{'branch'};
$officeArr = array("bookkeeping","operation","administrator","customerservice");
$HKDFilter = (in_array($branch,$officeArr)  ? true : false);
$coinChoose = $_GET['coin'];
$arr = array();

switch ($_GET['action']){
  case "reportTest":
    $server = new TransactionReport("administration_moneytransfer");
      $query = $server->getTransactionQuery($autonomy);
      $res = $server->makeTransactionsJson($query);
    $dataBuilder = new DataColumnBuilder($_SESSION['myusername'], $_SESSION['branch'], $res);
    $res = $dataBuilder->buildDataByUser();
    echo json_encode(array("result"=>$res));
    break;

}
exit;

/**
 *  Organize coins array - the choose coin will be the first one to display
 *
 * @param $coinChoose
 * @param $coinArr
 * @return string
 */
function coinOrganize($coinChoose, $coinArr){
    $coinArr = explode(",",$coinArr);
    $countCoins = count($coinArr);
    if($countCoins == 1){
      return $coinChoose;
    }else{
        $keyChoose = array_search($coinChoose,$coinArr);
        $firstCoin = $coinArr[0];

        $coinArr[0] = $coinArr[$keyChoose];
        $coinArr[$keyChoose] = $firstCoin;
    }
  return implode(",",$coinArr);
}